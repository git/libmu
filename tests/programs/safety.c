/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * safety.c -- check safety functions
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <errno.h>

/* Mu includes */
#include <options.h>
#include <safe.h>

#include "test.h"

/*
 * Static helper functions
 */
static int die_callback(int, const char *, void *, char *);
static int warn_callback(int, const char *, void *, char *);
static int xmalloc_callback(int, const char *, void *, char *);
static int xcalloc_callback(int, const char *, void *, char *);
static int xrealloc_callback(int, const char *, void *, char *);
static int xreallocarray_callback(int, const char *, void *, char *);
static int print_size_max(void *, char *);

int main(int argc, char **argv) {
  MU_OPT_CONTEXT *context;
  const MU_OPT options[] = {
    {
     .short_opt	= "d",
     .long_opt	= "die",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = die_callback,
     .cb_data	= &context,
     .arg_help	= "STATUS,MESSAGE",
     .help	= "call mu_die(STATUS, MESSAGE)"
    },
    {
     .short_opt	= "w",
     .long_opt	= "warn",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = warn_callback,
     .arg_help	= "MESSAGE",
     .help	= "call mu_warn(MESSAGE)"
    },
    {
     /* Can't use 'm' because that's for '--man'. */
     .long_opt	= "malloc",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = xmalloc_callback,
     .arg_help	= "BYTES",
     .help	= "call mu_xmalloc(BYTES)"
    },
    {
     .short_opt	= "c",
     .long_opt	= "calloc",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = xcalloc_callback,
     .arg_help	= "COUNT,ELTSIZE",
     .help	= "call mu_xcalloc(COUNT, ELTSIZE)"
    },
    {
     .short_opt	= "r",
     .long_opt	= "realloc",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = xrealloc_callback,
     .arg_help	= "BYTES",
     .help	= "call mu_xrealloc(mu_xmalloc(1), BYTES)"
    },
    {
     .short_opt	= "a",
     .long_opt	= "reallocarray",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = xreallocarray_callback,
     .arg_help	= "COUNT,ELTSIZE",
     .help	= "call mu_xrealloc(mu_xmalloc(1), COUNT, ELTSIZE)"
    },
    {
     .short_opt	= "z",
     .long_opt	= "size-max",
     .has_arg	= MU_OPT_NONE,
     .callback_none = print_size_max,
     .help	=
     "print three numbers separated by commas: the "
     "maximum number a 'size_t' can hold, an arbitrary "
     "number, and the first divided by the second"
    },
    { 0 }
  };
  int ret;

  test_init(argc, argv);

  context =
    TEST_NEW_CONTEXT_NODESC(0, "Safety", NULL, "safety functions");

  ret = mu_parse_opts(context);
  if (MU_OPT_ERR(ret))
    return 1;
  mu_shift_args(&argc, &argv, ret);
  if (argc > 1) {
    mu_format_help(stderr, context);
    return 1;
  }
  mu_opt_context_xfree(context);
  return 0;
}

/***************************\
|* Static helper functions *|
\***************************/

/*
 * Functions that process there argument and call
 * s/(.+?)_callback/mu_\1/ with the processed arguments (that's a PCRE
 * by the way).
 */

static int die_callback(int has_arg, const char *arg,
			void *data, char *err) {
  int status;
  const char *msg;
  MU_OPT_CONTEXT **context = data;

  if (get_int_str(&status, &msg, arg, err))
    return 1;

  /* This isn't really necessary, but it stops valgrind from warning,
     which allows us to check for real leaks. */
  mu_opt_context_xfree(*context);

  mu_die(status, "%s", msg);
}

static int warn_callback(int has_arg, const char *arg,
			 void *data, char *err) {
  int ret = mu_warn("%s", arg);
  if (ret)
    snprintf(err, MU_OPT_ERR_MAX, "mu_warn() returned %d", ret);
  return ret;
}

static int xmalloc_callback(int has_arg, const char *arg,
			    void *data, char *err) {
  size_t size;
  if (get_size_1(&size, arg, err))
    return 1;
  free(mu_xmalloc(size));
  printf("mu_xmalloc(%zu)\n", size);
  return 0;
}

static int xcalloc_callback(int has_arg, const char *arg,
			    void *data, char *err) {
  size_t nmemb, size;
  if (get_size_2(&nmemb, &size, arg, err))
    return 1;
  free(mu_xcalloc(nmemb, size));
  printf("mu_xcalloc(%zu, %zu)\n", nmemb, size);
  return 0;
}

static int xrealloc_callback(int has_arg, const char *arg,
			     void *data, char *err) {
  size_t size;
  if (get_size_1(&size, arg, err))
    return 1;
  free(mu_xrealloc(mu_xmalloc(1), size));
  printf("mu_xrealloc(PTR, %zu)\n", size);
  return 0;
}

static int xreallocarray_callback(int has_arg, const char *arg,
				  void *data, char *err) {
  size_t nmemb, size;
  if (get_size_2(&nmemb, &size, arg, err))
    return 1;
  free(mu_xreallocarray(mu_xmalloc(1), nmemb, size));
  printf("mu_xreallocarray(PTR, %zu, %zu)\n", nmemb, size);
  return 0;
}

/*
 * Print ULLONG_T_MAX and some other stuff. We use ULLONG_T_MAX,
 * because that is the maximum integer we can *parse*, although it may
 * or may not be the maximum number a `size_t' can hold.
 */
static int print_size_max(void *data, char *err) {
  if (ULLONG_T_MAX > SIZE_MAX / 2 && getenv("RUN_UNDER_VALGRIND")) {
    /* This is so that Valgrind doesn't warn about fishy argument
       values. */
    printf("%"PRIdULLT",8,%"PRIdULLT"\n",
	   (ullong_t)(SIZE_MAX / 2), (ullong_t)(SIZE_MAX / 16));
  }
  else {
    printf("%"PRIdULLT",8,%"PRIdULLT"\n",
	   ULLONG_T_MAX, ULLONG_T_MAX / 8);
  }

  return 0;
}

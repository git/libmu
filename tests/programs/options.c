/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * options.c -- check option parsing
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <assert.h>

/* Mu includes */
#include <options.h>
#include <safe.h>

#include "test.h"

#define FILE_OPT						\
  .has_arg	= MU_OPT_REQUIRED,				\
  .arg_type	= MU_OPT_FILE,					\
  .file_mode	= "r",						\
  .argstr	= &file_name,					\
  .arg		= &file,					\
  .callback_file = file_callback,				\
  .arg_help	= "FILE",					\
  .help		= "read FILE and print some of its contents"
#define DIR_OPT						\
  .has_arg	= MU_OPT_REQUIRED,			\
  .arg_type	= MU_OPT_DIRECTORY,			\
  .callback_directory = dir_callback,			\
  .arg_help	= "DIR",				\
  .help		= "list the contents of DIR"

#define ENUM_CASE(value) case (value): return #value;
#define ENUM_DEFAULT default: return NULL;

enum test { TEST_FOO, TEST_BAR, TEST_BAZ };

/*
 * Static helper functions
 */
static int file_callback(int, const char *, FILE *, void *, char *);
static int dir_callback(int, const char *, DIR *, void *, char *);
static int str_callback(int, const char *, void *, char *);
static int enum_callback(int, int, void *, char *);
static const char *enum2str(enum test);
static int int_callback(int, long, void *, char *);
static int float_callback(int, double, void *, char *);
static int bool_callback(int, int, void *, char *);
static int error_callback(void *, char *);
static int print_opt(void *, char *);
static int print_opt_arg(int, const char *, void *, char *);
static int print_opt_negatable(int, void *, char *);
static int print_opt_subopt(int, void *, char *);
static int subopt_no_arg(void *, char *);
static int subopt_arg(int, const char *, void *, char *);
static int print_argument(const char *, void *, char *);
static int free_and_succeed(void *);
static int read_and_print(FILE *);

int main(int argc, char **argv) {
  const char *file_name;
  FILE *file;
  int found_subopt, found_subopt_arg,
    found_negatable, found_negatable_subopt;
  MU_ENUM_VALUE enum_table[] = {
    { "foo", TEST_FOO },
    { "bar", TEST_BAR },
    { "baz", TEST_BAZ },
    { 0 }
  };
  const MU_OPT suboptions[] = {
    { .category		= "I/O suboptions" },
    {
     .subopt_name	= "file",
     FILE_OPT
    },
    {
     .subopt_name	= "directory",
     DIR_OPT
    },
    { .category		= "Another suboption category" },
    {
     .subopt_name	= "none",
     .env_var		= "SUBOPT_NONE",
     .has_arg		= MU_OPT_NONE,
     .callback_none	= subopt_no_arg,
     .help		= "a suboption taking no argument"
    },
    {
     .subopt_name	= "negatable|reversible",
     .env_var		= "SUBOPT_NEGATABLE|SUBOPT_REVERSIBLE",
     .has_arg		= MU_OPT_NONE,
     .negatable		= 1,
     .found_opt		= &found_negatable_subopt,
     .callback_negatable = print_opt_negatable,
     .cb_data		= (void *)1,
     .help		= "a negatable suboption"
    },
    {
     .subopt_name	= "optional|alias",
     .env_var		= "SUBOPT_OPTIONAL|SUBOPT_ALIAS",
     .has_arg		= MU_OPT_OPTIONAL,
     .arg_type		= MU_OPT_STRING,
     .callback_string	= subopt_arg,
     .cb_data		= (void *)0,
     .help		= "a suboption taking an optional argument",
    },
    {
     .subopt_name	= "required",
     .env_var		= "SUBOPT_REQUIRED",
     .has_arg		= MU_OPT_REQUIRED,
     .arg_type		= MU_OPT_STRING,
     .callback_string	= subopt_arg,
     .cb_data		= (void *)1,
     .help		= "a suboption taking a required argument",
    },
    { 0 }
  };
  const MU_OPT options[] = {
    {
     .short_opt	= "f",
     .long_opt	= "file",
     .env_var	= "FILE",
     FILE_OPT
    },
    {
     .short_opt	= "d",
     .long_opt	= "directory",
     .env_var	= "DIRECTORY",
     DIR_OPT
    },
    { .category	= "Options taking suboptions" },
    {
     .short_opt	= "su",
     .long_opt	= "subopts|alias",
     .env_var	= "SUBOPTS|ALIAS",
     .has_arg	= MU_OPT_OPTIONAL,
     .arg_type	= MU_OPT_SUBOPT,
     .found_opt	= &found_subopt,
     .found_arg	= &found_subopt_arg,
     .callback_subopt = print_opt_subopt,
     .subopts	= suboptions,
     .arg_help	= "SUBOPTS",
     .help	= "parse SUBOPTS as suboptions"
    },
    { .category	= "Negatable options" },
    {
     .short_opt	= "gl",
     .long_opt	= "negatable|reversible",
     .env_var	= "NEGATABLE|REVERSIBLE",
     .has_arg	= MU_OPT_NONE,
     .negatable	= 1,
     .found_opt	= &found_negatable,
     .callback_negatable = print_opt_negatable,
     .cb_data	= (void *)0,
     .help	= "a negatable option"
    },
    /* The following four options don't actually do anything. They are
       just dummy options to test abbreviated matching. */
    {
     .long_opt	= "negatable-again",
     .has_arg	= MU_OPT_NONE,
     .negatable	= 1,
     .help	= "another negatable option"
    },
    {
     .long_opt	= "no-negatable-not-really",
     .has_arg	= MU_OPT_NONE,
     .help	= "an option that looks negatable but isn't"
    },
    {
     .long_opt	= "no-negatable-really-is",
     .has_arg	= MU_OPT_NONE,
     .negatable	= 1,
     .help	=
     "an option that looks negatable and really "
     "is (but not, perhaps, how you think)"
    },
    {
     .long_opt	= "no-negatable-really-is-again",
     .has_arg	= MU_OPT_NONE,
     .negatable	= 1,
     .help	= "another option that looks negatable and really is"
    },
    { .category	= "Just a boring category" },
    {
     .short_opt	= "a",
     .long_opt	= "string",
     .env_var	= "STRING|STRING_ALIAS",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = str_callback,
     .help	= "take a string argument"
    },
    {
     .short_opt	= "r",
     .long_opt	= "enum",
     .env_var	= "ENUM",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_ENUM,
     .enum_case_match = 0,
     .callback_enum = enum_callback,
     .enum_values = enum_table,
     .help	=
     "take an enumerated argument, matching case insensitively"
    },
    {
     .short_opt	= "c",
     .long_opt	= "case-enum",
     .env_var	= "CASE_ENUM",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_ENUM,
     .enum_case_match = 1,
     .callback_enum = enum_callback,
     .enum_values = enum_table,
     .help	=
     "take an enumerated argument, matching case sensitively"
    },
    {
     .short_opt	= "i",
     .long_opt	= "integer",
     .env_var	= "INTEGER",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_INT,
     .ibound	= { -10, 10 },
     .callback_int = int_callback,
     .help	=
     "take an integer argument between -10 and 10 inclusive"
    },
    {
     .short_opt	= "n",
     .long_opt	= "float",
     .env_var	= "FLOAT",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_FLOAT,
     .callback_float = float_callback,
     .fbound	= { -10, 10 },
     .help	=
     "take a floating-point number argument between -10 and 10 "
     "inclusive"
    },
    {
     .short_opt	= "b",
     .long_opt	= "boolean",
     .env_var	= "BOOLEAN",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_BOOL,
     .callback_bool = bool_callback,
     .help	= "take a boolean argument"
    },
    {
     .short_opt	= "e",
     .long_opt	= "error",
     .env_var	= "ERROR",
     .has_arg	= MU_OPT_NONE,
     .callback_none = error_callback,
     .help	= "cause fatal error"
    },
    {
     .long_opt	= "foobarbaz",
     .has_arg	= MU_OPT_OPTIONAL,
     .arg_type	= MU_OPT_STRING,
     .callback_string = print_opt_arg,
     .cb_data	= "foobarbaz",
     .help	= "an option named 'foobarbaz'"
    },
    {
     .long_opt	= "foobazbar",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = print_opt_arg,
     .cb_data	= "foobazbar",
     .help	= "an option named 'foobazbar'"
    },
    {
     .long_opt	= "foobarbazqux",
     .has_arg	= MU_OPT_OPTIONAL,
     .arg_type	= MU_OPT_STRING,
     .callback_string = print_opt_arg,
     .cb_data	= "foobarbazqux",
     .help	= "an option named 'foobarbazqux'"
    },
    /*
     * The following option is to ensure that we don't erroneously
     * match things like 'jar' against 'jam|car'. Suppose we are
     * iterating over the string 'jar' as follows:
     *
     *	   First iteration (i = 0):
     *	       "jar"[i] => 'j'
     *	       "jam"[i] => 'j'
     *	       Match; continue.
     *
     *	   Second iteration (i = 1):
     *	       "jar"[i] => 'a'
     *	       "jam"[i] => 'a'
     *	       Match; continue.
     *
     *	   Third iteration (i = 2):
     *	       "jar"[i] => 'r'
     *	       "jam"[i] => 'm'
     *	       No match; check next alias.
     *	       "jar"[i] => 'r'
     *	       "car"[i] => 'r'
     *	       Exact match (not an actual match); done.
     *
     * As you can see, 'jar' is matched against 'jam|car', although it
     * shouldn't be. This also occurs, of course, even if it's not an
     * exact match. For example, matching 'jar' against 'carbon|jam'
     * (mmm, carbon jam...)
     */
    {
     .long_opt	= "jam|car",
     .has_arg	= MU_OPT_NONE,
     .callback_none = print_opt,
     .cb_data	= "jam', alias 'car",
     .help	= "an option named 'jam', alias 'car'"
    },
    {
     .long_opt	= "vroom",
     .has_arg	= MU_OPT_OPTIONAL,
     .arg_type	= MU_OPT_STRING,
     .callback_string = print_opt_arg,
     .cb_data	= "vroom",
     .help	=
     "a long option that doesn't share a first letter with a "
     "short option (that's right, all the others were taken)"
    },
    {
     .short_opt	= "w",
     .has_arg	= MU_OPT_NONE,
     .callback_none = print_opt,
     .cb_data	= "w",
     .help	= "an option named 'w'"
    },
    {
     .short_opt	= "x",
     .has_arg	= MU_OPT_NONE,
     .callback_none = print_opt,
     .cb_data	= "x",
     .help	= "an option named 'x'"
    },
    {
     .short_opt	= "y",
     .has_arg	= MU_OPT_OPTIONAL,
     .arg_type	= MU_OPT_STRING,
     .callback_string = print_opt_arg,
     .cb_data	= "y",
     .help	= "an option named 'y'"
    },
    {
     .short_opt	= "z",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = print_opt_arg,
     .cb_data	= "z",
     .help	= "an option named 'z'"
    },
    { 0 }
  };
  const MU_OPT extra_opt[] = {
    {
     .long_opt	= "extra",
     .has_arg	= MU_OPT_NONE,
     .callback_none = print_opt,
     .cb_data	= "extra",
     .help	=
     "an extra option, present only when the "
     "ADD_EXTRA_OPT environment variable is set"
    },
    { 0 }
  };
  int ret;
  int flags = 0, do_arg_callback = 0;
  MU_OPT_CONTEXT *context;
  char **no_prefixes = NULL;
  const char *add_extra_opt;

  test_init(argc, argv);

  /* Get the flags to use for parsing options and also set any
     environment variables passed on the command line. */
  while (argc > 1) {
    char *arg = argv[1];

    if (!strcmp(arg, "PERMUTE"))
      flags |= MU_OPT_PERMUTE;
    else if (!strcmp(arg, "BUNDLE"))
      flags |= MU_OPT_BUNDLE;
    else if (!strcmp(arg, "ALLOW-INVALID"))
      flags |= MU_OPT_ALLOW_INVALID;
    else if (!strcmp(arg, "STOP-AT-ARG"))
      flags |= MU_OPT_STOP_AT_ARG;
    else if (!strcmp(arg, "ARG-CALLBACK"))
      do_arg_callback = 1;
    else if (*arg != '-' && *arg != '+') {
      char *value = strchr(arg, '=');
      if (!value)
	break;
      *(value++) = '\0';
      if (strcmp(arg, "no-prefixes")) {
#ifdef HAVE_SETENV
	if (setenv(arg, value, 1))
	  mu_die(1, "%s: cannot set environment variable '%s' to "
		 "'%s': %s\n", argv[0], arg, value, strerror(errno));
#else
	exit(77); /* Skip the test. */
#endif
      }
      else {
	/* `value' is a list of comma-separated negation prefixes. */
	size_t len, i;
	/* Count the number of prefixes if the string's not
	   empty. Otherwise, take an empty string to mean no negation
	   prefixes. */
	if (*value) {
	  len = 1; /* The length is the number of commas plus one */
	  for (char *p = value; *p; p++) {
	    if (*p == ',') {
	      *p = '\0';
	      len++;
	    }
	  }
	}
	else
	  len = 0;

	/* Now build the list. Allocate +1 for the terminating
	   NULL. */
	no_prefixes = mu_xmalloc(sizeof(*no_prefixes) * (len + 1));
	i = 0;
	fputs("Negation prefixes: ", stdout);
	while (1) {
	  printf("%s'%s'", i ? ", " : "", value);
	  no_prefixes[i] = value;
	  if (++i < len)
	    value += strlen(value) + 1;
	  else
	    break;
	}
	putchar('\n');
	no_prefixes[len] = NULL;
      }
    }
    else
      break;

    mu_shift_args(&argc, &argv, 1);
  }

  context =
    TEST_NEW_CONTEXT(flags, "Options",
		     "[FLAGS]... [ARG-CALLBACK] "
		     "[no-prefixes=NO-PREFIXES] [ENV_VAR=VAL]... "
		     "[OPTIONS]... [ARG]...", "option parsing", "\
The first arguments must be specified before any options or \
positional arguments. FLAGS are flags to use for option parsing. \
ARG-CALLBACK (literally 'ARG-CALLBACK') causes an argument callback \
to be used to parse positional arguments. NO-PREFIXES specifies \
alternative negation prefixes (separated by commas) instead of \
'no-'. Environment variables may be specified on the command line in \
the form ENV_VAR=VAL. Arguments after these first special arguments \
are options and positional arguments, which will be parsed as such.");

  if (do_arg_callback)
    mu_opt_context_set_arg_callback(context, print_argument,
				    mu_xstrdup("hello"),
				    free_and_succeed);

  if (no_prefixes) {
    int len;

    /* If the length is exactly three, use
       mu_parse_opts_with_prefixes() instead of
       mu_parse_opts_with_prefix_array() so we can test that as
       well. */
    for (len = 0; len <= 3 && no_prefixes[len]; len++);
    if (len == 3) {
      puts("Using mu_opt_context_xset_no_prefixes().");
      mu_opt_context_xset_no_prefixes(context, no_prefixes[0],
				      no_prefixes[1],
				      no_prefixes[2], NULL);
    }
    else {
      puts("Using mu_opt_context_xset_no_prefix_array().");
      mu_opt_context_xset_no_prefix_array(context, no_prefixes);
    }

    free(no_prefixes);
  }

  /* Add the extra option if the ADD_EXTRA_OPT environment variable is
     set. */
  add_extra_opt = getenv("ADD_EXTRA_OPT");
  if (add_extra_opt) {
    if (!strcasecmp(add_extra_opt, "prepend")) {
      puts("Prepending the extra option.");
      mu_opt_context_xadd_options(context, extra_opt, MU_OPT_PREPEND);
    }
    else if (!strcasecmp(add_extra_opt, "append")) {
      puts("Appending the extra option.");
      mu_opt_context_xadd_options(context, extra_opt, MU_OPT_APPEND);
    }
    else
      mu_die(1, "%s: invalid value for ADD_EXTRA_OPT: '%s'\n",
	     argv[0], add_extra_opt);
  }

  /* Now parse the options. */
  ret = mu_parse_opts(context);
  mu_opt_context_xfree(context);
  if (MU_OPT_ERR(ret))
    return 10;		    /* 10 to indicate failed option parsing */
  mu_shift_args(&argc, &argv, ret);

  if (file) {
    printf("main: reading from %s\n", file_name);
    if (read_and_print(file))
      mu_die(1, "%s: cannot read a line from %s and print to "
	     "stdout: %s\n", argv[0], file_name, strerror(errno));
    if (fclose(file))
      mu_die(1, "%s: cannot close %s: %s\n",
	     argv[0], file_name, strerror(errno));
  }

  if (found_subopt)
    printf("Found the --subopt option, %s.\n", found_subopt_arg ?
	   "and it had an argument" : "but it had no arguments");
  if (found_negatable)
    puts("Found the --negatable option.");
  if (found_negatable_subopt)
    puts("Found the --negatable suboption.");

  if (argc > 1) {
    /* Print the remaining arguments. */
    printf("%d remaining argument%s: {",
	   argc - 1, (argc - 1 == 1) ? "" : "s");
    for (int i = 1; i < argc; i++)
      printf("%s'%s'", (i > 1) ? ", " : "", argv[i]);
    puts("}");
  }

  return 0;
}

/***************************\
|* Static helper functions *|
\***************************/

/*
 * Read a file and print one line of its contents.
 */
static int file_callback(int has_arg, const char *argstr, FILE *arg,
			 void *data, char *err) {
  printf("file_callback: reading from %s\n", argstr);
  if (read_and_print(arg)) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot read a line from %s and print to stdout: %s",
	     argstr, strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * List a directory's contents.
 */
static int dir_callback(int has_arg, const char *argstr, DIR *arg,
			void *data, char *err) {
  struct dirent **files;
  size_t n_files;

  printf("Reading '%s'...\n", argstr);

  /* Get the number of files. */
  errno = 0;
  for (n_files = 0; readdir(arg); n_files++)
    errno = 0;
  if (errno)
    goto error;
  rewinddir(arg);

  /* Now get the actual files. */
  files = malloc(sizeof(*files) * n_files);
  if (!files)
    goto error;
  for (size_t i = 0; i < n_files; i++) {
    errno = 0;
    files[i] = readdir(arg);
    if (!files[i]) {
      const int errno_save = errno;
      free(files);
      errno = errno_save;
      goto error;
    }
  }
  assert(!readdir(arg) && !errno);

  /* Sort them. */
  qsort(files, n_files, sizeof(*files),
	(int (*)(const void *, const void *))alphasort);

  /* Print them. */
  for (size_t i = 0; i < n_files; i++)
    puts(files[i]->d_name);

  free(files);

  return 0;

 error:
  snprintf(err, MU_OPT_ERR_MAX, "cannot read directory %s: %s",
	   argstr, strerror(errno));
  return 1;
}

/*
 * Print the string argument to standard output.
 */
static int str_callback(int has_arg, const char *arg,
			void *data, char *err) {
  if (printf("Found a string argument: '%s'\n", arg) < 0) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Print the enumerated argument to standard output.
 */
static int enum_callback(int has_arg, int arg,
			 void *data, char *err) {
  const char *name = enum2str(arg);
  assert(name);
  if (printf("Found an enumerated argument: '%s'\n", name) < 0) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Return a string representation of `e', or NULL if it's not a valid
 * value.
 */
static const char *enum2str(enum test e) {
  switch (e) {
    ENUM_CASE(TEST_FOO);
    ENUM_CASE(TEST_BAR);
    ENUM_CASE(TEST_BAZ);
    ENUM_DEFAULT;
  }
}

/*
 * Print the integer argument to standard output.
 */
static int int_callback(int has_arg, long arg,
			void *data, char *err) {
  if (printf("Found an integer argument: %ld\n", arg) < 0) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Print the floating-point number argument to standard output.
 */
static int float_callback(int has_arg, double arg,
			  void *data, char *err) {
  if (printf("Found a floating-point number argument: %g\n", arg)
      < 0) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Print the boolean argument to standard output.
 */
static int bool_callback(int has_arg, int arg,
			 void *data, char *err) {
  if (printf("Found a boolean argument: %s\n",
	     arg ? "true" : "false") < 0) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Return an error.
 */
static int error_callback(void *data, char *err) {
  strncpy(err, "an error occurred", MU_OPT_ERR_MAX);
  return 1;
}

/*
 * Print a message when an option that doesn't take an argument is
 * found.
 */
static int print_opt(void *data, char *err) {
  const char *name = data;
  if (printf("Found option: '%s'\n", name) < 0) {
    snprintf(err, MU_OPT_ERR_MAX, "cannot print option '%s': %s",
	     name, strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Print a message when an option that takes an argument is found.
 */
static int print_opt_arg(int has_arg, const char *arg,
			 void *data, char *err) {
  const char *name = data;
  if (printf("Found option: '%s', ", name) < 0 ||
      (has_arg ?
       (printf("with an argument: '%s'.\n", arg) < 0) :
       (puts("without an argument.") == EOF))) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot print option '%s' (%s an argument%s%s%s): %s",
	     name, has_arg ? "with" : "without", has_arg ? ": '" : "",
	     has_arg ? arg : "", has_arg ? "'" : "", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Print a message when a negatable option is found.
 */
static int print_opt_negatable(int value, void *data, char *err) {
  if (printf("Found a negatable %soption, and it was%s negated.\n",
	     data ? "sub" : "", value ? " not" : "") < 0) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Print a message when an option that takes suboptions is found.
 */
static int print_opt_subopt(int has_arg, void *data, char *err) {
  if (printf("The callback for an option which takes suboptions "
	     "was called, and the option did%s have suboptions.\n",
	     has_arg ? "" : " not") < 0) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Print a message when we receive a suboption without an argument.
 */
static int subopt_no_arg(void *data, char *err) {
  if (puts("Found a suboption which doesn't take an argument.")
      == EOF) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Print a message when we receive a suboption with an argument (data
 * is "optional" for an optional argument, or "required" for a
 * required argument).
 */
static int subopt_arg(int has_arg, const char *arg,
		      void *data, char *err) {
  int required = !!data;
  if (printf("Found a suboption which takes %s argument, %s",
	     required ? "a required" : "an optional",
	     has_arg ? "and the argument is " :
	     "but it did not have an argument") < 0 ||
      (has_arg && printf("'%s'", arg) < 0) ||
      puts(".") == EOF) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Print a message when we recieve a non-option positional argument.
 */
static int print_argument(const char *arg, void *data, char *err) {
  const char *const str = data;
  if (printf("Found a positional argument: '%s' "
	     "(data is '%s')\n", arg, str) < 0) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot write to standard output: %s", strerror(errno));
    return 1;
  }
  return 0;
}

/*
 * Call free() and return 0.
 */
static int free_and_succeed(void *data) {
  free(data);
  return 0;
}

/*
 * Read a single line from `stream' and print it to standard
 * output. Return zero on success or nonzero on error, in which case
 * `errno' will be set to indicate the error.
 */
static int read_and_print(FILE *stream) {
  int c;
  while ((c = getc(stream)) != EOF && c != '\n') {
    if (putchar(c) == EOF)
      return 1;
  }
  return ferror(stream) || putchar('\n') == EOF || fflush(stdout);
}

/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * compat.c -- check compatibility functions
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <limits.h>
#include <errno.h>

/* Mu includes */
#include <options.h>
#include <safe.h>
#include <compat.h>

#include "test.h"

/*
 * Static helper functions
 */
static int asprintf_callback(int, const char *, void *, char *);
static int strchrnul_callback(int, const char *, void *, char *);
static int reallocarray_callback(int, const char *, void *, char *);

int main(int argc, char **argv) {
  const MU_OPT options[] = {
    {
     .short_opt	= "a",
     .long_opt	= "asprintf",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = asprintf_callback,
     .arg_help	= "MESSAGE",
     .help	= "call asprintf(<PTR>, MESSAGE)"
    },
    {
     .short_opt	= "s",
     .long_opt	= "strchrnul",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = strchrnul_callback,
     .arg_help	= "STRING,C",
     .help	= "call strchrnul(STRING, C)"
    },
    {
     .short_opt	= "r",
     .long_opt	= "reallocarray",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_STRING,
     .callback_string = reallocarray_callback,
     .arg_help	= "COUNT,ELTSIZE",
     .help	= "call reallocarray(mu_xmalloc(1), COUNT, ELTSIZE)"
    },
    { 0 }
  };
  int ret;
  MU_OPT_CONTEXT *context;

  test_init(argc, argv);

  context = TEST_NEW_CONTEXT_NODESC(0, "Compatibility", NULL,
				    "compatibility functions");

  ret = mu_parse_opts(context);
  if (MU_OPT_ERR(ret))
    return 1;
  if (argc - ret > 1) {
    mu_format_help(stderr, context);
    return 1;
  }
  mu_opt_context_xfree(context);
  return 0;
}

/***************************\
|* Static helper functions *|
\***************************/

/*
 * Functions that process there argument and call s/_callback// with
 * the processed arguments.
 */

static int asprintf_callback(int has_arg, const char *arg,
			     void *data, char *err) {
  char *str;
  int ret = asprintf(&str, "%s", arg);
  if (ret < 0) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "asprintf() failed: %s", strerror(errno));
    return 1;
  }
  printf("ret = asprintf(&str, \"%%s\", \"%s\");\n"
	 "ret: %d\n"
	 "str: \"%s\"\n", arg, ret, str);
  free(str);
  return 0;
}

static int strchrnul_callback(int has_arg, const char *arg,
			      void *data, char *err) {
  char *str, *pos;
  char c;
  if (get_str_char(&str, &c, arg, err))
    return 1;
  pos = strchrnul(str, c);
  printf("pos = strchrnul(\"%s\", '%c');\n"
	 "pos: \"%s\"%s\n", str, c, pos, *pos ? "" : " (empty)");
  free(str);
  return 0;
}

static int reallocarray_callback(int has_arg, const char *arg,
				 void *data, char *err) {
  size_t nmemb, size;
  void *ptr, *newptr;

  if (get_size_2(&nmemb, &size, arg, err))
    return 1;
  ptr = mu_xmalloc(1);
  newptr = reallocarray(ptr, nmemb, size);
  if (!newptr)
    free(ptr);
  free(newptr);
  printf("ptr = reallocarray(mu_xmalloc(1), %zu, %zu);\n"
	 "ptr: %s\n",
	 nmemb, size, newptr ? "<valid pointer>" : "NULL");
  return 0;
}

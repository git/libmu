/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * format.c -- check text formatting
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <limits.h>
#include <errno.h>

/* Mu includes */
#include <options.h>
#include <format.h>
#include <safe.h>

#include "test.h"

struct buf_list {
  struct buf_list *next;
  char buf[4096];
};

/*
 * Static helper functions
 */
static char *get_text(int, char **, FILE *, const char *,
		      const MU_OPT_CONTEXT *);
static char *get_text_args(int, char **);
static char *get_text_file(FILE *, const char *, const char *);

int main(int argc, char **argv) {
  long goal, width, indent, subindent, tab_stop;
  int subindent_given, do_string;
  FILE *file;
  const char *filename;
  const MU_OPT options[] = {
    {
     .short_opt	= "g",
     .long_opt	= "goal",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_INT,
     .ibound	= {0, USHRT_MAX},
     .arg	= &goal,
     .arg_help	= "GOAL",
     .help	=
     "the goal formatting width; 0 for "
     "the same as '--width' (the default)"
    },
    {
     .short_opt	= "w",
     .long_opt	= "width",
     .env_var	= "COLUMNS",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_INT,
     .ibound	= {0, USHRT_MAX},
     .int_default = 80,
     .arg	= &width,
     .arg_help	= "WIDTH",
     .help	=
     "the maximum formatting width; 0 for infinite; default 80"
    },
    {
     .short_opt	= "i",
     .long_opt	= "indent",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_INT,
     .ibound	= {0, USHRT_MAX},
     .arg	= &indent,
     .arg_help	= "AMOUNT",
     .help	= "indent text by AMOUNT; default 0"
    },
    {
     .short_opt	= "s",
     .long_opt	= "subindent",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_INT,
     .ibound	= {0, USHRT_MAX},
     .found_opt	= &subindent_given,
     .arg	= &subindent,
     .arg_help	= "AMOUNT",
     .help	=
     "indent lines after the first by AMOUNT; "
     "defaults to the same as '--indent'"
    },
    {
     .short_opt	= "f",
     .long_opt	= "file",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_FILE,
     .file_mode	= "r",
     .arg	= &file,
     .argstr	= &filename,
     .arg_help	= "FILE",
     .help	= "read text to format from FILE"
    },
    {
     .long_opt	= "string",
     .has_arg	= MU_OPT_NONE,
     .found_opt	= &do_string,
     .help	=
     "format the output to a string first, and then print it"
    },
    {
     .short_opt	= "t",
     .long_opt	= "tab-stop",
     .has_arg	= MU_OPT_REQUIRED,
     .arg_type	= MU_OPT_INT,
     .ibound	= {0, USHRT_MAX},
     .int_default = MU_FORMAT_TAB_STOP,
     .arg	= &tab_stop,
     .arg_help	= "WIDTH",
     .help	=
     "the width at which tabs should stop when being expanded "
     "into spaces (default " XSTR(MU_FORMAT_TAB_STOP) ")"
    },
    { 0 }
  };
  int ret, err;
  char *text;
  unsigned short col;
  MU_OPT_CONTEXT *context;

  test_init(argc, argv);

  context =
    TEST_NEW_CONTEXT(MU_OPT_PERMUTE | MU_OPT_BUNDLE, "Format",
		     "[OPTION]... TEXT\n"
		     "[OPTION]... < FILE\n"
		     "[OPTION]... -f FILE", "text formatting",
		     "Format TEXT or standard "
		     "input if not specified.");

  ret = mu_parse_opts(context);
  if (MU_OPT_ERR(ret))
    return 1;
  mu_shift_args(&argc, &argv, ret);

  mu_format_tab_stop = tab_stop;

  if (!subindent_given)
    subindent = indent;

  /* Get the text to format. */
  text = get_text(argc, argv, file, filename, context);
  mu_opt_context_xfree(context);

  if (file && fclose(file))
    mu_die(1, "%s: cannot close %s: %s\n",
	   argv[0], filename, strerror(errno));

  /* Now format the text. We use `err' to save the value of `errno' so
     it doesn't get overwritten by free() calls. */
  col = 0;
  if (do_string) {
    char *formatted = mu_format_string(&col, goal, width, indent,
				       subindent, "%s", text);
    err = errno;
    if (formatted) {
      ret = fputs(formatted, stdout) == EOF;
      err = errno;
      free(formatted);
    }
    else
      ret = 1;
  }
  else {
    ret = mu_format(stdout, &col, goal, width,
		    indent, subindent, "%s", text);
    err = errno;
  }
  free(text);
  if (ret)
    fprintf(stderr, "%s: cannot format text: %s\n",
	    argv[0], strerror(err));

  return !!ret;
}

/***************************\
|* Static helper functions *|
\***************************/

/*
 * Decide whether to call get_text_args() or get_text_file(), and then
 * call it and return the result.
 */
static char *get_text(int argc, char **argv,
		      FILE *stream, const char *name,
		      const MU_OPT_CONTEXT *context) {
  if (stream) {
    if (argc > 1) {
      mu_format_help(stderr, context);
      exit(1);
    }
    return get_text_file(stream, name, argv[0]);
  }
  if (argc > 1)
    return get_text_args(argc, argv);
  return get_text_file(stdin, "<stdin>", argv[0]);
}

/*
 * Join all arguments with spaces and return the result.
 */
static char *get_text_args(int argc, char **argv) {
  char *str;
  size_t size;

  size = 0;
  for (int i = 1; i < argc; i++)
    size += strlen(argv[i]) + 1;
  str = mu_xmalloc(size);
  strcpy(str, argv[1]);
  for (int i = 2; i < argc; i++) {
    strcat(str, " ");
    strcat(str, argv[i]);
  }
  return str;
}

/*
 * Slurp `stream' and return the result.
 */
static char *get_text_file(FILE *stream, const char *name,
			   const char *prog_name) {
  struct buf_list *first, *last;
  char *str, *pos;
  size_t last_size, str_size;

  /* Slurp the file into a buffer list. */
  first = last = mu_xmalloc(sizeof(*first));
  str_size = 1;			/* 1 for the terminating null byte */
  while ((last_size = fread(last->buf, 1, sizeof(last->buf),
			    stream)) == sizeof(last->buf)) {
    str_size += last_size;
    last = last->next = mu_xmalloc(sizeof(*(last->next)));
  }
  str_size += last_size;

  if (ferror(stream))
    mu_die(1, "%s: cannot read from %s: %s\n",
	   prog_name, name, strerror(errno));

  /* Convert the buffer list to a string. */
  pos = str = mu_xmalloc(str_size);
  last->next = NULL;
  while (first) {
    struct buf_list *next = first->next;
    size_t size = next ? sizeof(first->buf) : last_size;

    memcpy(pos, first->buf, size);
    pos += size;

    free(first);
    first = next;
  }
  *pos = '\0';

  return str;
}

/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * test.c -- common functions that can be used by all test programs
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <assert.h>

/* Mu includes */
#include <options.h>
#include <format.h>
#include <safe.h>

#include "test.h"

/* The name the program was invoked as; set by test_init(). */
const char *prog_name = NULL;

/*
 * Static helper functions
 */
static char **split(const char *, int, char **);
static int print_name(void *, char *);
static int print_version(void *, char *);

/*
 * Initialize the test program. This must be called first thing in
 * main().
 */
void test_init(int argc, char **argv) {
  int has_version_opt;
  /* A dummy version option to test if that was given. */
  const MU_OPT vers_opt[] = {
    {
     .short_opt	= "v",
     .long_opt	= "version",
     /* Make this take an optional argument, because we don't want to
	diagnose an argument passed to --version here, but rather when
	the test program calls mu_parse_opts(). */
     .has_arg	= MU_OPT_OPTIONAL,
     .arg_type	= MU_OPT_STRING,
     .found_opt	= &has_version_opt,
    },
    { 0 }
  };
  const char *valgrind;
  char **vg_argv;
  MU_OPT_CONTEXT *context;
  int ret;

  prog_name = argv[0];

  if (getenv("RUN_UNDER_VALGRIND"))
    return;
  valgrind = getenv("VALGRIND");
  if (!valgrind)
    return;

  /* Check if we were given the '--version' option. If we were, we
     don't want to run under valgrind, because we're probably being
     called from AT_TESTED, not as an actual test. */
  context = mu_opt_context_xnew(argc, argv, vers_opt,
				MU_OPT_ALLOW_INVALID);
  ret = mu_parse_opts(context);
  mu_opt_context_xfree(context);
  if (MU_OPT_ERR(ret))
    mu_die(-1, "error while looking for '--version' "
	   "(this should not be happening)");
  if (has_version_opt)
    return;

  /* Set the RUN_UNDER_VALGRIND environment variable so that the child
     knows it was run under Valgrind. */
  if (setenv("RUN_UNDER_VALGRIND", "yes", 0))
    mu_die(-1, "%s: cannot set RUN_UNDER_VALGRIND: %s\n",
	   prog_name, strerror(errno));

  /* We need to run ourselves under Valgrind. Believe it or not, it's
     actually easier to do it here rather than in the testsuite
     script, because of wrapper script and Libtool complications. */
  errno = 0;
  vg_argv = split(valgrind, argc, argv);
  if (!vg_argv) {
    if (errno)
      mu_die(1, "%s: cannot split '%s': %s\n",
	     prog_name, valgrind, strerror(errno));
    /* VALGRIND was empty, so don't run it after all. */
    return;
  }
  execvp(vg_argv[0], vg_argv);
  mu_die(1, "%s: failed to execute '%s': %s\n",
	 prog_name, vg_argv[0], strerror(errno));
}

/*
 * Return a new option parsing context based on the given arguments.
 */
MU_OPT_CONTEXT *test_new_context(int argc, char **argv,
				 const MU_OPT *options, int flags,
				 const char *name, const char *usage,
				 const char *short_desc,
				 const char *desc) {
  /* Extra options to add to `options'. */
  const MU_OPT name_opt[] = {
    {
     .long_opt	= "name",
     .has_arg	= MU_OPT_NONE,
     .callback_none = print_name,
     .help	=
     "print the name that this program was called as and exit"
    },
    { 0 }
  };
  const MU_OPT version_opt[] = {
    {
     .short_opt	= "v",
     .long_opt	= "version",
     .has_arg	= MU_OPT_NONE,
     .callback_none = print_version,
     .cb_data	= (void *)name,
     .help	= "print version information and exit"
    },
    { 0 }
  };
  MU_OPT_CONTEXT *const context =
    mu_opt_context_xnew(argc, argv, options, flags);
  mu_opt_context_add_help(context, usage, short_desc, desc,
			  "Report bugs and test failures to "
			  PACKAGE_BUGREPORT ".", "1", NULL,
			  PACKAGE_STRING " Test Suite", NULL);
  mu_opt_context_xadd_options(context, name_opt, MU_OPT_APPEND);
  mu_opt_context_xadd_help_options(context, MU_HELP_ALL);
  mu_opt_context_xadd_options(context, version_opt, MU_OPT_APPEND);
  return context;
}

/*
 * Get an integer and a string from `str'. Returns zero on success or
 * nonzero on error, in which case an error message will be copied to
 * `err', and '*num' and '*outstr' will be left unchanged.
 */
int get_int_str(int *num, const char **outstr,
		const char *str, char *err) {
  long result;
  char *endptr;

  errno = 0;
  result = strtol(str, &endptr, 0);
  if (result < INT_MIN || result > INT_MAX)
    errno = ERANGE;
  if (errno) {
    snprintf(err, MU_OPT_ERR_MAX, "'%s': %s", str, strerror(errno));
    return 1;
  }
  if (!*endptr) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "'%s': missing second argument", str);
    return 1;
  }

  while (*endptr && isspace(*endptr))
    endptr++;
  if (*endptr != ',') {
    snprintf(err, MU_OPT_ERR_MAX, "'%s': not an integer", str);
    return 1;
  }
  endptr++;

  *num = result;
  *outstr = endptr;
  return 0;
}

/*
 * Get a string and a single character from `str'. The string,
 * returned in '*outstr', will be dynamically allocated and must be
 * freed. Returns zero on success or nonzero on error, in which case
 * an error message will be copied to `err', and '*outstr' and '*c'
 * will be left unchanged.
 */
int get_str_char(char **outstr, char *c, const char *str, char *err) {
  const char *comma;

  comma = strchr(str, ',');
  if (!comma) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "'%s': missing second argument", str);
    return 1;
  }
  if (!comma[1] || comma[2]) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "'%s': must be a single character", comma + 1);
    return 1;
  }

  *outstr = mu_xstrndup(str, comma - str);
  *c = comma[1];
  return 0;
}

/*
 * Parse a string into one or two `size_t's. Returns zero on success
 * or nonzero on error, in which case an error message will be copied
 * to `err' and '*size{,1,2}' will be unchanged.
 */

int get_size_1(size_t *size, const char *str, char *err) {
  ullong_t result;
  char *endptr;

  errno = 0;
  result = strtoullt(str, &endptr, 0);
  if (errno) {
    snprintf(err, MU_OPT_ERR_MAX, "'%s': %s", str, strerror(errno));
    return 1;
  }
  if (!*str || *endptr) {
    snprintf(err, MU_OPT_ERR_MAX, "'%s': not an integer", str);
    return 1;
  }

  *size = result;
  return 0;
}

int get_size_2(size_t *size1, size_t *size2,
	       const char *str, char *err) {
  ullong_t result1, result2;
  char *endptr1, *endptr2;

  if (!*str) {
    strncpy(err, "'': not an integer", MU_OPT_ERR_MAX);
    return 1;
  }
  errno = 0;
  result1 = strtoullt(str, &endptr1, 0);
  if (errno) {
    snprintf(err, MU_OPT_ERR_MAX, "'%s': %s", str, strerror(errno));
    return 1;
  }
  if (!*endptr1) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "'%s': missing second argument", str);
    return 1;
  }

  while (*endptr1 && isspace(*endptr1))
    endptr1++;
  if (*endptr1 != ',') {
    snprintf(err, MU_OPT_ERR_MAX, "'%s': not an integer", str);
    return 1;
  }
  endptr1++;

  errno = 0;
  result2 = strtoullt(endptr1, &endptr2, 0);
  if (errno) {
    snprintf(err, MU_OPT_ERR_MAX, "'%s': %s",
	     endptr1, strerror(errno));
    return 1;
  }
  if (!*endptr1 || *endptr2) {
    snprintf(err, MU_OPT_ERR_MAX, "'%s': not an integer", endptr1);
    return 1;
  }

  *size1 = result1;
  *size2 = result2;
  return 0;
}

/***************************\
|* Static helper functions *|
\***************************/

/*
 * Perform very simple argument splitting on spaces. `append_argc' and
 * `append_argv' specify extra arguments to append after splitting is
 * performed. Returns a dynamically allocated argument vector on
 * success. If an error occured, NULL is returned and `errno' is set
 * to indicate the error. If the returned argument vector would be
 * empty except for `append_argv', NULL is returned and `errno' is
 * left unchanged.
 */
static char **split(const char *arg,
		    int append_argc, char **append_argv) {
  int argc;
  char **argv;
  const char *pos;
  int errno_save;

  errno_save = errno;

  assert(append_argc >= 0);

  /* Strip leading spaces. */
  while (*arg && isspace(*arg)) arg++;

  /* Now get `argc'. */
  argc = 0;
  pos = arg;
  while (*pos) {
    while (*pos && !isspace(*pos)) pos++;
    while (*pos && isspace(*pos)) pos++;
    if (++argc == INT_MAX) {
      errno = E2BIG;
      return NULL;
    }
  }

  if (!argc) {
    errno = errno_save;
    return NULL;
  }

  /* Now get `argv'. +1 for the terminating NULL. */
  argv = malloc(sizeof(*argv) * (argc + append_argc + 1));
  if (!argv)
    return NULL;
  for (int i = 0; i < argc; i++) {
    pos = arg;
    while (*pos && !isspace(*pos)) pos++;
    argv[i] = strndup(arg, pos - arg);
    if (!argv[i]) {
      errno_save = errno;
      while (--i >= 0)
	free(argv[i]);
      free(argv);
      errno = errno_save;
      return NULL;
    }
    arg = pos;
    while (*arg && isspace(*arg)) arg++;
  }
  assert(!*arg);

  /* Now append the rest of the arguments. */
  memcpy(argv + argc, append_argv,
	 sizeof(*append_argv) * append_argc);

  /* And terminate the argument vector. */
  argv[argc + append_argc] = NULL;

  return argv;
}

/*
 * Print the name of the program and exit.
 */
static int print_name(void *data, char *err) {
  if (puts(prog_name) == EOF) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot print program name, '%s': %s",
	     prog_name, strerror(errno));
    return 1;
  }
  exit(0);
}

/*
 * Print version information and exit.
 */
static int print_version(void *data, char *err) {
  const char *name = data;
  unsigned short col = 0;

  if (mu_format(stdout, &col, 0, 70, 0, 0,
"%s: part of the " PACKAGE_STRING " test suite.\n\
Copyright (C) 2020 Asher Gordon <AsDaGo@posteo.net>\n\
\n\
This test suite is part of " PACKAGE_NAME ".\n\
\n"
PACKAGE_NAME " comes with ABSOLUTELY NO WARRANTY. You can \
redistribute and/or modify " PACKAGE_NAME " under the terms of the \
GNU General Public License, version 3 or, at your option, any later \
version.\n\
\n\
For more information, see the file COPYING.\n", name) < 0 ||
      fflush(stdout) == EOF) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot print version information: %s", strerror(errno));
    return 1;
  }
  exit(0);
}

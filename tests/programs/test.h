/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * test.h -- common header to be included by all test programs
 */

#ifndef _TEST_H
#define _TEST_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>

/* Mu includes */
#include <options.h>
#include <format.h>

#define STR(x)	#x
#define XSTR(x)	STR(x)

/* The name the program was invoked as. */
extern const char *prog_name;

/* Define strtoullt() to the next best thing if we don't have
   strtoull(). `ullong_t' is the type that strtoullt() returns, and
   ULLONG_T_MAX is the maximum value a `ullong_t' can hold. */
#ifdef HAVE_STRTOULL
typedef unsigned long long ullong_t;
# define ULLONG_T_MAX ULLONG_MAX
# define PRIdULLT "llu"
# define strtoullt strtoull
#else
typedef unsigned long ullong_t;
# define PRIdULLT "lu"
# ifdef HAVE_STRTOUL
#  define ULLONG_T_MAX ULONG_MAX
#  define strtoullt strtoul
# else
#  define ULLONG_T_MAX LONG_MAX
#  define strtoullt(n, e, b) (ullong_t)strtol(n, e, b)
# endif
#endif

#define _TEST_DESC_START(subject)			\
  "Test " subject ". This program is part of the "	\
  PACKAGE_STRING " test suite."

/* Call test_init() with the appropriate arguments. */
#define TEST_NEW_CONTEXT(flags, name, usage, subject, desc)	\
  test_new_context(argc, argv, options, flags,			\
		   name, usage, "test " subject,		\
		   _TEST_DESC_START(subject) "\n\n" desc)

/* Like TEST_NEW_CONTEXT(), but without a description. */
#define TEST_NEW_CONTEXT_NODESC(flags, name, usage, subject)	\
  test_new_context(argc, argv, options, flags, name, usage,	\
		   "test " subject, _TEST_DESC_START(subject))

void test_init(int, char **);
MU_OPT_CONTEXT *test_new_context(int, char **, const MU_OPT *, int,
				 const char *, const char *,
				 const char *, const char *);
int get_int_str(int *, const char **, const char *, char *);
int get_str_char(char **, char *, const char *, char *);
int get_size_1(size_t *, const char *, char *);
int get_size_2(size_t *, size_t *, const char *, char *);

#endif /* !_TEST_H */

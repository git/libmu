/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * compat.c -- provide some extensions if they aren't available
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <assert.h>

/* So compat.h doesn't define the aliases (because we might need to
   call the libc versions). */
#define _MU_COMPAT_C_INTERNAL
#include "compat.h"

int _mu_asprintf(char **strp, const char *fmt, ...) {
  va_list ap;
  int ret;

  va_start(ap, fmt);
  ret = _mu_vasprintf(strp, fmt, ap);
  va_end(ap);

  return ret;
}

int _mu_vasprintf(char **strp, const char *fmt, va_list ap) {
#ifdef HAVE_VASPRINTF
  return vasprintf(strp, fmt, ap);
#else
  int size, ret;
  va_list ap_copy;

  /* We'll be using this again, so make a copy. */
  va_copy(ap_copy, ap);
  /* Get the size to allocate. */
  size = vsnprintf(NULL, 0, fmt, ap_copy);
  va_end(ap_copy);

  if (size < 0)
    return size;

  size++; /* For '\0' */

  /* Now allocate and write the string. */
  *strp = malloc(sizeof(**strp) * size);
  if (!*strp)
    return -1;
  ret = vsnprintf(*strp, size, fmt, ap);
  assert(ret == size - 1 || ret < 0);

  return ret;
#endif /* !HAVE_VASPRINTF */
}

char *_mu_strchrnul(const char *s, int c) {
#ifdef HAVE_STRCHRNUL
  return strchrnul(s, c);
#else
  /* Don't use strchr() and strlen() because then we might have to
     perform two passes over the string. */
  while (*s) {
    if (*s == (char)c)
      break;
    s++;
  }

  /* Don't warn that we're discarding the `const' qualifier. We need
     the `const' qualifier so that the caller can pass us a `const'
     string, but we don't want to return a `const' string because if
     the caller passed a non-`const' string, we should not promote it
     to `const'. */
  return (char *)s;
#endif /* !HAVE_STRCHRNUL */
}

void *_mu_reallocarray(void *ptr, size_t nmemb, size_t size) {
#ifdef HAVE_REALLOCARRAY
  return reallocarray(ptr, nmemb, size);
#else
  size_t bytes;

  for (bytes = 0; nmemb; nmemb--) {
    if (bytes > SIZE_MAX - size) {
      /* We're only supposed to fail with ENOMEM, according to
	 reallocarray(3), although, EOVERFLOW would be more
	 appropriate. */
      errno = ENOMEM;
      return NULL;
    }
    bytes += size;
  }

  return realloc(ptr, bytes);
#endif /* !HAVE_REALLOCARRAY */
}

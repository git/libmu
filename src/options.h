/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * options.h -- routines for parsing options
 */

#ifndef _MU_OPTIONS_H
#define _MU_OPTIONS_H

#include <stdio.h>
#include <dirent.h>

#include "compat.h"		/* For __attribute__() */

/* The maximum length of an error message. */
#define MU_OPT_ERR_MAX	255

#define MU_OPT_PERMUTE		0b00000001
#define MU_OPT_BUNDLE		0b00000010
#define MU_OPT_CONTINUE		0b00000100
#define MU_OPT_ALLOW_INVALID	0b00001000
#define MU_OPT_IGNORE_POSIX	0b00010000
#define MU_OPT_STOP_AT_ARG	0b00100000

#define MU_HELP_SHORT		0b00000001
#define MU_HELP_LONG		0b00000010
#define MU_HELP_QUESTION_MARK	0b00000100
#define MU_HELP_MAN_SHORT	0b00001000
#define MU_HELP_MAN_LONG	0b00010000
#define MU_HELP_ENV		0b00100000
#define MU_HELP_PREPEND		0b01000000
#define MU_HELP_BOTH		(MU_HELP_SHORT | MU_HELP_LONG)
#define MU_HELP_MAN_BOTH	(MU_HELP_MAN_SHORT | MU_HELP_MAN_LONG)
#define MU_HELP_ALL (MU_HELP_BOTH | MU_HELP_MAN_BOTH | MU_HELP_ENV)

/* Error codes. */
#define MU_OPT_ERR_PARSE	-1
#define MU_OPT_ERR_IO		-2
#define MU_OPT_ERR_CALLBACK 	-3

/* Check if a value is an error code. */
#define MU_OPT_ERR(value) ((value) < 0 && (value) > -4)

enum MU_OPT_HAS_ARG {
  MU_OPT_NONE = 0,
  MU_OPT_OPTIONAL,
  MU_OPT_REQUIRED
};

enum MU_OPT_ARG_TYPE {
  /* Start with 1 so that if this is not specified and it should be,
     an error will be raised rather than just defaulting to
     MU_OPT_BOOL. */
  MU_OPT_BOOL = 1,
  MU_OPT_INT,
  MU_OPT_FLOAT,
  MU_OPT_STRING,
  MU_OPT_FILE,
  MU_OPT_DIRECTORY,
  MU_OPT_ENUM,
  MU_OPT_SUBOPT
};

/* For mu_opt_context_add_options(). */
enum MU_OPT_WHERE { MU_OPT_PREPEND, MU_OPT_APPEND };

/* An enumerator specification for arguments of type MU_OPT_ENUM. */
typedef struct _mu_enum_value {
  const char *name;
  int value;
} MU_ENUM_VALUE;

typedef struct _mu_opt {
  /* This is a category for following options. If this field is
     non-NULL, the rest of the option is ignored. */
  const char *category;

  enum MU_OPT_HAS_ARG has_arg; /* Whether the option has an argument */
  enum MU_OPT_ARG_TYPE arg_type; /* The type of the argument */
  int *found_opt;	/* Whether or not we found this option */
  int *found_arg;	/* Whether or not we found an argument */
  union {
    struct {
      /* Regular option. */
      const char *short_opt;	/* The short option(s) (if any) */
      const char *long_opt;	/* The long option(s) (if any) */
    };
    /* Suboption. */
    const char *subopt_name;	/* The suboption name(s) (if any) */
  };
  const char *env_var;	    /* The environment variable(s) (if any) */
  union {
    struct {
      /* No arguments. */
      int negatable;	       /* Whether the option can be negated */
      /* These are callbacks for options which take no arguments. See
	 below for more details on callbacks. */
      union {
	int (*callback_none)(void *, char *);
	int (*callback_negatable)(int, void *, char *);
      };
      /* Help for the negated option. If this is NULL, it will default
	 to "negate <OPTION>". */
      const char *negated_help;
    };
    struct {
      /* Simple arguments. */
      void *arg;	     /* Option argument */
      const char **argstr;   /* The string form of the argument */
      /* The default values for '*arg'. */
      union {
	int bool_default;
	long int_default;
	double float_default;
	const char *string_default;
	FILE *file_default;
	DIR *dir_default;
	int enum_default;
      };
      /* The callback should return zero on success or nonzero on
	 error, in which case the last argument (which must not be
	 written beyond OPT_ERR_MAX, including the terminating null
	 byte) must be set to a string describing the error. */
      union {
	int (*callback_bool)(int, int, void *, char *);
	int (*callback_int)(int, long, void *, char *);
	int (*callback_float)(int, double, void *, char *);
	int (*callback_string)(int, const char *, void *, char *);
	int (*callback_file)(int, const char *, FILE *, void *, char *);
	int (*callback_directory)(int, const char *, DIR *, void *, char *);
	int (*callback_enum)(int, int, void *, char *);
      };
      union {
	struct {
	  long lower, upper; /* Lower and upper bounds for integers */
	} ibound;
	struct {
	  double lower, upper; /* Lower and upper bounds for floats */
	} fbound;
	const char *file_mode;	/* Mode to pass to fopen() */
	struct {
	  const MU_ENUM_VALUE *enum_values;
	  int enum_case_match;	/* Whether enums should be matched
				   case sensitively. */
	};
      };
    };
    struct {
      /* Suboption arguments. */
      int (*callback_subopt)(int, void *, char *);
      const struct _mu_opt *subopts; /* The suboptions themselves */
    };
  };
  void *cb_data;		/* Data to pass to the callback */
  int (*cb_data_destructor)(void *); /* Free `cb_data' */
  const char *arg_help;	/* Format of the argument (e.g. "FILE", "WxH",
			   etc.) */
  const char *help;	/* Description of the option */
} MU_OPT;

typedef struct _mu_opt_context MU_OPT_CONTEXT;
typedef struct _mu_subopt_context MU_SUBOPT_CONTEXT;

MU_OPT_CONTEXT *mu_opt_context_new(int, char **, const MU_OPT *, int);
MU_OPT_CONTEXT *mu_opt_context_new_with_env(int, char **, char **,
					    const MU_OPT *, int);
int mu_opt_context_free(MU_OPT_CONTEXT *);
void mu_opt_context_set_arg_callback(MU_OPT_CONTEXT *,
				     int (const char *,
					  void *, char *),
				     void *, int (void *));
int mu_opt_context_set_no_prefixes(MU_OPT_CONTEXT *, ...);
int mu_opt_context_set_no_prefix_array(MU_OPT_CONTEXT *, char **);
int mu_parse_opts(MU_OPT_CONTEXT *);
MU_SUBOPT_CONTEXT *mu_subopt_context_new(const char *, const char *,
					 const MU_OPT *);
int mu_subopt_context_free(MU_SUBOPT_CONTEXT *);
int mu_subopt_context_set_no_prefixes(MU_SUBOPT_CONTEXT *, ...);
int mu_subopt_context_set_no_prefix_array(MU_SUBOPT_CONTEXT *,
					  char **);
int mu_parse_subopts(MU_SUBOPT_CONTEXT *);
void mu_shift_args(int *, char ***, int);
int mu_opt_context_shift(MU_OPT_CONTEXT *, int);
int mu_opt_context_add_options(MU_OPT_CONTEXT *, const MU_OPT *,
			       enum MU_OPT_WHERE);
void mu_opt_context_add_help(MU_OPT_CONTEXT *, const char *,
			     const char *, const char *, const char *,
			     const char *, const char *, const char *,
			     const char *);
int mu_opt_context_add_help_options(MU_OPT_CONTEXT *, int);
int mu_format_help(FILE *, const MU_OPT_CONTEXT *);
char *mu_format_help_string(const MU_OPT_CONTEXT *,
			    unsigned short, unsigned short)
  __attribute__((malloc));
int mu_format_help_man(FILE *, const MU_OPT_CONTEXT *);
char *mu_format_help_man_string(const MU_OPT_CONTEXT *)
  __attribute__((malloc));

#endif /* !_MU_OPTIONS_H */

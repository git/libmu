/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * options-internal.h -- internal functions related to option parsing
 * (used in safe.c)
 */

#ifndef _OPTIONS_INTERNAL_H
#define _OPTIONS_INTERNAL_H

#include <stdarg.h>

#include "options.h"

int opt_context_vset_no_prefixes(MU_OPT_CONTEXT *, va_list);
int subopt_context_vset_no_prefixes(MU_SUBOPT_CONTEXT *, va_list);

#endif /* OPTIONS_INTERNAL_H */

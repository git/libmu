/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * safe.h -- versions of standard functions that do not return on
 * error
 */

#ifndef _MU_SAFE_H
#define _MU_SAFE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "compat.h"
#include "format.h"
#include "options.h"

/* For use in __attribute__(). Offset by 3 for `file', `line',
   `func'. */
#define _mu_safe_format_attr(archetype, fmt_ind, first)		\
  format(archetype, (fmt_ind) + 3, (first) ? (first) + 3 : 0)

#define _MU_SAFE_TR_NAME(name) _##name##_line
#define _MU_SAFE_DECL(name, ...)					\
  _MU_SAFE_TR_NAME(name)						\
    (const char *file, int line, const char *func, __VA_ARGS__)

/* Include definitions for the functions declared here. Define
   _MU_SAFE_H_INTERNAL so that safe_defs.h knows it's not just some
   dumb user trying to include it. */
#define _MU_SAFE_H_INTERNAL
#include "safe_defs.h"
/* Undefine _MU_SAFE_H_INTERNAL so that if a dumb user does try to
   include safe_defs.h, safe_defs.h will detect it. */
#undef _MU_SAFE_H_INTERNAL

void _MU_SAFE_DECL(mu_die, int, const char *, ...)
  __attribute__((_mu_safe_format_attr(printf, 2, 3), noreturn));
int _MU_SAFE_DECL(mu_warn, const char *, ...)
  __attribute__((_mu_safe_format_attr(printf, 1, 2)));
void _MU_SAFE_DECL(mu_vdie, int, const char *, va_list)
  __attribute__((_mu_safe_format_attr(printf, 2, 0), noreturn));
int _MU_SAFE_DECL(mu_vwarn, const char *, va_list)
  __attribute__((_mu_safe_format_attr(printf, 1, 0)));

void *_MU_SAFE_DECL(mu_xmalloc, size_t) __attribute__((malloc));
void *_MU_SAFE_DECL(mu_xcalloc, size_t, size_t)
  __attribute__((malloc));
void *_MU_SAFE_DECL(mu_xrealloc, void *, size_t);
void *_MU_SAFE_DECL(mu_xreallocarray, void *, size_t, size_t);
char *_MU_SAFE_DECL(mu_xstrdup, const char *) __attribute__((malloc));
char *_MU_SAFE_DECL(mu_xstrndup, const char *, size_t)
  __attribute__((malloc));
unsigned _MU_SAFE_DECL(mu_xasprintf, char **, const char *, ...)
  __attribute__((_mu_safe_format_attr(printf, 2, 3)));
unsigned _MU_SAFE_DECL(mu_xvasprintf, char **, const char *, va_list)
  __attribute__((_mu_safe_format_attr(printf, 2, 0)));
void _MU_SAFE_DECL(mu_xformat,
		   FILE *, unsigned short *, unsigned short, unsigned short,
		   unsigned short, unsigned short, const char *, ...)
  __attribute__((_mu_safe_format_attr(printf, 7, 8)));
char *_MU_SAFE_DECL(mu_xformat_string,
		    unsigned short *, unsigned short, unsigned short,
		    unsigned short, unsigned short, const char *, ...)
  __attribute__((_mu_safe_format_attr(printf, 6, 7), malloc));
void _MU_SAFE_DECL(mu_xvformat,
		   FILE *, unsigned short *, unsigned short, unsigned short,
		   unsigned short, unsigned short, const char *, va_list)
  __attribute__((_mu_safe_format_attr(printf, 7, 0)));
char *_MU_SAFE_DECL(mu_xvformat_string,
		    unsigned short *, unsigned short, unsigned short,
		    unsigned short, unsigned short, const char *, va_list)
  __attribute__((_mu_safe_format_attr(printf, 6, 0), malloc));
MU_OPT_CONTEXT *_MU_SAFE_DECL(mu_opt_context_xnew,
			      int, char **, const MU_OPT *, int);
MU_OPT_CONTEXT *_MU_SAFE_DECL(mu_opt_context_xnew_with_env,
			      int, char **, char **,
			      const MU_OPT *, int);
void _MU_SAFE_DECL(mu_opt_context_xfree, MU_OPT_CONTEXT *);
void _MU_SAFE_DECL(mu_opt_context_xset_no_prefixes,
		   MU_OPT_CONTEXT *, ...);
void _MU_SAFE_DECL(mu_opt_context_xset_no_prefix_array,
		   MU_OPT_CONTEXT *, char **);
void _MU_SAFE_DECL(mu_opt_context_xadd_options,
		   MU_OPT_CONTEXT *, const MU_OPT *,
		   enum MU_OPT_WHERE);
void _MU_SAFE_DECL(mu_opt_context_xadd_help_options,
		   MU_OPT_CONTEXT *, int);
MU_SUBOPT_CONTEXT *_MU_SAFE_DECL(mu_subopt_context_xnew,
				 const char *, const char *,
				 const MU_OPT *);
void _MU_SAFE_DECL(mu_subopt_context_xfree, MU_SUBOPT_CONTEXT *);
void _MU_SAFE_DECL(mu_subopt_context_xset_no_prefixes,
		   MU_SUBOPT_CONTEXT *, ...);
void _MU_SAFE_DECL(mu_subopt_context_xset_no_prefix_array,
		   MU_SUBOPT_CONTEXT *, char **);
void _MU_SAFE_DECL(mu_xformat_help, FILE *, const MU_OPT_CONTEXT *);
char *_MU_SAFE_DECL(mu_xformat_help_string,
		    const MU_OPT_CONTEXT *,
		    unsigned short, unsigned short)
  __attribute__((malloc));
void _MU_SAFE_DECL(mu_xformat_help_man,
		   FILE *, const MU_OPT_CONTEXT *);
char *_MU_SAFE_DECL(mu_xformat_help_man_string,
		    const MU_OPT_CONTEXT *)
  __attribute__((malloc));

#endif /* !_MU_SAFE_H */

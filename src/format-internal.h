/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * format-internal.h -- internal formatting functions (used by man
 * page formatting routines in options.c)
 */

#ifndef _FORMAT_INTERNAL_H
#define _FORMAT_INTERNAL_H

#include <stdio.h>
#include <stdarg.h>

#include "compat.h"		/* For __attribute__() */

int vformat(FILE *, unsigned short *, unsigned short,
	    unsigned short, unsigned short,
	    unsigned short, int, const char *, va_list)
  __attribute__((format(printf, 8, 0)));
char *vformat_string(unsigned short *, unsigned short,
		     unsigned short, unsigned short,
		     unsigned short, int, const char *, va_list)
  __attribute__((format(printf, 7, 0)));

#endif /* FORMAT_INTERNAL_H */

/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * format.c -- print nicely formatted text
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>

#include "safe.h"
#include "compat.h"
#include "format.h"
#include "format-internal.h"

/* Get the width of a TAB character at column `col'. */
#define get_tab_width(col)					\
  (mu_format_tab_stop ?						\
   mu_format_tab_stop - ((col) % mu_format_tab_stop) : 0)

struct string_list {
  char *str;
  struct string_list *next;
};

/*
 * Static helper functions
 */
static int format_part(char **, unsigned short *, unsigned short,
		       unsigned short, unsigned short, int, char **);

/* The number of spaces per tab. */
unsigned short mu_format_tab_stop = MU_FORMAT_TAB_STOP;

/*
 * These simply call the corresponding v* functions (see below).
 */

int mu_format(FILE *stream, unsigned short *col,
	      unsigned short goal, unsigned short width,
	      unsigned short indent, unsigned short subindent,
	      const char *fmt, ...) {
  va_list ap;
  int ret;

  va_start(ap, fmt);
  ret = mu_vformat(stream, col, goal, width, indent, subindent, fmt, ap);
  va_end(ap);

  return ret;
}

char *mu_format_string(unsigned short *col, unsigned short goal,
		       unsigned short width, unsigned short indent,
		       unsigned short subindent,
		       const char *fmt, ...) {
  va_list ap;
  char *ret;

  va_start(ap, fmt);
  ret = mu_vformat_string(col, goal, width, indent, subindent, fmt, ap);
  va_end(ap);

  return ret;
}

/*
 * Like vformat() and vformat_string(), but these functions always
 * format tab characters.
 */

int mu_vformat(FILE *stream, unsigned short *col,
	       unsigned short goal, unsigned short width,
	       unsigned short indent, unsigned short subindent,
	       const char *fmt, va_list ap) {
  return
    vformat(stream, col, goal, width, indent, subindent, 1, fmt, ap);
}

char *mu_vformat_string(unsigned short *col, unsigned short goal,
			unsigned short width, unsigned short indent,
			unsigned short subindent,
			const char *fmt, va_list ap) {
  return
    vformat_string(col, goal, width, indent, subindent, 1, fmt, ap);
}

/*
 * These functions are not static because they are used by the man
 * page formatting routines in options.c. However, they are not
 * exported by the library.
 */

/*
 * Print a formatted string to `stream'. Returns zero on success,
 * nonzero on error. See format_part().
 */
int vformat(FILE *stream, unsigned short *col,
	    unsigned short goal, unsigned short width,
	    unsigned short indent, unsigned short subindent,
	    int format_tabs, const char *fmt, va_list ap) {
  char *str, *str_beg, *formatted;
  int ret, errno_save;

  ret = vasprintf(&str, fmt, ap);
  if (ret < 0)
    return ret;

  /* Keep track of the beginning of `str' so we can free() it
     later. */
  str_beg = str;

  /* Print the formatted string. */
  while ((ret = format_part(&formatted, col, goal, width,
			    indent, format_tabs, &str))) {
    if (ret < 0)
      break;

    if (fputs(formatted, stream) == EOF) {
      errno_save = errno;
      free(formatted);
      errno = errno_save;
      break;
    }

    free(formatted);

    if (ret < 2)
      indent = subindent;
  }

  errno_save = errno;
  free(str_beg);
  errno = errno_save;

  return ret;
}

/*
 * Like vformat(), but returns a string instead (or NULL on
 * error). See vformat() and format_part().
 */
char *vformat_string(unsigned short *col, unsigned short goal,
		     unsigned short width, unsigned short indent,
		     unsigned short subindent, int format_tabs,
		     const char *fmt, va_list ap) {
  char *str, *str_beg;
  size_t size, index;
  struct string_list *string_list, *pos;
  int ret, errno_save;

  ret = vasprintf(&str, fmt, ap);
  if (ret < 0)
    return NULL;

  pos = string_list = malloc(sizeof(*string_list));
  if (!string_list) {
    errno_save = errno;
    free(str);
    errno = errno_save;
    return NULL;
  }

  /* Keep track of the beginning of `str' so we can free() it
     later. */
  str_beg = str;

  /* Get the list of strings to be concatenated. */
  while ((ret = format_part(&(pos->str), col, goal, width,
			    indent, format_tabs, &str))) {
    if (ret == -1)
      break;

    pos->next = malloc(sizeof(*(pos->next)));
    if (!pos->next) {
      errno_save = errno;
      free(pos->str);
      errno = errno_save;
      break;
    }
    pos = pos->next;

    if (ret < 2)
      indent = subindent;
  }

  pos->next = NULL;

  errno_save = errno;
  free(str_beg);

  if (ret) {
    struct string_list *old = string_list;

    /* The current 'string_list->str' is not allocated. */
    string_list = string_list->next;
    free(old);
    while (string_list) {
      old = string_list;
      string_list = string_list->next;
      free(old->str);
      free(old);
    }

    errno = errno_save;

    return NULL;
  }

  /* Get the size of the string. */
  size = 0;
  for (pos = string_list; pos->next; pos = pos->next)
    size += strlen(pos->str);
  size++;			/* For '\0' */

  /* Construct the string. */
  index = 0;
  str = malloc(sizeof(*str) * size);
  errno_save = errno;		/* free() might set `errno' */
  while (string_list) {
    struct string_list *old = string_list;

    if (str && string_list->next) {
      size_t len = strlen(string_list->str);
      memcpy(str + index, string_list->str, len);
      index += len;
    }

    string_list = string_list->next;
    if (string_list)
      free(old->str);
    free(old);
  }
  errno = errno_save;
  if (str) {
    assert(index == size - 1);
    str[index] = '\0';
  }

  return str;
}

/***************************\
|* Static helper functions *|
\***************************/

/*
 * Format a part of a string, '*str' and return it as an allocated
 * string in '*formatted', while staying in the constraints of `width'
 * (or no constraints if `width' is 0). If `goal' is nonzero, it will
 * be the goal width, i.e., the width at which words will be wrapped
 * if possible, but extending beyond that to `width' if that isn't
 * possible. '*col' should be initialized to zero and is used to keep
 * track of the current column. `indent' will be used to indent the
 * current line of text. If `format_tabs' is nonzero, tabs will be
 * replaced by spaces, according to `mu_format_tab_stop'. Otherwise,
 * tabs will be output verbatim. Note that when `format_tabs' is zero,
 * tabs are treated as a single character, which may cause wrapping
 * problems.
 *
 * Returns zero if we have finished formatting the entire `str', 1 if
 * we still need to format some, 2 if we still need to format some and
 * the `indent' should be kept the same, or `-1' on error in which
 * case `errno' will be set to indicate the error. If -1 or zero is
 * returned, '*formatted' will not be allocated.
 *
 * This algorithm only makes a half-hearted attempt to preserve manual
 * indentation. That part could be improved.
 */
static int format_part(char **formatted, unsigned short *col,
		       unsigned short goal, unsigned short width,
		       unsigned short indent, int format_tabs,
		       char **str) {
  char *pos;
  size_t size, index;
  /* The number of spaces required to reach `indent'. */
  unsigned short n_indent;
  enum { SPLIT_NONE = 0, SPLIT_SPACE, SPLIT_WORD } split;

  if (!col || (width && (indent >= width || goal > width))) {
    errno = EINVAL;
    return -1;
  }

  if (!**str)
    return 0;

  if (**str == '\n') {
    /* Add the newlines. */
    for (size = 0; **str == '\n'; (*str)++)
      size++;
    *formatted = malloc(sizeof(**formatted) * (size + 1));
    if (!*formatted)
      return -1;
    memset(*formatted, '\n', size);
    (*formatted)[size] = '\0';
    *col = 0;
    return 1;
  }

  if (width && *col >= width) {
    /* The width must have changed since we were last called with this
       `col'. So simply add a newline. */
    *formatted = strdup("\n");
    if (!*formatted)
      return -1;
    *col = 0;
    return 2;
  }

  /* Get the size of the current string. */
  split = SPLIT_NONE;
  size = n_indent = (*col < indent) ? indent - *col : 0;
  for (pos = *str; *pos && *pos != '\n'; pos++) {
    unsigned short char_width;

    /* We don't handle non-printable characters except TAB (which is
       considered non-printable). */
    if (*pos != '\t' && !isprint(*pos)) {
      errno = EINVAL;
      return -1;
    }

    if (format_tabs && *pos == '\t')
      char_width = get_tab_width(*col + size);
    else
      char_width = 1;

    if (width && size + char_width > width - *col) {
      split = isspace(*pos) ? SPLIT_SPACE : SPLIT_WORD;
      break;
    }
    if (goal && size + char_width > goal - *col) {
      /* We've reached the goal width. Split if we're on whitespace or
	 else as long as the word doesn't take up the whole line. */
      if (isspace(*pos)) {
	split = SPLIT_SPACE;
	break;
      }
      else {
	const char *s = pos;

	while (s > *str && !isspace(*s)) s--;

	/* If 's == *str', the word takes up the whole
	   line. Otherwise, we might need to split it. */
	if (s > *str) {
	  while (s > *str && isspace(*s)) s--;

	  /* If 's == *str && isspace(*s)', the word takes up the
	     whole line. Otherwise, split the line. */
	  if (s > *str || !isspace(*s)) {
	    split = SPLIT_WORD;
	    break;
	  }
	}
      }
    }

    size += char_width;
  }

  /* Allocate the string. '+ !!split' because we will add a newline if
     `split'. */
  *formatted = malloc(sizeof(**formatted) * (size + 1 + !!split));
  if (!*formatted)
    return -1;

  /* Indent with spaces. */
  memset(*formatted, ' ', n_indent);

  /* Copy over the unformatted string, converting tabs to spaces. */
  index = n_indent;
  for (pos = *str; *pos && *pos != '\n'; pos++) {
    if (format_tabs && *pos == '\t') {
      unsigned short tab_width = get_tab_width(*col + index);

      /* Make sure we don't exceed the `width' constraint. */
      if (index + tab_width > size) {
	assert(goal || width);
	if (goal)
	  assert(index + tab_width > goal - *col);
	else
	  assert(index + tab_width > width - *col);
	break;
      }

      memset(*formatted + index, ' ', tab_width);
      index += tab_width;
    }
    else {
      /* Make sure we don't exceed the `width' or `goal'
	 constraint. */
      if (index + 1 > size) {
	assert(goal || width);
	if (goal)
	  assert(index + 1 > goal - *col);
	else if (width)
	  assert(index + 1 > width - *col);
	break;
      }

      (*formatted)[index++] = *pos;
    }
  }
  assert(index == size);
  (*formatted)[index] = '\0';

  if (split == SPLIT_WORD) {
    size_t i, j;

    /* We split a word, so put that word on the next line instead. */
    for (i = size - 1; i && !isspace((*formatted)[i]); i--);
    for (j = i; j && isspace((*formatted)[j]); j--);

    if (j || (i && !isspace((*formatted)[j])) || isspace(**str)) {
      do pos--; while (pos > *str && !isspace(*pos));
      if (isspace(*pos)) pos++;
      size = j + !isspace(**str);
    }
    else {
      /* The word takes up the whole line, so just put a dash at the
	 end if we have room and continue it on the next line. */
      if (size > 1 && indent < width - 1) {
	(*formatted)[size - 1] = '-';
	pos--;
      }
    }
  }
  else if (split == SPLIT_SPACE) {
    /* We split in the middle of some space. Get rid of it. */
    do
      size--;
    while (size && isspace((*formatted)[size]));
    if (!isspace((*formatted)[size]))
      size++;
    while (*pos && isspace(*pos))
      pos++;
  }

  if (split) {
    (*formatted)[size++] = '\n';
    (*formatted)[size] = '\0';
    *col = 0;
  }
  else {
    *col += size;
    if (width)
      assert(*col <= width);
  }

  *str = pos;

  return 1;
}

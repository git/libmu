/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * options.c -- routines for parsing options
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#if defined HAVE_WORKING_FORK && \
  defined HAVE_DUP2 && defined HAVE_FCNTL_H
# define USE_MAN_PIPE 1
#endif

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <libgen.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <signal.h>
#include <dirent.h>
#include <limits.h>
#include <errno.h>
#include <assert.h>
#ifdef USE_MAN_PIPE
# include <fcntl.h>
#endif

#include "sig2str.h"
#include "safe.h"
#include "format-internal.h"
#include "compat.h"
#include "error.h"
#include "options.h"
#include "options-internal.h"

#define MANDATORY_ARGS_MSG			\
  "Mandatory arguments to long options "	\
  "are mandatory for short options too."

#define OPT_END(opt)				\
  (!((opt)->category || (opt)->short_opt ||	\
     (opt)->long_opt || (opt)->env_var))
#define SUBOPT_END(opt) (!((opt)->category || (opt)->subopt_name))
#define ANYOPT_END(opt, is_subopt)			\
  ((is_subopt) ? SUBOPT_END(opt) : OPT_END(opt))

/* Check if an option has suboptions. */
#define HAS_SUBOPTS(opt)			\
  (!(opt)->category && (opt)->has_arg &&	\
   (opt)->arg_type == MU_OPT_SUBOPT)
/* Check if an option is negatable. */
#define NEGATABLE(opt)						\
  (!(opt)->category && !(opt)->has_arg && (opt)->negatable)
/* Check if an option has a callback. */
#define HAS_CALLBACK(opt)					\
  (!(opt)->category &&						\
   (opt)->has_arg ? ((opt)->arg_type == MU_OPT_SUBOPT) ?	\
   !!(opt)->callback_subopt : !!(opt)->callback_bool :		\
   !!(opt)->callback_none)
/* Check whether we can use '*argstr'. */
#define HAS_ARGSTR(opt)					\
  (!(opt)->category && (opt)->has_arg &&		\
   (opt)->arg_type != MU_OPT_SUBOPT && (opt)->argstr)	\

/* Set `opts' to the next alias in `opts'. If there are no more
   aliases, set `opts' to NULL. */
#define NEXT_ALIAS(opts)					\
  ((void)(((opts) = strchr(opts, ALIAS_SEPARATOR)) && (opts)++))

/* Increment a generic pointer by `n' bytes. GCC allows incrementing
   'void *' pointers, but this is a GNU extension. */
#define INC_PTR(ptr, n) ((ptr) = (const char *)(ptr) + n)

/* The argument separator for long options, suboptions, and
   environment variables. */
#define ARG_SEPARATOR '='
/* The separator for suboptions. */
#define SUBOPT_SEPARATOR ','
/* The separator for long option, suboption, and environment variable
   aliases. */
#define ALIAS_SEPARATOR '|'

/* The default line length for usage messages if we can't determine
   the terminal width. */
#define USAGE_DEFAULT_WIDTH 80

/* Internal flags for parsing options. Note: these must not conflict
   with the user-visible flags in options.h. USER_FLAG_MASK is a mask
   which includes all flags which may be specified by users. It is
   used by mu_opt_context_new_with_env() to ensure that no bogus flags
   are given. */
#define USER_FLAG_MASK		0b0000000000111111
#define FLAG_STOP_PARSING	0b0000000001000000
#define FLAG_FIRST		0b0000000010000000
#define FLAG_POSIXLY_CORRECT	0b0000000100000000

/* Flags for format_man_str(). */
#define ARG_HELP		0b00000001
#define USAGE_STRING		0b00000010
#define ESCAPE_ONLY		0b00000100
#define INDENT_PARAGRAPH	0b00001000

/* Types of options. */
enum opt_type {
  OPT_TYPE_SHORT,
  OPT_TYPE_LONG,
  OPT_TYPE_ENV,
  OPT_TYPE_SUBOPT /* Must be last */
};

/* The format in which to output help text. */
enum help_type {
  HELP_TYPE_PLAIN,
  HELP_TYPE_MAN
};

/* A structure that contains the data for parsing options. */
struct _mu_opt_context {
  int argc;
  char **argv;
  char **env;
  MU_OPT *options;
  char **no_prefixes;
  uint16_t flags;
  int index;
  int (*arg_callback)(const char *, void *, char *);
  void *arg_cb_data;
  int (*arg_cb_data_destructor)(void *);

  /* Help data: */
  enum help_type default_format;
  const char *usage, *short_desc, *desc, *notes,
    *section, *sect_name, *pkg_str, *date;
};

/* A structure that contains the data for parsing suboptions by
   themselves. */
struct _mu_subopt_context {
  const char *prog_name;
  char *string;
  MU_OPT *subopts;
  char **no_prefixes;
  char first;
};

typedef union opt_arg {
  int arg_bool;
  long arg_int;
  double arg_float;
  const char *arg_string;
  FILE *arg_file;
  DIR *arg_directory;
  int arg_enum;
} OPT_ARG;

/* Data for mu_opt_context_set_no_prefixes() and
   mu_opt_context_set_no_prefix_array() to pass to
   get_no_prefixes(). */

struct no_prefix_va_list_data {
  va_list ap_start, ap;
};

struct no_prefix_array_data {
  char **strings;
  size_t index;
};

/* A callback to pass to the help formatting functions. It should
   return zero on success or nonzero on error, in which case `errno'
   should be set to indicate the error. */
typedef int FORMAT_HELP_CALLBACK(void *, unsigned short *,
				 unsigned short, unsigned short,
				 unsigned short, unsigned short,
				 int, const char *, ...)
/* Yep, we can put attributes on function typedefs. */
  __attribute__((format(printf, 8, 9)));

/* A list of strings. */
struct string_list {
  char *str;
  struct string_list *next;
};

/* This is used for find_unambiguously(). */
struct match {
  const char *name;
  size_t index;
  const char *no_prefix;
};

/* An API for retrieving various properties from an option or
   enumerated argument. */
struct opt_or_enum_api {
  /* Whether this marks the end of the list. */
  int (*end)(const void *, void *);
  const char *(*name)(const void *, void *);
  int (*negatable)(const void *, void *);
  const char *(*type_name)(const void *, void *);
  /* The size of a single item. */
  size_t size;
};

/* Enumeration values for mu_create_help_option(). */
static const MU_ENUM_VALUE help_type_values[] = {
  { "plain|text",	HELP_TYPE_PLAIN },
  { "man",		HELP_TYPE_MAN   },
  { 0 }
};

/*
 * Static helper functions
 */
static void switch_args(char **, int, int);
static int free_opts(const MU_OPT *, int);
static size_t get_opts_len(const MU_OPT *);
static void init_opts(const char *, char **, const MU_OPT *, int);
static void check_opt_dups(const char *, char **, const MU_OPT *,
			   const MU_OPT *, int);
static void check_enum_dups(const char *, const MU_ENUM_VALUE *,
			    const MU_ENUM_VALUE *, int);
static void check_opt_internal_dups(const char *, char **,
				    const MU_OPT *, int);
static void check_enum_internal_dups(const char *,
				     const MU_ENUM_VALUE *, int);
static void check_dups_generic(const char *, char **, const void *,
			       const void *, int, int,
			       const struct opt_or_enum_api *, void *);
static const char *opt_full_type_name(const void *, void *)
  __attribute__((pure));
static const char *enum_type_name(const void *, void *)
  __attribute__((pure));
static int get_no_prefixes(char ***, void (void *),
			   const char *(void *), void *);
static void no_prefix_va_list_start(void *);
static const char *no_prefix_va_list_iterate(void *);
static void no_prefix_array_start(void *);
static const char *no_prefix_array_iterate(void *);
static void free_no_prefixes(char **);
static int parse_opt(const MU_OPT_CONTEXT *, char *, char *);
static int parse_short_opt(const MU_OPT_CONTEXT *,
			   char *, char *, int);
static int parse_long_opt(const MU_OPT_CONTEXT *,
			  const char *, char *, int);
static int parse_env_vars(const MU_OPT_CONTEXT *);
static int search_env_var(const MU_OPT_CONTEXT *, const MU_OPT *);
static int handle_option(const char *, enum opt_type, const char *,
			 const MU_OPT *, char **, char *, ...);
static int parse_subopts(const char *, char *,
			 const MU_OPT *, char **);
static size_t find_opt(struct match **, int *, const MU_OPT *,
		       const char *, char **, enum opt_type);
static size_t find_enum(struct match **, int *,
			const MU_OPT *, const char *);
static int anyopt_end(const void *, void *);
static const char *do_opt_name(const void *, void *);
static int do_opt_has_no_prefix(const void *, void *);
static int enum_end(const void *, void *);
static const char *enum_name(const void *, void *);
static int enum_negatable(const void *, void *);
static size_t find_unambiguously(struct match **, int *, const void *,
				 const char *, char **, int, int,
				 const struct opt_or_enum_api *,
				 void *);
static int maybecasecmp(const char *, const char *, size_t, int);
static const char *opt_name(const MU_OPT *, enum opt_type)
  __attribute__((pure));
static int opt_has_no_prefix(const MU_OPT *, enum opt_type)
  __attribute__((pure));
static int print_matches(FILE *, const char *,
			 struct match *, size_t);
static int print_alias(FILE *, const char *, int);
static int print_opt_generic(FILE *, const char *,
			     enum opt_type, const char *);
static char *get_alias_str(const char *, int);
static size_t get_alias_len(const char *, int);
static const char *opt_type_name(enum opt_type) __attribute__((pure));
static int match_cmp(const void *, const void *)
  __attribute__((pure));
static char *get_arg_help(int *, char **, const MU_OPT *);
static char *get_enum_arg_help(const MU_ENUM_VALUE *)
  __attribute__((malloc));
static const char *get_arg_separator(const MU_OPT *, enum opt_type)
  __attribute__((pure));
static size_t get_min_indent(const MU_OPT *,
			     enum opt_type, char **, int)
  __attribute__((pure));
static int free_opt_arg(const char *, const MU_OPT *);
static int free_opt_args(const MU_OPT_CONTEXT *);
static int format_help_plain(const MU_OPT_CONTEXT *, char **,
			     FORMAT_HELP_CALLBACK, void *,
			     unsigned short, unsigned short);
static int format_help_man(const MU_OPT_CONTEXT *, char **,
			   FORMAT_HELP_CALLBACK, void *);
static char **get_opt_strings(char *, const MU_OPT_CONTEXT *);
static size_t write_opt_strings(char **, size_t, int *, size_t,
				const MU_OPT *, char **, int);
static size_t write_alias_strings(char **, size_t, int *,
				  size_t, enum opt_type,
				  const char *, const char *);
static char **get_multiple_usage_strings(char **);
static char **get_usage_strings(const char *);
static size_t write_usage_strings(char **, size_t, int *,
				  const char *);
static char **create_usage_list(const char *);
static void free_string_array(char **);
static int print_subopts_header(const MU_OPT *, const char *,
				const char *, const char *,
				FORMAT_HELP_CALLBACK, void *,
				unsigned short *,
				unsigned short, unsigned short);
static int print_aliases(const char *, FORMAT_HELP_CALLBACK, void *,
			 unsigned short *, unsigned short,
			 unsigned short, unsigned short,
			 unsigned short, const char *, const char *,
			 const char *, ...)
  __attribute__((format(printf, 11, 12)));
static int format_opt_help(const MU_OPT *, enum opt_type, char **,
			   int, int *, FORMAT_HELP_CALLBACK, void *,
			   unsigned short *,
			   unsigned short, unsigned short,
			   unsigned short, unsigned short);
static int format_opt_help_maybe_negate(const MU_OPT *, enum opt_type,
					char **, int, int *,
					FORMAT_HELP_CALLBACK, void *,
					unsigned short *,
					unsigned short,
					unsigned short,
					unsigned short,
					unsigned short);
static int format_opt_help_man(const MU_OPT *, enum opt_type, char **,
			       char **, char **, FORMAT_HELP_CALLBACK,
			       void *, unsigned short *);
static int format_opt_help_man_maybe_negate(const MU_OPT *,
					    enum opt_type, char **,
					    char **, char **,
					    FORMAT_HELP_CALLBACK,
					    void *, unsigned short *);
static int has_options_for_category(const MU_OPT *, enum opt_type)
  __attribute__((pure));
static int opt_is_documented(const MU_OPT *, enum opt_type, int)
  __attribute__((pure));
static int print_help_message(const MU_OPT *, enum opt_type, int, int,
			      int, char **, char **,
			      FORMAT_HELP_CALLBACK, void *,
			      unsigned short *,
			      unsigned short, unsigned short,
			      unsigned short, unsigned short);
static int print_aliases_generic(const MU_OPT *, enum opt_type,
				 char **, int, int, const char *,
				 FORMAT_HELP_CALLBACK, void *,
				 unsigned short *, unsigned short,
				 unsigned short, unsigned short,
				 unsigned short);
static char *format_man_str(const char *, char **,
			    char **, char **, int)
  __attribute__((malloc));
static size_t format_man_str_no_alloc(char *, size_t,
				      char **, char **, char **,
				      const char *, size_t, int);
static int write_out(char *, size_t *, size_t, const char *, ...)
  __attribute__((format(printf, 4, 5)));
static size_t find_opt_or_var(const char *, size_t,
			      char **, int, int)
  __attribute__((pure));
static int is_var_char(int);
static int is_word_char(int);
static FORMAT_HELP_CALLBACK format_help_file_callback;
static FORMAT_HELP_CALLBACK format_help_str_callback;
static char *concat_string_list(struct string_list *,
				struct string_list *)
  __attribute__((malloc));
static void free_string_list(struct string_list *,
			     struct string_list *);
static int help_option_callback(int, int, void *, char *);
static int man_option_callback(void *, char *);
#ifdef USE_MAN_PIPE
static void report_err(int, const char *, ...)
  __attribute__((format(printf, 2, 3), noreturn));
static char *get_err(int) __attribute__((malloc));
#endif

/* An API for option lists. */
static const struct opt_or_enum_api option_api =
  { anyopt_end, do_opt_name, do_opt_has_no_prefix,
    opt_full_type_name, sizeof(MU_OPT) };

/* And one for enumerated argument lists. */
static const struct opt_or_enum_api enum_api =
  { enum_end, enum_name, enum_negatable,
    enum_type_name, sizeof(MU_ENUM_VALUE) };

/* Default negation prefixes. */
static char *default_no_prefixes[] = { "no-", NULL };

/*
 * Call mu_opt_context_new_with_env() with `environ' passed as the
 * `env' parameter.
 */
MU_OPT_CONTEXT *mu_opt_context_new(int argc, char **argv,
				   const MU_OPT *options, int flags) {
  extern char **environ;
  return
    mu_opt_context_new_with_env(argc, argv, environ, options, flags);
}

/*
 * Return a new dynamically allocated option context. The returned
 * context must be freed with mu_opt_context_free(). On error, NULL is
 * returned and `errno' will be set to indicate the error.
 */
MU_OPT_CONTEXT *mu_opt_context_new_with_env(int argc, char **argv,
					    char **env,
					    const MU_OPT *options,
					    int flags) {
  MU_OPT_CONTEXT *context;
  size_t size;

  if (argc < 1 || !argv || !options || flags & ~USER_FLAG_MASK) {
    errno = EINVAL;
    return NULL;
  }

  context = calloc(1, sizeof(*context));
  if (!context)
    return NULL;

  /* Copy `options' to 'context->options'. We make a copy so that the
     caller doesn't have to worry about `options' going out of
     scope. */
  /* +1 for the terminating empty option. */
  size = sizeof(*options) * (get_opts_len(options) + 1);
  context->options = malloc(size);
  if (!context->options) {
    const int errno_save = errno;
    free(context);
    errno = errno_save;
    return NULL;
  }
  memcpy(context->options, options, size);

  context->argc		= argc;
  context->argv		= argv;
  context->env		= env;
  context->no_prefixes	= default_no_prefixes;
  context->flags	= flags | FLAG_FIRST;
  context->index	= 1; /* Start at 'argv[1]' */

  if (flags & MU_OPT_STOP_AT_ARG)
    context->flags |= FLAG_POSIXLY_CORRECT;
  else if (!(flags & MU_OPT_IGNORE_POSIX)) {
    if (!env) {
      /* Search the program environment instead. */
      extern char **environ;
      env = environ;
    }

    /* Search for the POSIXLY_CORRECT environment variable in `env',
       and set the FLAG_POSIXLY_CORRECT flag if it's set. */
    while (*env) {
      const char *const wanted = "POSIXLY_CORRECT";
      /* The compiler should optimize this strlen() away. */
      const size_t wanted_len = strlen(wanted);
      const char *const equals = strchr(*env, '=');
      const size_t name_len = equals ? equals - *env : 0;
      if (!name_len)
	mu_die(-1, "%s: '%s': invalid environment variable\n",
	       argv[0], *env);
      if (name_len == wanted_len &&
	  !memcmp(*env, wanted, wanted_len)) {
	context->flags |= FLAG_POSIXLY_CORRECT;
	break;
      }
      env++;
    }
  }

  /* Don't call init_opts() yet, becaus we may still add more
     options. Instead, defer calling that until mu_parse_opts(). */

  return context;
}

/*
 * Free `context' and all data associated with it. Returns zero on
 * success, or nonzero if we couldn't free the options.
 */
int mu_opt_context_free(MU_OPT_CONTEXT *context) {
  int ret = 0, errno_save = 0;

  free_no_prefixes(context->no_prefixes);

  if (free_opts(context->options, 0)) {
    ret = 1;
    errno_save = errno;
  }
  if (context->arg_cb_data && context->arg_cb_data_destructor &&
      context->arg_cb_data_destructor(context->arg_cb_data)) {
    ret = 1;
    errno_save = errno;
  }

  free(context->options);
  free(context);

  if (ret)
    errno = errno_save;
  return ret;
}

/*
 * Set the argument callback for `context'. `callback' is the
 * callback, `data' is data that will be passed to the callback, and
 * `destructor' is the destructor for `data', or NULL. `callback' must
 * not be NULL.
 *
 * Note that the MU_OPT_CONTINUE flag must not be present in
 * `context', and mu_parse_opts() must never have been called on
 * `context'.
 */
void mu_opt_context_set_arg_callback(MU_OPT_CONTEXT *context,
				     int callback(const char *,
						  void *, char *),
				     void *data,
				     int destructor(void *)) {
  assert(callback);
  assert(!(context->flags & MU_OPT_CONTINUE));
  assert(context->flags & FLAG_FIRST);

  context->arg_callback			= callback;
  context->arg_cb_data			= data;
  context->arg_cb_data_destructor	= destructor;
}

/*
 * Set the negation prefixes for `context', which must never have been
 * passed to mu_parse_opts(). Returns zero on success or nonzero on
 * error, in which case `errno' will be set to indicate the
 * error. Note that each argument is copied, not used directly.
 */
int mu_opt_context_set_no_prefixes(MU_OPT_CONTEXT *context, ...) {
  va_list ap;
  int ret;
  va_start(ap, context);
  ret = opt_context_vset_no_prefixes(context, ap);
  va_end(ap);
  return ret;
}

/*
 * Exported for mu_opt_context_xset_no_prefixes() (in safe.c).
 */
int opt_context_vset_no_prefixes(MU_OPT_CONTEXT *context,
				 va_list ap) {
  struct no_prefix_va_list_data data;
  int ret;
  assert(context->flags & FLAG_FIRST);
  va_copy(data.ap_start, ap);
  ret = get_no_prefixes(&(context->no_prefixes),
			no_prefix_va_list_start,
			no_prefix_va_list_iterate, &data);
  va_end(data.ap_start);
  return ret;
}

/*
 * Like mu_opt_context_set_no_prefixes(), but set them from an array
 * rather than a variable argument list. Note that the array
 * (including the strings in it) will be copied, rather than used
 * directly.
 */
int mu_opt_context_set_no_prefix_array(MU_OPT_CONTEXT *context,
				       char **strings) {
  struct no_prefix_array_data data;
  assert(context->flags & FLAG_FIRST);
  data.strings = strings;
  return get_no_prefixes(&(context->no_prefixes),
			 no_prefix_array_start,
			 no_prefix_array_iterate, &data);
}

/*
 * Parse options. If MU_OPT_PERMUTE is present in 'context->flags' and
 * FLAG_POSIXLY_CORRECT is not present, then '*(context->argv)' + the
 * return value will contain only non-option positional arguments
 * (plus 'context->argv[0]') when we're done parsing. Otherwise, if
 * MU_OPT_PERMUTE is not present in 'context->flags' or
 * FLAG_POSIXLY_CORRECT is present, we will stop parsing at the first
 * non-option argument and '*argv' will be left alone.
 *
 * 'context->env' is the environment used for parsing environment
 * variables. If it is NULL, no environment variable parsing will be
 * performed.
 *
 * Returns the number of arguments parsed (options and arguments to
 * those options) on success. You can pass this value to
 * mu_shift_args(). Note that if 'context->arg_callback' is non-NULL,
 * positional arguments will be counted in the return value except
 * when MU_OPT_PERMUTE is also present in 'context->flags'. On error,
 * an error code is returned and an error message will be printed to
 * `stderr'. The last element of 'context->options' must satisfy
 * OPT_END().
 */
int mu_parse_opts(MU_OPT_CONTEXT *context) {
  int ret = 0, nonopt_index = 1, args_parsed = 0;
  size_t i = context->index;
  const int permute = context->flags & MU_OPT_PERMUTE &&
    !(context->flags & FLAG_POSIXLY_CORRECT);

  if (context->flags & FLAG_FIRST) {
    /* Initialize the options. */
    init_opts(context->argv[0], context->no_prefixes,
	      context->options, 0);

    /* Parse the environment first (because command line options
       should take precedence). */
    if (context->env) {
      ret = parse_env_vars(context);
      if (MU_OPT_ERR(ret)) {
	/* Error! */
	free_opt_args(context);
	args_parsed = ret;
	goto done;
      }
    }
  }
  else if (context->flags & MU_OPT_CONTINUE) {
    /* Treat all arguments after the first non-option argument as
       non-option arguments as well if POSIXLY_CORRECT. */
    assert(!context->arg_callback);
    if (context->flags & FLAG_POSIXLY_CORRECT)
      return 0;
  }
  else
    mu_die(-1, "%s: %s() was called more than once without the "
	   "MU_OPT_CONTINUE flag\n", context->argv[0], __func__);

  /* Parse the command line options. */
  for (; i < context->argc; i++) {
    char *optstr = context->argv[i];
    char *arg = (i + 1 < context->argc) ? context->argv[i + 1] : NULL;

    /* Stop if we see "--". */
    if (!(context->flags & FLAG_STOP_PARSING) &&
	!strcmp(optstr, "--")) {
      /* Move "--" back with the other options, but only if
	 MU_OPT_ALLOW_INVALID is *not* passed in `flags'. This is
	 because if MU_OPT_ALLOW_INVALID is passed, we are probably
	 going to get called again with a different set of
	 options. And when we are, we want to see this "--" again so
	 we don't continue parsing options. */
      if (permute && !(context->flags & MU_OPT_ALLOW_INVALID))
	switch_args(context->argv, nonopt_index++, i);
      args_parsed++;

      /* Remember to stop parsing options in case we're going to be
	 called again. */
      context->flags |= FLAG_STOP_PARSING;
    }
    else {
      if (context->flags & FLAG_STOP_PARSING)
	ret = 2;
      else
	/* Parse the option. */
	ret = parse_opt(context, optstr, arg);

      if (ret == 2) {
	/* It's not an option. */
	if (context->flags & FLAG_POSIXLY_CORRECT)
	  context->flags |= FLAG_STOP_PARSING;

	if (context->arg_callback) {
	  char err[MU_OPT_ERR_MAX + 1] = { 0 };

	  if (!context->arg_callback)
	    break;

	  /* Parse it as a non-option argument. */
	  ret =
	    context->arg_callback(optstr, context->arg_cb_data, err);
	  if (!(context->flags &
		(MU_OPT_PERMUTE | MU_OPT_STOP_AT_ARG)))
	    args_parsed++;
	  if (ret) {
	    fprintf(stderr, "%s: %s\n", context->argv[0], err);
	    free_opt_args(context);
	    args_parsed = MU_OPT_ERR_CALLBACK;
	    break;
	  }
	}

	if (permute || context->arg_callback)
	  continue;
	else
	  break;
      }

      args_parsed++;

      if (MU_OPT_ERR(ret)) {
	/* Error! */
	free_opt_args(context);
	args_parsed = ret;
	break;
      }

      if (permute)
	switch_args(context->argv, nonopt_index++, i);

      if (ret == 1) {
	/* For the argument to the option. */
	i++;
	args_parsed++;
	if (permute)
	  switch_args(context->argv, nonopt_index++, i);
      }
    }
  }

 done:
  /* Remember that the next call is no longer our first call and
     update the index. */
  context->flags &= ~FLAG_FIRST;
  context->index = i;

  return args_parsed;
}

/*
 * The following functions are like mu_opt_context_new() et al, but
 * for suboptions. We export these functions in case the user has some
 * strange option parsing needs (for example, suboption-style options
 * in a positional argument or something).
 */

MU_SUBOPT_CONTEXT *mu_subopt_context_new(const char *prog_name,
					 const char *suboptstr,
					 const MU_OPT *subopts) {
  MU_SUBOPT_CONTEXT *context;
  int errno_save;
  size_t size;

  context = malloc(sizeof(*context));
  if (!context)
    return NULL;

  context->string = strdup(suboptstr);
  if (!context->string) {
    errno_save = errno;
    free(context);
    errno = errno_save;
    return NULL;
  }

  /* +1 for the terminating empty option. */
  size = sizeof(*subopts) * (get_opts_len(subopts) + 1);
  context->subopts = malloc(size);
  if (!context->subopts) {
    errno_save = errno;
    free(context->string);
    free(context);
    errno = errno_save;
    return NULL;
  }
  memcpy(context->subopts, subopts, size);

  context->prog_name	= prog_name;
  context->no_prefixes	= default_no_prefixes;
  context->first	= 1;

  return context;
}

int mu_subopt_context_free(MU_SUBOPT_CONTEXT *context) {
  const MU_OPT *const subopts = context->subopts;
  free(context->string);
  free(context);
  return free_opts(subopts, 1);
}

int mu_subopt_context_set_no_prefixes(MU_SUBOPT_CONTEXT *context,
				      ...) {
  va_list ap;
  int ret;
  va_start(ap, context);
  ret = subopt_context_vset_no_prefixes(context, ap);
  va_end(ap);
  return ret;
}

/*
 * Exported for mu_subopt_context_xset_no_prefixes() (in safe.c).
 */
int subopt_context_vset_no_prefixes(MU_SUBOPT_CONTEXT *context,
				    va_list ap) {
  struct no_prefix_va_list_data data;
  int ret;
  assert(context->first);
  va_copy(data.ap_start, ap);
  ret = get_no_prefixes(&(context->no_prefixes),
			no_prefix_va_list_start,
			no_prefix_va_list_iterate, &data);
  va_end(ap);
  return ret;
}

int mu_subopt_context_set_no_prefix_array(MU_SUBOPT_CONTEXT *context,
					  char **strings) {
  struct no_prefix_array_data data;
  assert(context->first);
  data.strings = strings;
  return get_no_prefixes(&(context->no_prefixes),
			 no_prefix_array_start,
			 no_prefix_array_iterate, &data);
}

int mu_parse_subopts(MU_SUBOPT_CONTEXT *context) {
  if (!context->first)
    mu_die(-1, "%s: %s() was called more than once\n",
	   context->prog_name, __func__);
  context->first = 0;
  init_opts(context->prog_name, context->no_prefixes,
	    context->subopts, 1);
  return parse_subopts(context->prog_name, context->string,
		       context->subopts, context->no_prefixes);
}

/*
 * Shift arguments while keeping argv[0] in place.
 */
void mu_shift_args(int *argc, char ***argv, int amount) {
  /* We can't shift arguments by a negative amount, since we update
     '(*argv)[0]'. */
  assert(amount >= 0);
  *argc -= amount;
  *argv += amount;
  (*argv)[0] = (*argv)[-amount];
}

/*
 * Change 'context->index' by `amount', while making sure we stay in
 * range. If the new index is out of range, set it to the closest
 * value. Returns the difference between the new index and the sum of
 * the old index and `amount'. On success, this will be zero.
 */
int mu_opt_context_shift(MU_OPT_CONTEXT *context, int amount) {
  int ret;

  /* Perform the range check in this slightly complicated way to avoid
     overflow. */
  if (amount == INT_MIN || -amount >= context->index) {
    ret = context->index + amount + 1;
    context->index = 1;
    return ret;
  }
  if (amount >= context->argc - context->index) {
    ret = amount - context->argc + context->index + 1;
    context->index = context->argc - 1;
    return ret;
  }

  context->index += amount;
  return 0;
}

/*
 * Add `options' to 'context->options'. If `where' is MU_OPT_PREPEND,
 * prepend the options to 'context->options'. Otherwise, if `where' is
 * MU_OPT_APPEND, append them. Returns zero on success or nonzero on
 * error, in which case `errno' will be set to indicate the error. On
 * error, `context' will be left unchanged.
 */
int mu_opt_context_add_options(MU_OPT_CONTEXT *context,
			       const MU_OPT *options,
			       enum MU_OPT_WHERE where) {
  MU_OPT *new;
  size_t new_len, old_len, add_len, new_size;

  if (where != MU_OPT_PREPEND && where != MU_OPT_APPEND) {
    errno = EINVAL;
    return 1;
  }

  /* Get the various lengths we will need. */
  add_len = get_opts_len(options);
  old_len = get_opts_len(context->options);
  new_len = old_len + add_len;
  /* The allocation size of the new options. */
  new_size = sizeof(*new) * (new_len + 1);

  /* Now allocate size for the new options. */
  new = (where == MU_OPT_APPEND) ?
    realloc(context->options, new_size) : malloc(new_size);
  if (!new)
    return 1;

  /* Copy over the new options. */
  switch (where) {
  case MU_OPT_PREPEND:
    memcpy(new, options, sizeof(*new) * add_len);
    memcpy(new + add_len, context->options, sizeof(*new) * old_len);
    free(context->options);
    break;
  case MU_OPT_APPEND:
    memcpy(new + old_len, options, sizeof(*new) * add_len);
    break;
  }
  /* Terminate the list. */
  memset(new + new_len, 0, sizeof(*new));

  context->options = new;

  return 0;
}

/*
 * Add help data to `context'.
 */
void mu_opt_context_add_help(MU_OPT_CONTEXT *context,
			     const char *usage,
			     const char *short_desc, const char *desc,
			     const char *notes, const char *section,
			     const char *sect_name,
			     const char *pkg_str, const char *date) {
  context->usage		= usage;
  context->short_desc		= short_desc;
  context->desc			= desc;
  context->notes		= notes;
  context->section		= section;
  context->sect_name		= sect_name;
  context->pkg_str		= pkg_str;
  context->date			= date;
}

/*
 * Add help options to `context', according to `flags'. If 'flags &
 * MU_HELP_PREPEND', the options will be prepended to `context',
 * otherwise they will be appended. Returns zero on success or nonzero
 * on error, in which case `errno' will be set to indicate the
 * error. Note that for these options to work correctly, you must call
 * mu_opt_context_add_help() as well.
 */
int mu_opt_context_add_help_options(MU_OPT_CONTEXT *context,
				    int flags) {
  /* We're going to add at most 3 options: the help option, the man
     option, and the environment variable. 4 because we need an extra
     space for the terminating option. */
  MU_OPT options[4] = { 0 };
  MU_OPT *opt = options;

  context->default_format = HELP_TYPE_PLAIN;

  if (flags & (MU_HELP_BOTH | MU_HELP_QUESTION_MARK)) {
#define HELP(msg) "\
print this help in plain text format if 'plain', \
or as a man(1) page if 'man'; if the argument \
is omitted, it will default to" msg "'plain'."
    const char *const help_no_env = HELP(" ");
    const char *const help_env =
      HELP(" the value of the MU_HELP_FORMAT "
	   "environment variable if set, otherwise ");
#undef HELP
    const char *const help =
      (flags & MU_HELP_ENV) ? help_env : help_no_env;

    if (flags & MU_HELP_SHORT)
      opt->short_opt		=
	(flags & MU_HELP_QUESTION_MARK) ? "h?" : "h";
    else if (flags & MU_HELP_QUESTION_MARK)
      opt->short_opt		= "?";
    if (flags & MU_HELP_LONG)
      opt->long_opt		= "help";
    opt->has_arg		= MU_OPT_OPTIONAL;
    opt->arg_type		= MU_OPT_ENUM;
    opt->enum_values		= help_type_values;
    opt->callback_enum		= help_option_callback;
    opt->cb_data		= context;
    opt->help			= help;

    opt++;
  }
  else
    assert(!(flags & MU_HELP_ENV));

  if (flags & MU_HELP_MAN_BOTH) {
    if (flags & MU_HELP_MAN_SHORT)
      opt->short_opt		= "m";
    if (flags & MU_HELP_MAN_LONG)
      opt->long_opt		= "man";
    opt->has_arg		= MU_OPT_NONE;
    opt->callback_none		= man_option_callback;
    opt->cb_data		= context;
    opt->help			= "print this help as a man(1) page";

    opt++;
  }

  if (flags & MU_HELP_ENV) {
    /* This is pretty ugly, but there's not really a better way to do
       it, since we can't dynamically allocate the help string because
       then it will never be freed, and we can't use static memory
       because then evil things will happen if we are called more than
       once. */
#define HELP_START "the default format for "
    const char *const help_short	= HELP_START "-h";
    const char *const help_long		= HELP_START "--help";
    const char *const help_qstn		= HELP_START "-?";
    const char *const help_short_long	= HELP_START "-h, --help";
    const char *const help_short_qstn	= HELP_START "-h, -?";
    const char *const help_long_qstn	= HELP_START "-?, --help";
    const char *const help_all		= HELP_START "-h, -?, --help";
#undef HELP_START
    const char *help;

    if (flags & MU_HELP_SHORT) {
      if (flags & MU_HELP_LONG)
	help = (flags & MU_HELP_QUESTION_MARK) ?
	  help_all : help_short_long;
      else
	help = (flags & MU_HELP_QUESTION_MARK) ?
	  help_short_qstn : help_short;
    }
    else if (flags & MU_HELP_LONG)
      help = (flags & MU_HELP_QUESTION_MARK) ?
	help_long_qstn : help_long;
    else if (flags & MU_HELP_QUESTION_MARK)
      help = help_qstn;
    else
      mu_die(-1, "this should not be happening");

    opt->env_var	= "MU_HELP_FORMAT";
    opt->has_arg	= MU_OPT_OPTIONAL;
    opt->arg_type	= MU_OPT_ENUM;
    opt->enum_values	= help_type_values;
    opt->enum_default	= context->default_format;
    opt->arg		= &(context->default_format);
    opt->help		= help;

    opt++;
  }

  /* Now add the options. */
  return mu_opt_context_add_options(context, options,
				    (flags & MU_HELP_PREPEND) ?
				    MU_OPT_PREPEND : MU_OPT_APPEND);
}

/*
 * Format help output from the data in `context', printing it to
 * `stream'. Returns zero on success, nonzero on error in which case
 * `errno' will be set to indicate the error. Before calling this
 * function, you must call mu_opt_context_add_help().
 */
int mu_format_help(FILE *stream, const MU_OPT_CONTEXT *context) {
  unsigned short goal, width;
  char *columns_str;
  int columns;
  char **usage_list;
  int ret;
#ifdef TIOCGWINSZ
  struct winsize ws;
#endif

  /* COLUMNS should take precedence in case the user wants us to act
     as though we were on a different sized screen (e.g. for testing
     purposes). */
  columns_str = getenv("COLUMNS");
  columns = columns_str ? atoi(columns_str) : 0;

  if (columns < 1) {
    /* Either the COLUMNS environment variable was not set, or it was
       not a valid integer, or it was less than 1. In any case, get the
       terminal width if possible, otherwise using the default width. */
#ifdef TIOCGWINSZ
    if (ioctl(fileno(stream), TIOCGWINSZ, &ws) >= 0)
      columns = ws.ws_col;
    else
#endif
      columns = USAGE_DEFAULT_WIDTH;
  }

  assert(columns > 0);
  if (columns > USHRT_MAX)
    columns = USHRT_MAX;

  goal = width = columns;

  /* Set `goal' to 93% of `width' (same as fmt(1)), unless
     'width * 93 / 100 <= USAGE_DEFAULT_WIDTH', in which case default
     to USAGE_DEFAULT_WIDTH or `width', whichever is smaller. */
  /* Do it like this so it doesn't overflow but we still get more
     accuracy if we can. */
  if (width > USHRT_MAX / 93)
    goal = (width / 100) * 93;
  else
    goal = (width * 93) / 100;
  if (goal <= USAGE_DEFAULT_WIDTH && width > USAGE_DEFAULT_WIDTH)
    goal = USAGE_DEFAULT_WIDTH;

  /* Finally format the help message. */
  usage_list = create_usage_list(context->usage);
  if (!usage_list)
    return 1;
  ret =
    format_help_plain(context, usage_list, format_help_file_callback,
		      stream, goal, width);
  free_string_array(usage_list);
  return ret;
}

/*
 * Format help output from `usage' and `options'. Returns the
 * formatted help (a dynamically allocated string which must be freed)
 * on success or NULL on error, in which case `errno' will be set to
 * indicate the error.
 */
char *mu_format_help_string(const MU_OPT_CONTEXT *context,
			    unsigned short goal,
			    unsigned short width) {
  struct string_list *first, *last;
  char **usage_list;
  int ret;

  first = last = malloc(sizeof(*first));
  if (!first)
    return NULL;

  usage_list = create_usage_list(context->usage);
  if (!usage_list)
    return NULL;

  /* Format the help message. */
  ret =
    format_help_plain(context, usage_list, format_help_str_callback,
		      &last, goal, width);
  free_string_array(usage_list);
  if (ret) {
    free_string_list(first, last);
    return NULL;
  }

  return concat_string_list(first, last);
}

/*
 * Format help output in man(7) format from `usage' and `options',
 * printing it to `file'. Returns zero on success, nonzero on error in
 * which case `errno' will be set to indicate the error.
 */
int mu_format_help_man(FILE *stream, const MU_OPT_CONTEXT *context) {
  int ret;
  char **usage_list;

  usage_list = create_usage_list(context->usage);
  if (!usage_list)
    return 1;
  ret = format_help_man(context, usage_list,
			format_help_file_callback, stream);
  free_string_array(usage_list);
  return ret;
}

/*
 * Format help output in man(7) format from `usage' and
 * `options'. Returns the formatted help (a dynamically allocated
 * string which must be freed) on success or NULL on error, in which
 * case `errno' will be set to indicate the error.
 */
char *mu_format_help_man_string(const MU_OPT_CONTEXT *context) {
  struct string_list *first, *last;
  char **usage_list;
  int ret;

  first = last = malloc(sizeof(*first));
  if (!first)
    return NULL;

  usage_list = create_usage_list(context->usage);
  if (!usage_list)
    return NULL;

  /* Format the help message. */
  ret = format_help_man(context, usage_list,
			format_help_str_callback, &last);
  free_string_array(usage_list);
  if (ret) {
    free_string_list(first, last);
    return NULL;
  }

  return concat_string_list(first, last);
}

/***************************\
|* Static helper functions *|
\***************************/

/*
 * Set 'argv[a]' to 'argv[b]' and then shift the arguments over.
 */
static void switch_args(char **argv, int a, int b) {
  char *old_arg;

  assert(a >= 0);
  assert(b >= 0);

  if (a > b) {
    int c;
    c = a;
    a = b;
    b = c;
  }

  old_arg   = argv[a];
  argv[a++] = argv[b];
  while (a <= b) {
    char *tmp = old_arg;
    old_arg = argv[a];
    argv[a++] = tmp;
  }
}

/*
 * Free all callback data that needs to be freed. Returns zero on
 * success or nonzero if any of the destructor callbacks returned
 * nonzero. `data' will be passed to each destructor as the second
 * argument. All destructors will be called, even if one or more fail.
 */
static int free_opts(const MU_OPT *options, int subopts) {
  int ret = 0, errno_save = 0;

  for (const MU_OPT *opt = options;
       !ANYOPT_END(opt, subopts); opt++) {
    if (HAS_SUBOPTS(opt)) {
      if (subopts)
	mu_die(-1, "suboptions cannot have suboptions");
      if (free_opts(opt->subopts, 1)) {
	ret = 1;
	errno_save = errno;
      }
    }
    else if (opt->cb_data && opt->cb_data_destructor &&
	     opt->cb_data_destructor(opt->cb_data)) {
      ret = 1;
      errno_save = errno;
    }
  }

  if (ret)
    errno = errno_save;
  return ret;
}

/*
 * Return the length of a list of options (not including the
 * terminating empty option).
 */
static size_t get_opts_len(const MU_OPT *opts) {
  size_t len;
  for (len = 0; !OPT_END(opts + len); len++);
  return len;
}

/*
 * Initalize all the `found_opt', `found_arg', and `argstr' fields to
 * zero and the `arg' fields to the default values. Also validate the
 * options.
 */
static void init_opts(const char *prog_name, char **no_prefixes,
		      const MU_OPT *options, int subopts) {
  for (const MU_OPT *opt = options;
       !ANYOPT_END(opt, subopts); opt++) {
    /* Environment variables are not allowed to be negated, so
       specifying `negatable' for an option which only has an
       environment variable should be a hard error. */
    if (!subopts && NEGATABLE(opt) &&
	opt->env_var && !opt->short_opt && !opt->long_opt)
      mu_die(-1, "%s: making '%s' negatable has no effect\n",
	     prog_name, opt->env_var);

    /* Make sure there are no internal duplicates in this option. */
    check_opt_internal_dups(prog_name, no_prefixes, opt, subopts);

    /* And no duplicates between options. */
    for (const MU_OPT *check_opt = opt + 1;
	 !ANYOPT_END(check_opt, subopts); check_opt++)
      check_opt_dups(prog_name, no_prefixes, opt, check_opt, subopts);

    if (opt->found_opt)
      *(opt->found_opt) = 0;
    if (opt->has_arg && opt->found_arg)
      *(opt->found_arg) = 0;

    if (opt->has_arg) {
      OPT_ARG *arg = opt->arg;

      /* Make sure 'opt->arg_help' contains no newlines. */
      if (opt->arg_help) {
	for (const char *p = opt->arg_help; *p; p++) {
	  if (*p == '\n')
	    mu_die(-1, "%s: argument help (\"%s\") cannot "
		   "have newlines\n", prog_name, opt->arg_help);
	}
      }

      switch (opt->arg_type) {
      case MU_OPT_BOOL:
      case MU_OPT_INT:
      case MU_OPT_FLOAT:
      case MU_OPT_STRING:
      case MU_OPT_FILE:
      case MU_OPT_DIRECTORY:
	break;

      case MU_OPT_ENUM:
	/* Check for duplicates in 'opt->enum_values'. Note that
	   duplicate `value's are legitimate, because that is one way
	   to specify aliases. */
	for (const MU_ENUM_VALUE *a = opt->enum_values;
	     a->name; a++) {
	  /* Check for internal dups. */
	  check_enum_internal_dups(prog_name, a,
				   opt->enum_case_match);
	  /* And for duplicates between enumerated argument
	     specifications. */
	  for (const MU_ENUM_VALUE *b = a + 1; b->name; b++)
	    check_enum_dups(prog_name, a, b, opt->enum_case_match);
	}

	break;

      case MU_OPT_SUBOPT:
	if (subopts)
	  mu_die(-1, "%s: cannot have subsuboptions\n", prog_name);
	else
	  init_opts(prog_name, no_prefixes, opt->subopts, 1);
	break;

      default:
	mu_die(-1,
	       "%s: invalid option argument type (type code %u)\n",
	       prog_name, opt->arg_type);
      }

      if (arg) {
	switch (opt->arg_type) {
	case MU_OPT_BOOL:
	  arg->arg_bool = opt->bool_default;
	  break;
	case MU_OPT_INT:
	  arg->arg_int = opt->int_default;
	  break;
	case MU_OPT_FLOAT:
	  arg->arg_float = opt->float_default;
	  break;
	case MU_OPT_STRING:
	  arg->arg_string = opt->string_default;
	  break;
	case MU_OPT_FILE:
	  arg->arg_file = opt->file_default;
	  break;
	case MU_OPT_DIRECTORY:
	  arg->arg_directory = opt->dir_default;
	  break;
	case MU_OPT_ENUM:
	  arg->arg_enum = opt->enum_default;
	  break;
	case MU_OPT_SUBOPT:
	  /* Don't warn on -Wswitch. */
	  break;
	}
      }

      if (HAS_ARGSTR(opt))
	*(opt->argstr) = NULL;
    }
  }
}

/*
 * Check for duplicates between options `a' and `b'. If they are
 * suboptions, `subopts' should be nonzero, zero otherwise.
 */
static void check_opt_dups(const char *prog_name, char **no_prefixes,
			   const MU_OPT *a, const MU_OPT *b,
			   int subopts) {
  enum opt_type type;

  if (subopts) {
    type = OPT_TYPE_SUBOPT;
    check_dups_generic(prog_name, no_prefixes, a, b,
		       1, 0, &option_api, &type);
  }
  else {
    for (type = OPT_TYPE_SHORT; type <= OPT_TYPE_ENV; type++)
      check_dups_generic(prog_name, no_prefixes, a, b, 1,
			 type == OPT_TYPE_SHORT, &option_api, &type);
  }
}

/*
 * Check for duplicates between enumerated argument specifications `a'
 * and `b'. If checking should be case sensitive `case_sensitive'
 * should be nonzero, zero otherwise.
 */
static void check_enum_dups(const char *prog_name,
			    const MU_ENUM_VALUE *a,
			    const MU_ENUM_VALUE *b,
			    int case_sensitive) {
  check_dups_generic(prog_name, NULL, a, b,
		     case_sensitive, 0, &enum_api, NULL);
}

/*
 * Check for internal duplicates in `opt'. If `opt' is a suboption,
 * `subopts' should be nonzero, zero otherwise.
 */
static void check_opt_internal_dups(const char *prog_name,
				    char **no_prefixes,
				    const MU_OPT *opt, int subopts) {
  check_opt_dups(prog_name, no_prefixes, opt, NULL, subopts);
}

/*
 * Check for internal duplicates in `e'. If checking should be case
 * sensitive `case_sensitive' should be nonzero, zero otherwise.
 */
static void check_enum_internal_dups(const char *prog_name,
				     const MU_ENUM_VALUE *e,
				     int case_sensitive) {
  check_enum_dups(prog_name, e, NULL, case_sensitive);
}

/*
 * Check for duplicate items in `a_ptr' and `b_ptr', case sensitively
 * if `case_sensitive'. `api' is the API for both `a_ptr' and
 * `b_ptr'. If `is_short' aliases are single characters. Otherwise,
 * aliases are separated by ALIAS_SEPARATOR. If internal duplicates
 * are being checked, `b_ptr' should be NULL. If a duplicate is found,
 * die with an appropriate message. Otherwise, return.
 */
static void check_dups_generic(const char *prog_name,
			       char **no_prefixes,
			       const void *a_ptr, const void *b_ptr,
			       int case_sensitive, int is_short,
			       const struct opt_or_enum_api *api,
			       void *data) {
  const char *a, *b, *c, *d, *e;
  size_t a_len, c_len, d_len, e_len;
  int internal, a_negatable, b_negatable;
  const void *empty_item;
  const char *empty_name, *empty_type;

  internal = !b_ptr;
  a = api->name(a_ptr, data);
  if (!a)
    return;
  a_negatable = api->negatable(a_ptr, data);
  if (internal) {
    b = a;
    b_negatable = a_negatable;
    if (is_short) {
      if (*b)
	b++;
    }
    else
      NEXT_ALIAS(b);

    /* There can't be any duplicates if there's only one alias! */
    if (!b || !*b)
      return;
  }
  else {
    b = api->name(b_ptr, data);
    if (!b)
      return;
    b_negatable = api->negatable(b_ptr, data);
  }

  for (; a && *a; is_short ? a++ : NEXT_ALIAS(a)) {
    a_len = is_short ? !!*a : strchrnul(a, ALIAS_SEPARATOR) - a;
    if (!a_len) {
      empty_item = a_ptr;
      goto empty;
    }
    /* `negate' is 1 to remove negation prefixes from `a', 2 to remove
       negation prefixes from `b'. */
    for (int negate = 0; negate < 3; negate++) {
      int match;
      size_t no_prefix_len;

      if (negate == 1) {
	if (!b_negatable)
	  continue;

	match = 0;
	for (char **p = no_prefixes; *p; p++) {
	  no_prefix_len = strlen(*p);
	  if (a_len > no_prefix_len &&
	      !maybecasecmp(a, *p, no_prefix_len, case_sensitive)) {
	    match = 1;
	    break;
	  }
	}
	if (!match)
	  continue;

	c = a + no_prefix_len;
	c_len = a_len - no_prefix_len;
      }
      else {
	c = a;
	c_len = a_len;
      }

      for (d = b; d && *d; is_short ? d++ : NEXT_ALIAS(d)) {
	d_len = is_short ? !!*d : strchrnul(d, ALIAS_SEPARATOR) - d;
	if (!d_len) {
	  empty_item = internal ? a_ptr : b_ptr;
	  goto empty;
	}

	if (negate == 2) {
	  if (!a_negatable)
	    break;

	  match = 0;
	  for (char **p = no_prefixes; *p; p++) {
	    no_prefix_len = strlen(*p);
	    if (d_len > no_prefix_len &&
		!maybecasecmp(d, *p, no_prefix_len, case_sensitive)) {
	      match = 1;
	      break;
	    }
	  }
	  if (!match)
	    break;

	  e = d + no_prefix_len;
	  e_len = d_len - no_prefix_len;
	}
	else {
	  e = d;
	  e_len = d_len;
	}

	if (c_len == e_len &&
	    !maybecasecmp(c, e, c_len, case_sensitive)) {
	  /* Make sure we show `b_no_prefix'. */
	  if (negate == 2) {
	    a = d;
	    a_len = d_len;
	  }
	  goto duplicate;
	}
      }
    }
    /* If we're checking for internal duplicates, only check the first
       alias of `a'. */
    if (internal)
      break;
  }

  /* Everything's good; return. */
  return;

 duplicate:
  /* We found a duplicate option. */
  fprintf(stderr, "%s: duplicate %s%s: '", prog_name,
	  api->type_name(a_ptr, data), internal ? " alias" : "");
  fwrite(a, sizeof(*a), a_len, stderr);
  mu_die(-1, "'\n");

 empty:
  /* We found an empty option. */
  empty_name = api->name(empty_item, data);
  empty_type = api->type_name(empty_item, data);
  if (*empty_name)
    mu_die(-1, "%s: empty %s found in '%s'\n",
	   prog_name, empty_type, empty_name);
  else
    mu_die(-1, "%s: empty %s found\n", prog_name, empty_type);
}

/*
 * Return the full name for the type specified in `data'.
 */
static const char *opt_full_type_name(const void *opt, void *data) {
  const enum opt_type *type = data;
  switch (*type) {
  case OPT_TYPE_SHORT:
    return "short option";
  case OPT_TYPE_LONG:
    return "long option";
  case OPT_TYPE_ENV:
    return "environment variable";
  case OPT_TYPE_SUBOPT:
    return "suboption";
  default:
    mu_die(-1, "invalid option type (type code %u)", *type);
  }
}

/*
 * Return "enumerated argument".
 */
static const char *enum_type_name(const void *e, void *d) {
  return "enumerated argument";
}

/*
 * Do the main work for mu_opt_context_set_no_prefixes(), etc. No
 * prefixes will be set in '*no_prefixes_ptr', which will be freed if
 * needed. Returns zero on success or nonzero on error, in which case
 * `errno' will be set to indicate the error. On error,
 * '*no_prefixes_ptr' is left alone.
 */
static int get_no_prefixes(char ***no_prefixes_ptr, void start(void *),
			   const char *iterate(void *), void *data) {
  size_t len, size, ptr_index, str_index;
  const char *str;
  char **no_prefixes;

  /* First count the number of negation prefixes and get the size of
     all the strings together (including the terminating null
     bytes). */
  start(data);
  len = size = 0;
  while ((str = iterate(data))) {
    size += strlen(str) + 1;
    len++;
  }

  /* Now allocate '*no_prefixes' (+1 for the terminating NULL). */
  no_prefixes = malloc(sizeof(*no_prefixes) * (len + 1));
  if (!no_prefixes)
    return 1;
  *no_prefixes = malloc(sizeof(**no_prefixes) * size);
  if (!*no_prefixes) {
    const int errno_save = errno;
    free(no_prefixes);
    errno = errno_save;
    return 1;
  }

  /* Copy over the strings. */
  ptr_index = str_index = 0;
  start(data);
  while ((str = iterate(data))) {
    const size_t str_size = strlen(str) + 1;
    assert(ptr_index < len);
    assert(str_index + str_size <= size);
    no_prefixes[ptr_index] = *no_prefixes + str_index;
    memcpy(no_prefixes[ptr_index], str, str_size);
    ptr_index++;
    str_index += str_size;
  }
  assert(ptr_index == len);
  assert(str_index == size);
  no_prefixes[len] = NULL;

  /* Make sure that there are no duplicates. */
  for (char **p = no_prefixes; *p; p++) {
    for (char **q = p + 1; *q; q++) {
      if (!strcmp(*p, *q)) {
	free_no_prefixes(no_prefixes);
	errno = EINVAL;
	return 1;
      }
    }
  }

  free_no_prefixes(*no_prefixes_ptr);
  *no_prefixes_ptr = no_prefixes;
  return 0;
}

/*
 * An API for getting negation prefixes from a `va_list'.
 */

static void no_prefix_va_list_start(void *data) {
  struct no_prefix_va_list_data *const d = data;
  va_copy(d->ap, d->ap_start);
}

static const char *no_prefix_va_list_iterate(void *data) {
  struct no_prefix_va_list_data *const d = data;
  return va_arg(d->ap, const char *);
}

/*
 * An API for getting negation prefixes from a NULL-terminated array
 * of strings.
 */

static void no_prefix_array_start(void *data) {
  struct no_prefix_array_data *const d = data;
  d->index = 0;
}

static const char *no_prefix_array_iterate(void *data) {
  struct no_prefix_array_data *const d = data;
  return d->strings[d->index++];
}

/*
 * Free a list of negation prefixes, unless it's the default.
 */
static void free_no_prefixes(char **no_prefixes) {
  if (!no_prefixes || no_prefixes == default_no_prefixes)
    return;
  free(*no_prefixes);
  free(no_prefixes);
}

/*
 * Try to parse an option as a short option, and if that doesn't work,
 * try parsing it as a long option instead. Returns 0 on success if
 * `arg' wasn't used, 1 on success if `arg' was used, 2 if it's not an
 * option, and an error code if a fatal error occured (in which case
 * an error message will be printed to `stderr'). `arg' and `optstr'
 * must be writable because we might pass them to parse_subopts().
 */
static int parse_opt(const MU_OPT_CONTEXT *context,
		     char *optstr, char *arg) {
  int ret;

  /* Try to parse it as a short option. */
  ret = parse_short_opt(context, optstr, arg,
			!(context->flags & MU_OPT_BUNDLE));

  if (ret == 4) {
    /* Long options should take precedence, but we should still parse
       it as a short option if no long option was found. */
    ret = parse_long_opt(context, optstr, arg, 1);
    if (ret == 2)
      ret = parse_short_opt(context, optstr, arg, 0);
  }
  else if (ret == 2) {
    /* Didn't work? Try to parse it as a long option. */
    ret = parse_long_opt(context, optstr, arg, 0);
  }

  if (ret == 3) {
    /* 3 means that MU_OPT_ALLOW_INVALID was passed in flags, so we
       should treat invalid options as positional arguments. */
    assert(context->flags & MU_OPT_ALLOW_INVALID);
    ret = 2;
  }

  return ret;
}

/*
 * Parse a short option, returning 0 on sucess if `arg' wasn't used, 1
 * on success if `arg' was used, 2 if it's not a short option, 3 if
 * it's an invalid option and MU_OPT_ALLOW_INVALID was present in
 * 'context->flags', and an error code if a fatal error occured (in
 * which case an error message will be printed to `stderr').  `arg'
 * and `optstr' must be writable because we might pass them to
 * parse_subopts().
 *
 * If `check_req' is nonzero, then parse_short_opt() will return 4 if
 * it matched a short option with a required argument, but it will not
 * call handle_option(). This is used by parse_opt() to determine
 * whether short or long options should take precedence when
 * MU_OPT_BUNDLE is not present in 'context->flags'.
 */
static int parse_short_opt(const MU_OPT_CONTEXT *context,
			   char *optstr, char *arg, int check_req) {
  int used_arg = 0, negate = 0;

  if (*optstr == '+')
    negate = 1;
  else if (*optstr != '-')
    return 2;
  optstr++;
  if (*optstr == '-' || !*optstr)
    return 2;

  for (char c = *optstr; (c = *optstr); optstr++) {
    const MU_OPT *opt;
    char *argstr = NULL;
    int ret;

    /* Find the short option. */
    for (opt = context->options; !OPT_END(opt); opt++) {
      if ((opt)->category || (negate && !NEGATABLE(opt)))
	continue;
      if (opt->short_opt && strchr(opt->short_opt, c))
	break;
    }

    if (OPT_END(opt)) {
      if (!(context->flags & MU_OPT_BUNDLE))
	return 2;		/* It could still be a long option. */

      if (context->flags & MU_OPT_ALLOW_INVALID)
	return 3;
      fprintf(stderr, "%s: '%c%c': invalid option\n",
	      context->argv[0], negate ? '+' : '-', c);
      return MU_OPT_ERR_PARSE;
    }

    /* This is so that parse_short_opt() knows that long options
       should take precedence. But single-character options should
       always be treated as short options. */
    if (check_req && opt->has_arg == MU_OPT_REQUIRED && optstr[1])
      return 4;

    /* Get the argument if there is one. */
    if (optstr[1]) {
      if (opt->has_arg)
	argstr = optstr + 1;
      else if (!(context->flags & MU_OPT_BUNDLE))
	return 2;
    }
    else if (opt->has_arg == MU_OPT_REQUIRED) {
      argstr = arg;
      used_arg = 1;
    }

    ret = handle_option(context->argv[0], OPT_TYPE_SHORT, &c, opt,
			negate ? context->no_prefixes : NULL, argstr);
    if (MU_OPT_ERR(ret))
      return ret;

    if (!(context->flags & MU_OPT_BUNDLE) || opt->has_arg)
      break;
  }

  return used_arg;
}

/*
 * Parse a long option, returning 0 on sucess if `arg' wasn't used, 1
 * on success if `arg' was used, 2 if it's not a long option, 3 if
 * it's an invalid option and MU_OPT_ALLOW_INVALID was present in
 * 'context->flags', and an error code if a fatal error occured (in
 * which case an error message will be printed to `stderr'). `arg'
 * must be writable because we might pass it to
 * parse_subopts(). `could_be_short' should be nonzero if the option
 * might actually be a short option.
 */
static int parse_long_opt(const MU_OPT_CONTEXT *context,
			  const char *optstr, char *arg,
			  int could_be_short) {
  int used_arg = 0, ret;
  struct match *matches;
  size_t n_matches;
  const MU_OPT *opt;
  char *argstr;	/* Must be writable because we might pass it to
		   parse_subopts(). */
  const char *dash, *name;
  int exact_match, negate;

  if (*(optstr++) != '-')
    return 2;

  /* If we're not bundling options, long options can start with a
     single '-' too (or a "--"). */
  if (*optstr == '-') {
    optstr++;
    dash = "--";
    could_be_short = 0;
  }
  else if (context->flags & MU_OPT_BUNDLE)
    return 2;
  else
    dash = "-";

  if (!*optstr)
    return 2;

  /* An option cannot start with ARG_SEPARATOR. */
  if (*optstr == ARG_SEPARATOR) {
    /* Don't check if MU_OPT_ALLOW_INVALID is in `flags', because
       ARG_SEPARATOR should *never* appear in a long option. */
    fprintf(stderr, "%s: '%s%s': invalid option\n",
	    context->argv[0], dash, optstr);
    return MU_OPT_ERR_PARSE;
  }

  /* Search for the option. */
  n_matches = find_opt(&matches, &exact_match, context->options,
		       optstr, context->no_prefixes, OPT_TYPE_LONG);
  if (could_be_short && !exact_match) {
    /* It didn't match exactly, so treat it as a short option. */
    free(matches);
    return 2;
  }
  if (n_matches != 1) {
    if (context->flags & MU_OPT_ALLOW_INVALID) {
      free(matches);
      return 3;
    }

    if (n_matches) {
      fprintf(stderr, "%s: '%s%s': option is ambiguous; "
	      "possibilities:\n", context->argv[0], dash, optstr);
      print_matches(stderr, dash, matches, n_matches);
    }
    else
      fprintf(stderr, "%s: '%s%s': invalid option\n",
	      context->argv[0], dash, optstr);

    free(matches);		/* Come get them while they last! */

    return MU_OPT_ERR_PARSE;
  }

  opt = context->options + matches->index;
  name = matches->name;
  negate = !!matches->no_prefix;

  free(matches);		/* Seriously! */

  argstr = strchr(optstr, ARG_SEPARATOR);
  if (argstr)
    argstr++;
  else if (opt->has_arg == MU_OPT_REQUIRED) {
    argstr = arg;
    used_arg = 1;
  }

  ret =
    handle_option(context->argv[0], OPT_TYPE_LONG, name, opt,
		  negate ? context->no_prefixes : NULL, argstr, dash);
  return MU_OPT_ERR(ret) ? ret : used_arg;
}

/*
 * Parse environment variables. Note that, unlike options, if an
 * unknown environment variable is encountered, it is not treated as
 * an error. Also note that, unlike long options and suboptions,
 * abbreviation is not allowed. Returns zero on success, or an error
 * code on error.
 */
static int parse_env_vars(const MU_OPT_CONTEXT *context) {
  /* Search for environment variables. */
  for (const MU_OPT *opt = context->options; !OPT_END(opt); opt++) {
    int ret = search_env_var(context, opt);
    if (ret)
      return MU_OPT_ERR(ret) ? ret : 0;
    if (HAS_SUBOPTS(opt)) {
      for (const MU_OPT *subopt = opt->subopts;
	   !SUBOPT_END(subopt); subopt++) {
	ret = search_env_var(context, subopt);
	if (ret)
	  return MU_OPT_ERR(ret) ? ret : 0;
      }
    }
  }

  /* No match was found. */
  return 0;
}

/*
 * Search for environment variables specified by `opt' in `env', and
 * call handle_option() if found. Returns zero if no match was found,
 * a positive number if it does match, or an error code if an error
 * occured.
 */
static int search_env_var(const MU_OPT_CONTEXT *context,
			  const MU_OPT *opt) {
  const char *match, *arg;
  char *arg_copy;
  int ret;

  if (opt->category || !opt->env_var)
    return 0;

  /* Search for each alias. Aliases which are specified first take
     higher precedence than those specified after.
     NOTE: This does not handle negation, but environment variables
     cannot be negated anyway. */
  match = NULL;
  for (const char *alias = opt->env_var;
       !match && alias; NEXT_ALIAS(alias)) {
    for (char **var = context->env; !match && *var; var++) {
      const size_t alias_len = get_alias_len(alias, '\0');
      const size_t var_len = strchrnul(*var, ARG_SEPARATOR) - *var;
      if (alias_len == var_len && !memcmp(alias, *var, alias_len))
	match = *var;
    }
  }

  if (!match)
    return 0;

  /* We found a match; handle it. */
  arg = strchr(match, ARG_SEPARATOR);
  if (!arg || arg == match) {
    fprintf(stderr, "%s: '%s': invalid environment variable\n",
	    context->argv[0], match);
    return MU_OPT_ERR_PARSE;
  }
  if (*++arg) {
    if (HAS_SUBOPTS(opt))
      arg_copy = mu_xstrdup(arg);
    else {
      /* Since we know that 'opt->arg_type' is not MU_OPT_SUBOPT (and
	 thus handle_option() won't modify `arg_copy'), we can perform
	 this cast safely. This is actually necessary, in case the
	 `argstr' field is used. */
      arg_copy = (char *)arg;
    }
  }
  else
    arg_copy = NULL;
  ret = handle_option(context->argv[0], OPT_TYPE_ENV,
		      match, opt, 0, arg_copy);
  if (arg_copy && arg_copy != arg)
    free(arg_copy);
  return MU_OPT_ERR(ret) ? ret : 1;
}

/*
 * Handle an option. `name' is the name of the option, as it was
 * passed on the command line. If `type' is OPT_TYPE_SHORT, `name'
 * should point to a single character. Otherwise, `name' will end at
 * either the first ARG_SEPARATOR, ALIAS_SEPARATOR, or null byte;
 * whichever comes first. Returns zero on success, or an error code on
 * error (in which case an error message will be printed to `stderr').
 */
static int handle_option(const char *prog_name, enum opt_type type,
			 const char *name, const MU_OPT *opt,
			 char **no_prefixes, char *argstr, ...) {
  OPT_ARG arg = { 0 };
  OPT_ARG *p_arg = opt->arg ? opt->arg : &arg;
  int ret, cb_ret;
  const char *dash;

  if (type == OPT_TYPE_LONG) {
    va_list ap;
    va_start(ap, argstr);
    dash = va_arg(ap, const char *);
    va_end(ap);
  }
  else
    dash = NULL;

  if (opt->has_arg == MU_OPT_REQUIRED && !argstr) {
    fprintf(stderr, "%s: '", prog_name);
    print_opt_generic(stderr, name, type, dash);
    fprintf(stderr, "': %s requires %s\n", opt_type_name(type),
	    (type == OPT_TYPE_ENV) ? "value" : "argument");
    return MU_OPT_ERR_PARSE;
  }

  /*
   * Process the argument.
   */
  if (argstr) {
    if (opt->has_arg == MU_OPT_NONE) {
      fprintf(stderr, "%s: '", prog_name);
      print_opt_generic(stderr, name, type, dash);
      fprintf(stderr, "': %s %s\n", opt_type_name(type),
	      (type == OPT_TYPE_ENV) ? "cannot have value" :
	      "does not take argument");
      return MU_OPT_ERR_PARSE;
    }

    /* We are going to overwrite the previous argument. */
    if (free_opt_arg(prog_name, opt))
      return 1;

    switch (opt->arg_type) {
      size_t arglen;
      long arg_int;
      char *endptr;
      struct match *matches;
      size_t n_matches;

    case MU_OPT_BOOL:
      arglen = strlen(argstr);
      if (!strncasecmp(argstr, "yes", arglen) ||
	  !strncasecmp(argstr, "true", arglen)) {
	p_arg->arg_bool = 1;
	break;
      }
      if (!strncasecmp(argstr, "no", arglen) ||
	  !strncasecmp(argstr, "false", arglen)) {
	p_arg->arg_bool = 0;
	break;
      }

      /* fall-through */

    case MU_OPT_INT:
      errno = 0;
      arg_int = strtol(argstr, &endptr, 0);

      if (*endptr) {
	fprintf(stderr, "%s: '%s': not a%s\n", prog_name, argstr,
		(opt->arg_type == MU_OPT_BOOL) ?
		" yes or no value" : "n integer");
	return MU_OPT_ERR_PARSE;
      }

      if (opt->arg_type == MU_OPT_INT) {
	if (!errno && (arg_int < opt->ibound.lower ||
		       arg_int > opt->ibound.upper))
	  errno = ERANGE;

	if (errno) {
	  fprintf(stderr, "%s: %s: ", prog_name, argstr);
	  if (errno == ERANGE)
	    fprintf(stderr,
		    "not an integer between %ld and %ld inclusive",
		    opt->ibound.lower, opt->ibound.upper);
	  else
	    fputs(strerror(errno), stderr);
	  putc('\n', stderr);
	  return MU_OPT_ERR_PARSE;
	}
      }

      if (opt->arg_type == MU_OPT_BOOL)
	p_arg->arg_bool = !!arg_int;
      else
	p_arg->arg_int = arg_int;

      break;

    case MU_OPT_FLOAT:
      errno = 0;
      p_arg->arg_float = strtod(argstr, &endptr);

      if (*endptr) {
	fprintf(stderr, "%s: '%s': not a floating-point number\n",
		prog_name, argstr);
	return MU_OPT_ERR_PARSE;
      }

      if (!errno && (p_arg->arg_float < opt->fbound.lower ||
		     p_arg->arg_float > opt->fbound.upper))
	errno = ERANGE;

      if (errno) {
	fprintf(stderr, "%s: %s: ", prog_name, argstr);
	if (errno == ERANGE)
	  fprintf(stderr, "not a floating-point "
		  "number between %g and %g inclusive",
		  opt->fbound.lower, opt->fbound.upper);
	else
	  fputs(strerror(errno), stderr);
	putc('\n', stderr);
	return MU_OPT_ERR_PARSE;
      }

      break;

    case MU_OPT_STRING:
      p_arg->arg_string = argstr;
      break;

    case MU_OPT_FILE:
      if (strcmp(argstr, "-"))
	p_arg->arg_file = fopen(argstr, opt->file_mode);
      else {
	char c;
	const char *mode = opt->file_mode;

	/* Handle stdin/stdout. */
	c = *(mode++);
	if (*mode == 'b')
	  mode++;
	if (*mode) {
	  /* Even if it's '+', it's still not valid for
	     stdin/stdout. */
	  errno = EINVAL;
	  p_arg->arg_file = NULL;
	}

	switch (c) {
	case 'r':
	  p_arg->arg_file = stdin;
	  argstr = "<stdin>";
	  break;

	case 'w':
	case 'a':
	  p_arg->arg_file = stdout;
	  argstr = "<stdout>";
	  break;

	default:
	  errno = EINVAL;
	  p_arg->arg_file = NULL;
	}
      }

      if (!p_arg->arg_file) {
	fprintf(stderr, "%s: cannot open '%s': %s\n",
		prog_name, argstr, strerror(errno));
	return MU_OPT_ERR_IO;
      }

      break;

    case MU_OPT_DIRECTORY:
      p_arg->arg_directory = opendir(argstr);
      if (!p_arg->arg_directory) {
	fprintf(stderr, "%s: cannot open directory '%s': %s\n",
		prog_name, argstr, strerror(errno));
	return MU_OPT_ERR_IO;
      }

      break;

    case MU_OPT_ENUM:
      n_matches = find_enum(&matches, NULL, opt, argstr);
      if (n_matches != 1) {
	if (n_matches) {
	  fprintf(stderr, "%s: '%s': argument for '",
		  prog_name, argstr);
	  print_opt_generic(stderr, name, type, dash);
	  fputs("' is ambiguous; possibilities:\n", stderr);
	  print_matches(stderr, "", matches, n_matches);
	}
	else {
	  const char *comma = ", ";

	  fprintf(stderr, "%s: '%s': invalid argument for '",
		  prog_name, argstr);
	  print_opt_generic(stderr, name, type, dash);
	  putc('\'', stderr);

	  /* Print possibilities, if any. */
	  if (opt->enum_values[0].name) {
	    fputs("; must be ", stderr);
	    if (opt->enum_values[1].name) {
	      if (opt->enum_values[2].name)
		fputs("one of ", stderr);
	      else
		comma = " ";
	    }
	  }
	  for (const MU_ENUM_VALUE *e = opt->enum_values;
	       e->name; e++) {
	    putc('\'', stderr);
	    print_alias(stderr, e->name, '\0');
	    putc('\'', stderr);
	    if (e[1].name) {
	      fputs(comma, stderr);
	      if (!e[2].name)
		fputs("or ", stderr);
	    }
	  }
	  putc('\n', stderr);
	}

	free(matches);

	return MU_OPT_ERR_PARSE;
      }

      p_arg->arg_enum = opt->enum_values[matches->index].value;

      free(matches);

      break;

    case MU_OPT_SUBOPT:
      if (type == OPT_TYPE_SUBOPT)
	mu_die(-1, "%s: cannot have subsuboptions\n", prog_name);

      /* Wait until after we call the callback to parse the
	 suboptions. */
      break;

    default:
      mu_die(-1, "%s: invalid option argument type (type code %u)\n",
	     prog_name, opt->arg_type);
    }
  }

  if (opt->found_opt)
    *(opt->found_opt) = !no_prefixes;
  if (opt->has_arg && opt->found_arg)
    *(opt->found_arg) = !!argstr;

  if (HAS_ARGSTR(opt))
    *(opt->argstr) = argstr;

  /*
   * Call the callback.
   */
  if (HAS_CALLBACK(opt)) {
    /* Initialize to zeros in case the callback forgets to add a
       terminating null byte. +1 for the terminating null byte. */
    char err[MU_OPT_ERR_MAX + 1] = { 0 };

    if (opt->has_arg == MU_OPT_NONE) {
      if (NEGATABLE(opt))
	cb_ret = opt->callback_negatable(!no_prefixes,
					 opt->cb_data, err);
      else
	cb_ret = opt->callback_none(opt->cb_data, err);
    }
    else {
      switch (opt->arg_type) {
      case MU_OPT_BOOL:
	cb_ret = opt->callback_bool(!!argstr, p_arg->arg_bool,
				    opt->cb_data, err);
	break;
      case MU_OPT_INT:
	cb_ret = opt->callback_int(!!argstr, p_arg->arg_int,
				   opt->cb_data, err);
	break;
      case MU_OPT_FLOAT:
	cb_ret = opt->callback_float(!!argstr, p_arg->arg_float,
				     opt->cb_data, err);
	break;
      case MU_OPT_STRING:
	cb_ret = opt->callback_string(!!argstr, p_arg->arg_string,
				      opt->cb_data, err);
	break;
      case MU_OPT_FILE:
	cb_ret = opt->callback_file(!!argstr, argstr, p_arg->arg_file,
				    opt->cb_data, err);
	break;
      case MU_OPT_DIRECTORY:
	cb_ret = opt->callback_directory(!!argstr, argstr,
					 p_arg->arg_directory,
					 opt->cb_data, err);
	break;
      case MU_OPT_ENUM:
	cb_ret = opt->callback_enum(!!argstr, p_arg->arg_enum,
				    opt->cb_data, err);
	break;
      case MU_OPT_SUBOPT:
	cb_ret = opt->callback_subopt(!!argstr, opt->cb_data, err);
	break;
      default:
	mu_die(-1,
	       "%s: invalid option argument type (type code %u)\n",
	       prog_name, opt->arg_type);
      }
    }

    if (cb_ret)
      fprintf(stderr, "%s: %s\n", prog_name, err);
  }
  else
    cb_ret = 0;

  if (!cb_ret && argstr && HAS_SUBOPTS(opt)) {
    /* We already checked for this, but you never know... */
    assert(type != OPT_TYPE_SUBOPT);

    /* Now that we've called the callback for the option, we can parse
       the suboptions (possibly calling callbacks for those). */
    ret = parse_subopts(prog_name, argstr, opt->subopts, no_prefixes);
    if (ret)
      return ret;
  }
  else
    ret = 0;

  if (argstr && !opt->arg) {
    /* So we can modify the `arg' and `argstr' fields. */
    MU_OPT mutable_opt = *opt;
    const char *const_argstr = argstr;
    int free_ret;

    mutable_opt.arg = p_arg;
    mutable_opt.argstr = &const_argstr;

    /* We're not going to return the file/directory we opened, so we
       should close it now. */
    free_ret = free_opt_arg(prog_name, &mutable_opt);
    if (free_ret)
      ret = free_ret;
  }

  return ret ? ret : cb_ret ? MU_OPT_ERR_CALLBACK : 0;
}

/*
 * Parse suboptions. `prog_name' is used for reporting errors. `arg'
 * must be writable, because we overwrite commas in `arg'. Returns 0
 * on success, or an error code on error.
 */
static int parse_subopts(const char *prog_name, char *suboptstr,
			 const MU_OPT *subopts, char **no_prefixes) {
  int ret = 0;
  char *next;
  const MU_OPT *subopt;

  for (char *str = suboptstr; str; str = next) {
    struct match *matches;
    size_t n_matches;
    char *argstr;
    const char *name;
    int negate;

    next = strchr(str, SUBOPT_SEPARATOR);
    if (next)
      *(next++) = '\0';

    /* A suboption cannot start with ARG_SEPARATOR. */
    if (*str == ARG_SEPARATOR) {
      fprintf(stderr, "%s: '%s': invalid suboption\n", prog_name, str);
      ret = MU_OPT_ERR_PARSE;
      break;
    }

    /* Search for the suboption. */
    n_matches = find_opt(&matches, NULL, subopts, str,
			 no_prefixes, OPT_TYPE_SUBOPT);
    if (n_matches != 1) {
      if (n_matches) {
	fprintf(stderr,
		"%s: '%s': suboption is ambiguous; possibilities:\n",
		prog_name, str);
	print_matches(stderr, "", matches, n_matches);
      }
      else
	fprintf(stderr, "%s: '%s': invalid suboption\n",
		prog_name, str);

      free(matches);

      ret = MU_OPT_ERR_PARSE;
      break;
    }

    subopt = subopts + matches->index;
    name = matches->name;
    negate = !!matches->no_prefix;

    free(matches);

    argstr = strchr(str, ARG_SEPARATOR);
    if (argstr)
      argstr++;

    ret = handle_option(prog_name, OPT_TYPE_SUBOPT, name, subopt,
			negate ? no_prefixes : NULL, argstr);
    if (ret)
      break;
  }

  if (ret) {
    /* An error occurred; clean up. */
    for (subopt = subopts; !SUBOPT_END(subopt); subopt++)
      free_opt_arg(prog_name, subopt);
  }

  return ret;
}

/*
 * Find an option (or environment variable or suboption), `needle', in
 * `haystack'. See find_unambiguously() for more details.
 */
static size_t find_opt(struct match **matches, int *exact_match,
		       const MU_OPT *haystack, const char *needle,
		       char **no_prefixes, enum opt_type type) {
  return find_unambiguously(matches, exact_match, haystack,
			    needle, no_prefixes, 1,
			    ARG_SEPARATOR, &option_api, &type);
}

/*
 * Find an enumerator, `needle', in `haystack'. See
 * find_unambiguously() for more details.
 */
static size_t find_enum(struct match **matches, int *exact_match,
			const MU_OPT *opt, const char *needle) {
  return find_unambiguously(matches, exact_match, opt->enum_values,
			    needle, NULL, opt->enum_case_match,
			    '\0', &enum_api, NULL);
}

/*
 * Check if we're at the end of a list of options.
 */
static int anyopt_end(const void *option, void *data) {
  const MU_OPT *opt = option;
  const enum opt_type *type = data;
  return ANYOPT_END(opt, *type == OPT_TYPE_SUBOPT);
}

/*
 * Call opt_name().
 */
static const char *do_opt_name(const void *option, void *data) {
  const MU_OPT *opt = option;
  const enum opt_type *type = data;
  return opt_name(opt, *type);
}

/*
 * Call opt_has_no_prefix().
 */
static int do_opt_has_no_prefix(const void *option, void *data) {
  const MU_OPT *const opt = option;
  const enum opt_type *const type = data;
  return opt_has_no_prefix(opt, *type);
}

/*
 * Check if we're at the end of a list of enumerators.
 */
static int enum_end(const void *enumerator, void *data) {
  return !enum_name(enumerator, data);
}

/*
 * Get the possible aliases of an enumerator.
 */
static const char *enum_name(const void *enumerator, void *data) {
  const MU_ENUM_VALUE *e = enumerator;
  return e->name;
}

/*
 * Return 0 because enumerated arguments cannot be negated.
 */
static int enum_negatable(const void *enumerator, void *data) {
  return 0;
}

/*
 * Find `needle' in `haystack', allowing abbreviations as long as they
 * are not ambiguous. Returns the number of items that were found in
 * `haystack'. `matches' will be allocated and will be a list of
 * indices in `haystack' that point to the found items. If
 * `exact_match' is not NULL, '*exact_match' will be set to nonzero if
 * `needle' matched exactly (not just unambiguously), or 0 if it
 * didn't. Unless `case_sensitive', case will be ignored.
 *
 * `terminator' is a character which can terminate `needle', in
 * addition to ALIAS_SEPARATOR and the null byte. If there is no such
 * character, set it to '\0'.
 *
 * `api' is the API for `haystack'.
 */
static size_t find_unambiguously(struct match **matches,
				 int *exact_match,
				 const void *haystack,
				 const char *needle,
				 char **no_prefixes,
				 int case_sensitive, int terminator,
				 const struct opt_or_enum_api *api,
				 void *data) {
  size_t n_haystack, n_matches, index;
  size_t needle_size;
  size_t n_no_prefixes;
  const void *item;

  if (exact_match)
    *exact_match = 0;

  /* Count the number of negation prefixes. */
  n_no_prefixes = 0;
  if (no_prefixes) {
    while (no_prefixes[n_no_prefixes])
      n_no_prefixes++;
  }

  /* Get the size of the matches and the haystack. */
  n_matches = 0;
  for (n_haystack = 0, item = haystack; !api->end(item, data);
       n_haystack++, INC_PTR(item, api->size)) {
    /* Add one if it has a name, and then add enough for all the
       negation prefixes if it is negatable. */
    if (api->name(item, data)) {
      n_matches++;
      if (api->negatable(item, data))
	n_matches += n_no_prefixes;
    }
  }

  /* Construct the matches. */
  *matches = mu_xcalloc(n_matches, sizeof(**matches));
  index = 0;
  for (size_t i = 0; i < n_haystack; i++) {
    item = (const char *)haystack + (api->size * i);
    if (api->name(item, data)) {
      assert(index < n_matches);
      (*matches)[index++].index = i;

      if (no_prefixes && api->negatable(item, data)) {
	for (char **p = no_prefixes; *p; p++) {
	  assert(index < n_matches);
	  (*matches)[index].index = i;
	  (*matches)[index].no_prefix = *p;
	  index++;
	}
      }
    }
  }
  assert(index == n_matches);

  /* An item that includes ALIAS_SEPARATOR cannot be valid. */
  if (strchr(needle, ALIAS_SEPARATOR))
    return 0;

  /* We stop parsing at `terminator'. */
  needle_size = strchrnul(needle, terminator) - needle;

  /* If it's empty, it's invalid. */
  if (!needle_size)
    return 0;

  /* Search for the needle. */
  for (size_t i = 0; i < needle_size; i++) {
    /* Remove all non-matching items. */
    for (size_t j = 0; j < n_matches;) {
      int found_match = 0;
      const char *needle_check = needle;
      size_t needle_check_size = needle_size;

      if (j >= n_matches)
	break;

      item =
	(const char *)haystack + (api->size * (*matches)[j].index);

      if ((*matches)[j].no_prefix) {
	size_t len = strlen((*matches)[j].no_prefix);
	if (needle_size < len)
	  len = needle_size;
	if (!maybecasecmp(needle, (*matches)[j].no_prefix,
			  len, case_sensitive)) {
	  needle_check += len;
	  needle_check_size -= len;
	}
	else
	  goto not_found;
      }

      for (const char *check = api->name(item, data);
	   check; NEXT_ALIAS(check)) {
	/* Although it seems redundant to check previous characters,
	   this is necessary because if it matched against one alias,
	   that doesn't meen it matches against *this* alias. We could
	   improve efficiency by keeping a table in memory to keep
	   track of which aliases match and which don't, but I don't
	   think performance is that important. */
	if (!maybecasecmp(check, needle_check,
			  (i >= needle_check_size) ?
			  needle_check_size : i + 1,
			  case_sensitive)) {
	  (*matches)[j].name = check;
	  if (i + 1 == needle_check_size &&
	      (!check[i + 1] || check[i + 1] == ALIAS_SEPARATOR)) {
	    /* There is only one possibility, so it's not ambiguous
	       (e.g. "foo" matches "foo" unambiguously out of "foo"
	       and "foo-bar"). */
	    **matches = (*matches)[j];
	    if (exact_match)
	      *exact_match = 1;
	    return 1;
	  }
	  else {
	    found_match = 1;
	    j++;
	    break;
	  }
	}
      }

      if (!found_match) {
      not_found:
	(*matches)[j] = (*matches)[--n_matches];
	/* There's no point in searching any more if all matches are
	   gone. */
	if (!n_matches)
	  return n_matches;
      }
    }
  }

  return n_matches;
}

/*
 * Like memcmp(), but possibly case insensitive if `case_sensitive' is
 * 0.
 */
static int maybecasecmp(const char *a, const char *b,
			size_t n, int case_sensitive) {
  if (case_sensitive)
    return memcmp(a, b, n);

  for (size_t i = 0; i < n; i++) {
    char c = tolower(a[i]);
    char d = tolower(b[i]);
    if (c < d)
      return -1;
    if (c > d)
      return 1;
  }

  return 0;
}

/*
 * Return the name of the option with aliases separated by
 * ALIAS_SEPARATOR, or as single characters if `type' is
 * OPT_TYPE_SHORT.
 */
static const char *opt_name(const MU_OPT *opt, enum opt_type type) {
  if (opt->category)
    return NULL;
  switch (type) {
  case OPT_TYPE_SHORT:
    return opt->short_opt;
  case OPT_TYPE_LONG:
    return opt->long_opt;
  case OPT_TYPE_ENV:
    return opt->env_var;
  case OPT_TYPE_SUBOPT:
    return opt->subopt_name;
  default:
    mu_die(-1, "invalid option type (type code %u)", type);
  }
}

/*
 * Return nonzero if `opt' can be negated, or zero if it can't be.
 */
static int opt_has_no_prefix(const MU_OPT *opt, enum opt_type type) {
  if (!NEGATABLE(opt))
    return 0;

  switch (type) {
  case OPT_TYPE_LONG:
  case OPT_TYPE_SUBOPT:
    return 1;
  case OPT_TYPE_SHORT:
  case OPT_TYPE_ENV:
    return 0;
  default:
    mu_die(-1, "invalid option type (type code %u)", type);
  }
}

/*
 * Print matches to `stream', each prefixed with two spaces and
 * `prefix'. Returns zero on success or nonzero on error, in which
 * case `errno' will be set to indicate the error.
 */
static int print_matches(FILE *stream, const char *prefix,
			 struct match *matches, size_t n_matches) {
  /* Sort the matches so they appear in the same order as they do in
     the help message. */
  qsort(matches, n_matches, sizeof(*matches), match_cmp);
  for (size_t i = 0; i < n_matches; i++) {
    if (fputs("  ", stream) == EOF			||
	fputs(prefix, stream) == EOF			||
	(matches[i].no_prefix &&
	 fputs(matches[i].no_prefix, stream) == EOF)	||
	print_alias(stream, matches[i].name, '\0')	||
	putc('\n', stream) == EOF) {
      return 1;
    }
  }
  return 0;
}

/*
 * Print a single alias to `stream'. Returns zero on success, nonzero
 * on error.
 */
static int print_alias(FILE *stream, const char *aliases,
		       int terminator) {
  size_t len = get_alias_len(aliases, terminator);
  return fwrite(aliases, sizeof(*aliases), len, stream) != len;
}

/*
 * Print a generic option, with a specified type. Returns zero on
 * success, nonzero on error. If `opt' is OPT_TYPE_LONG, `dash' must
 * be specified.
 */
static int print_opt_generic(FILE *stream, const char *opt,
			     enum opt_type type, const char *dash) {
  switch (type) {
  case OPT_TYPE_SHORT:
    return fprintf(stderr, "-%c", *opt) < 0;
  case OPT_TYPE_LONG:
    if (fputs(dash, stderr) == EOF)
      return 1;
    /* fall-through */
  case OPT_TYPE_ENV:
  case OPT_TYPE_SUBOPT:
    return print_alias(stderr, opt, ARG_SEPARATOR);
  default:
    mu_die(-1, "invalid option type (type code %u)", type);
  }
}

/*
 * Return a dynamically allocated copy of the first alias in
 * `aliases', which is null-terminated.
 */
static char *get_alias_str(const char *aliases, int terminator) {
  return mu_xstrndup(aliases, get_alias_len(aliases, terminator));
}

/*
 * Get the length of the first alias in `aliases', stopping at the
 * first ALIAS_SEPARATOR, `terminator', or null byte; whichever comes
 * first.
 */
static size_t get_alias_len(const char *aliases, int terminator) {
  size_t i = 0;

  while (aliases[i] && aliases[i] != ALIAS_SEPARATOR &&
	 aliases[i] != terminator)
    i++;
  return i;
}

/*
 * Return the name of a type of option.
 */
static const char *opt_type_name(enum opt_type type) {
  switch (type) {
  case OPT_TYPE_SHORT:
  case OPT_TYPE_LONG:
    return "option";
  case OPT_TYPE_ENV:
    return "environment variable";
  case OPT_TYPE_SUBOPT:
    return "suboption";
  default:
    mu_die(-1, "invalid option type (type code %u)", type);
  }
}

/*
 * Compare two 'struct match's by index and whether they have a
 * 'no'-prefix. Pass this to qsort().
 */
static int match_cmp(const void *ap, const void *bp) {
  const struct match *a = ap, *b = bp;
  if (a->index > b->index)
    return 1;
  if (a->index < b->index)
    return -1;
  /* The indices are equal, so put non-'no'-prefixed options before
     'no'-prefixed options. */
  if (a->no_prefix && !b->no_prefix)
    return 1;
  if (!a->no_prefix && b->no_prefix)
    return -1;

  /* Well then, they are well and truly equal. In fact, this should
     never happen. */
  mu_die(-1, "matches are unexpectedly equal:\n"
	 "  a: { name = \"%s\", index = %zu, no_prefix = \"%s\" }\n"
	 "  b: { name = \"%s\", index = %zu, no_prefix = \"%s\" }",
	 a->name, a->index, a->no_prefix,
	 b->name, b->index, b->no_prefix);
}

/*
 * Set '*str' to the argument text for an option or a string based on
 * the type of the argument if there is no argument text. If the
 * caller should free the returned string, '*should_free' will be set
 * to nonzero. Otherwise, '*should_free' will be set to zero and
 * '*str' should be treated as 'const'. The return value is '*str'.
 */
static char *get_arg_help(int *should_free, char **str,
			  const MU_OPT *opt) {
  *should_free = 0;

  if (opt->arg_help)
    return *str = (char *)opt->arg_help;

  switch (opt->arg_type) {
  case MU_OPT_BOOL:
    return *str = "Y/N";
  case MU_OPT_INT:
    return *str = "INT";
  case MU_OPT_FLOAT:
    return *str = "FLOAT";
  case MU_OPT_STRING:
    return *str = "STRING";
  case MU_OPT_FILE:
    return *str = "FILE";
  case MU_OPT_DIRECTORY:
    return *str = "DIR";
  case MU_OPT_ENUM:
    *should_free = 1;
    return *str = get_enum_arg_help(opt->enum_values);
  case MU_OPT_SUBOPT:
    /* Note: we could do something similar to get_enum_arg_help()
       here, but then we would have to output stuff like
       "--subopts=foo=FILE|bar[=STRING]". Worse, if a suboption takes
       an enumerated argument, we could have something ambiguous like
       "--subopts=foo=foo|bar|baz". Of course, we could just output
       the name of the suboptions, but that is confusing. So it is
       best to just avoid it all. */
    return *str = "SUBOPTS";
  default:
    mu_die(-1, "invalid option argument type (type code %u)",
	   opt->arg_type);
  }
}

/*
 * Return a dynamically allocated argument string based on
 * `enum_values'. Does not fail.
 */
static char *get_enum_arg_help(const MU_ENUM_VALUE *enum_values) {
  const MU_ENUM_VALUE *e;
  char *str, *pos;
  size_t size;

  size = 0;
  for (e = enum_values; e->name; e++)
    size += get_alias_len(e->name, '\0') + 1;
  pos = str = mu_xmalloc(size);
  e = enum_values;
  while (e->name) {
    size_t len = get_alias_len(e->name, '\0');
    assert(pos + len - str < size);
    memcpy(pos, e->name, len);
    pos += len;
    e++;
    if (e->name) {
      assert(pos + 1 - str < size);
      *pos++ = ALIAS_SEPARATOR;
    }
  }
  assert(pos - str == size - 1);
  *pos = '\0';

  return str;
}

/*
 * Return the string that should be used to separate an option and its
 * argument. `type' is the type of the option.
 */
static const char *get_arg_separator(const MU_OPT *opt,
				     enum opt_type type) {
  switch (type) {
  case OPT_TYPE_SHORT:
  case OPT_TYPE_LONG:
    if (!opt->long_opt)
      break;
    /* fall-through */
  case OPT_TYPE_ENV:
  case OPT_TYPE_SUBOPT:
    return "=";

  default:
    mu_die(-1, "invalid option type (type code %u)", type);
  }

  return (opt->has_arg == MU_OPT_OPTIONAL) ? "" : " ";
}

/*
 * Returns the minimum amount the help text should be indented for
 * this option. Set `short_opts' to nonzero if some of the options are
 * short.
 */
static size_t get_min_indent(const MU_OPT *opt, enum opt_type type,
			     char **no_prefixes, int has_short_opts) {
  size_t opt_len = 0;
  const char *names;
  int dashes = 0;

  if (!opt_has_no_prefix(opt, type))
    no_prefixes = NULL;

  switch (type) {
  case OPT_TYPE_SHORT:
  case OPT_TYPE_LONG:
    names = opt->long_opt;

    if (opt->short_opt) {
      /* Each short option, 's', contributes 4 characters, ", -s",
	 except for the first, which contributes 2 ("-s"). However,
	 keep the extra 2 (for ", ") unless there are no long
	 options. */
      opt_len += strlen(opt->short_opt) * 4;
      if (!opt->long_opt)
	opt_len -= 2;
    }
    else if (opt->long_opt && has_short_opts)
      opt_len += 4;		/* For "    " */

    dashes = 2;			/* For "--" */

    break;

  case OPT_TYPE_ENV:
    names = opt->env_var;
    break;

  case OPT_TYPE_SUBOPT:
    names = opt->subopt_name;
    break;

  default:
    mu_die(-1, "invalid option type (type code %u)", type);
  }

  if (names) {
    char **no_prefix_ptr = no_prefixes;
    while (1) {
      size_t prefix = dashes;
      if (no_prefix_ptr && *no_prefix_ptr)
	prefix += strlen(*no_prefix_ptr);
      opt_len += prefix;
      for (const char *p = names; *p; p++) {
	if (*p == ALIAS_SEPARATOR)
	  opt_len += 2 + prefix;
	else
	  opt_len++;
      }
      if (!no_prefix_ptr || !*no_prefix_ptr)
	break;
      if (no_prefix_ptr)
	no_prefix_ptr++;
    }
  }

  if (opt->has_arg) {
    int should_free;
    char *str;

    /* Add the length of the argument string. */
    opt_len += strlen(get_arg_help(&should_free, &str, opt));
    if (should_free)
      free(str);
    if (type == OPT_TYPE_ENV || type == OPT_TYPE_SUBOPT ||
	opt->long_opt || opt->has_arg == MU_OPT_REQUIRED)
      opt_len++;		/* For ' ' or '=' */
    if (opt->has_arg == MU_OPT_OPTIONAL)
      opt_len += 2;		/* For '[' and ']' */
  }

  /* For the two leading spaces and an extra two spaces between the
     option and its help. */
  opt_len += 4;

  return opt_len;
}

/*
 * Release resources (currently just opened files/directories)
 * associated with 'opt->arg'. Returns zero on success or an error
 * code on error, in which case an error message will be printed to
 * `stderr'.
 */
static int free_opt_arg(const char *prog_name, const MU_OPT *opt) {
  OPT_ARG *arg;

  if (!opt->has_arg)
    return 0;

  arg = opt->arg;
  if (!arg)
    return 0;

  switch (opt->arg_type) {
  case MU_OPT_BOOL:
    arg->arg_bool = 0;
    break;
  case MU_OPT_INT:
    arg->arg_int = 0;
    break;
  case MU_OPT_FLOAT:
    arg->arg_float = 0;
    break;
  case MU_OPT_STRING:
    arg->arg_string = NULL;
    break;

  case MU_OPT_FILE:
    if (arg->arg_file) {
      if (arg->arg_file != stdin && arg->arg_file != stdout &&
	  fclose(arg->arg_file)) {
	fprintf(stderr, "%s: cannot close %s: %s\n", prog_name,
		HAS_ARGSTR(opt) ? *(opt->argstr) : "file",
		strerror(errno));
	return MU_OPT_ERR_IO;
      }
      arg->arg_file = NULL;
    }
    break;

  case MU_OPT_DIRECTORY:
    if (arg->arg_directory) {
      if (closedir(arg->arg_directory)) {
	fprintf(stderr, "%s: cannot close directory%s%s: %s\n",
		prog_name, HAS_ARGSTR(opt) ? " " : "",
		HAS_ARGSTR(opt) ? *(opt->argstr) : "",
		strerror(errno));
	return MU_OPT_ERR_IO;
      }
      arg->arg_directory = NULL;
    }

    break;

  case MU_OPT_ENUM:
    arg->arg_enum = 0;
    break;

  case MU_OPT_SUBOPT:
    break;

  default:
    mu_die(-1, "%s: invalid option argument type (type code %u)\n",
	   prog_name, opt->arg_type);
  }

  if (HAS_ARGSTR(opt))
    *(opt->argstr) = NULL;

  return 0;
}

/*
 * Call free_opt_arg() for each option in 'context->options'. Returns
 * the number of failures.
 */
static int free_opt_args(const MU_OPT_CONTEXT *context) {
  int ret = 0;

  for (const MU_OPT *opt = context->options; !OPT_END(opt); opt++) {
    if (HAS_SUBOPTS(opt)) {
      for (const MU_OPT *subopt = opt->subopts;
	   !SUBOPT_END(subopt); subopt++)
	ret += free_opt_arg(context->argv[0], subopt);
    }
    else
      ret += free_opt_arg(context->argv[0], opt);
  }

  return ret;
}

/*
 * Format help output from `usage' and `options', using `format' to
 * format the message. `data' will be passed to `format' as the first
 * argument. Returns zero on success or nonzero on error, in which
 * case `errno' will be set to indicate the error.
 */
static int format_help_plain(const MU_OPT_CONTEXT *context,
			     char **usage,
			     FORMAT_HELP_CALLBACK format, void *data,
			     unsigned short goal,
			     unsigned short width) {
  unsigned short indent, offset, max_indent, cur_col = 0;
  int short_opts, env_vars;
  int print_mandatory_args_msg, category_newline;
  const MU_OPT *opt, *subopt;
  int first;

  /* The amount we should indent beyond the indentation of the first
     line for following lines. */
  offset = 2;

  /* Give the help text at least 5/8 of the screen (so the options get
     3/8). */
  max_indent = (goal * 3) / 8;
  /* This is possible for very small goals/widths. */
  if (max_indent + offset >= width - 1) {
    if (max_indent >= width)
      max_indent = width - 1;
    if (width - max_indent - 2 >= 0)
      offset = width - max_indent - 2; /* Allow for a continuing '-' */
    else {
      offset = width - max_indent - 1;
      /* Allow for a continuing '-' if there's room. */
      if (max_indent > 0)
	max_indent--;
    }
  }

  /* Format the usage(s). */
  assert(*usage);
  for (first = 1; *usage; usage++, first = 0) {
    if (format(data, &cur_col, 0, 0, 0, 0, 1, "%s %s%s%s\n",
	       first ? "Usage:" : "  or: ", context->argv[0],
	       **usage ? " " : "", *usage)) {
      return 1;
    }
  }

  if ((context->desc && format(data, &cur_col, goal, width,
			      0, 0, 1, "%s\n", context->desc)) ||
      format(data, &cur_col, goal, width, 0, 0, 1, "%s",
	     OPT_END(context->options) ? "" : "\n"))
    return 1;

 /*
  * Check whether there are long options with requried arguments that
  * have equivalent short options, and also whether there are short
  * options and environment variables at all.
  */
  short_opts = env_vars = print_mandatory_args_msg = 0;
  for (opt = context->options; !OPT_END(opt); opt++) {
    if (opt->short_opt) {
      short_opts = 1;
      if (opt->long_opt && opt->has_arg == MU_OPT_REQUIRED)
	print_mandatory_args_msg = 1;
    }

    if (opt->env_var)
      env_vars = 1;
    if (!env_vars && HAS_SUBOPTS(opt)) {
      for (subopt = opt->subopts; !SUBOPT_END(subopt); subopt++) {
	if (subopt->env_var) {
	  env_vars = 1;
	  break;
	}
      }
    }

    if (short_opts && env_vars && print_mandatory_args_msg)
      /* We have all the info we need. */
      break;
  }

  /* Get the amount we should indent. */
  indent = 0;
  for (opt = context->options; !OPT_END(opt); opt++) {
    unsigned short opt_len, max_opt_len = 0;

    if (opt->short_opt || opt->long_opt) {
      opt_len = get_min_indent(opt, OPT_TYPE_LONG,
			       context->no_prefixes, short_opts);
      if (opt_len > max_opt_len)
	max_opt_len = opt_len;
    }
    if (opt->env_var) {
      opt_len =
	get_min_indent(opt, OPT_TYPE_ENV, context->no_prefixes, 0);
      if (opt_len > max_opt_len)
	max_opt_len = opt_len;
    }

    /* Don't count it if it's more than the maximum indent. */
    if (max_opt_len > indent && max_opt_len <= max_indent)
      indent = max_opt_len;

    if (HAS_SUBOPTS(opt)) {
      /* Don't forget the suboptions. */
      for (subopt = opt->subopts; !SUBOPT_END(subopt); subopt++) {
	opt_len = get_min_indent(subopt, OPT_TYPE_SUBOPT,
				 context->no_prefixes, 0);
	if (opt_len > max_opt_len)
	  max_opt_len = opt_len;
	if (subopt->env_var) {
	  opt_len = get_min_indent(subopt, OPT_TYPE_ENV,
				   context->no_prefixes, 0);
	  if (opt_len > max_opt_len)
	    max_opt_len = opt_len;
	}

	/* Don't count it if it's more than the maximum indent. */
	if (max_opt_len > indent && max_opt_len <= max_indent)
	  indent = max_opt_len;
      }
    }
  }

  /* The indent should be at least 4. */
  if (indent < 4) {
    indent = 4;
    /* Unless, of course, it's greater than the maximum indent. */
    if (indent > max_indent)
      indent = max_indent;
  }

  if (print_mandatory_args_msg &&
      format(data, &cur_col, goal, width, 0, 0, 1,
	     MANDATORY_ARGS_MSG "\n"))
    return 1;

  /* Print the help for each option. */
  category_newline = print_mandatory_args_msg;
  for (opt = context->options; !OPT_END(opt); opt++) {
    if (format_opt_help(opt, OPT_TYPE_LONG, context->no_prefixes,
			short_opts, &category_newline, format, data,
			&cur_col, goal, width, indent, offset)) {
      return 1;
    }
  }

  /* Print suboption help after regular options. */
  category_newline = 1;
  for (opt = context->options; !OPT_END(opt); opt++) {
    if (HAS_SUBOPTS(opt)) {
      if (print_subopts_header(opt, "\n", ":\n", "-",
			       format, data, &cur_col, goal, width))
	return 1;

      for (subopt = opt->subopts; !SUBOPT_END(subopt); subopt++) {
	if (format_opt_help(subopt, OPT_TYPE_SUBOPT,
			    context->no_prefixes, 0,
			    &category_newline, format, data,
			    &cur_col, goal, width, indent, offset)) {
	  return 1;
	}
      }
    }
  }

  /* Finally, print environment variable help. */
  category_newline = 0;
  if (env_vars) {
    if (format(data, &cur_col, goal, width, 0, 0, 1,
	       "\nENVIRONMENT\n\n"))
      return 1;

    for (opt = context->options; !OPT_END(opt); opt++) {
      if (format_opt_help(opt, OPT_TYPE_ENV, context->no_prefixes,
			  0, &category_newline, format, data,
			  &cur_col, goal, width, indent, offset)) {
	return 1;
      }
    }

    /* Print environment variables for suboptions as well (after
       environment variables for regular options). */
    for (opt = context->options; !OPT_END(opt); opt++) {
      if (HAS_SUBOPTS(opt)) {
	for (subopt = opt->subopts; !SUBOPT_END(subopt); subopt++) {
	  if (format_opt_help(subopt, OPT_TYPE_ENV,
			      context->no_prefixes, 0,
			      &category_newline, format, data,
			      &cur_col, goal, width,
			      indent, offset)) {
	    return 1;
	  }
	}
      }
    }
  }

  if (context->notes && format(data, &cur_col, goal, width, 0, 0, 1,
			       "\n%s\n", context->notes))
    return 1;

  return 0;
}

/*
 * Like format_help_plain(), but output help in the man(7)
 * format. Returns zero on success or nonzero on error, in which case
 * `errno' will be set to indicate the error.
 */
static int format_help_man(const MU_OPT_CONTEXT *context,
			   char **usage,
			   FORMAT_HELP_CALLBACK format, void *data) {
  unsigned short cur_col = 0;
  int env_vars, print_mandatory_args_msg;
  char *tmp, *title_name, *name;
  const MU_OPT *opt, *subopt;
  char **opt_strings = NULL, **usage_strings = NULL;
  int ret, errno_save;
  /* Used when `date' is NULL. 19 bytes for the year, 2 for the month,
     2 for the date, 2 for the hyphens, and 1 for the terminating null
     byte. The reason we use 19 bytes for the year is because, if
     sizeof(int) is 8, the maximum year is 2^(8*8 - 1) + 1900 which
     requires 19 bytes to be represented in decimal. This way, we
     won't fail even if the user has their clock set to some crazy
     date (or, unlikely, if it actually is that date). */
  char date_str[26];
  const char *const sect_table[] = {
    NULL,
    /* Default section names. */
    "User Commands",		/* 1 */
    "System Calls",		/* 2 */
    "Library Calls",		/* 3 */
    "Special Files",		/* 4 */
    "File Formats",		/* 5 */
    "Games Manual",		/* 6 */
    /* No default for section 7 because that can be lots of things. */
    NULL,			/* 7 */
    "System Administration",	/* 8 */
    /* This one's non standard, but there's no reason not to support
       it. */
    "Kernel Routines"		/* 9 */
  };
  int return_value = 1;
  /* We might need to change these. */
  const char *sect_name = context->sect_name,
    *date = context->date, *pkg_str = context->pkg_str;

  if (!sect_name) {
    int sect_index;

    sect_index = atoi(context->section);
    if (sect_index < 0 ||
	sect_index >= sizeof(sect_table) / sizeof(*sect_table)) {
      errno = ERANGE;
      goto error;
    }

    sect_name = sect_table[sect_index];
    if (!sect_name) {
      errno = EINVAL;
      goto error;
    }
  }

  if (!date) {
    struct timespec secs;
    struct tm *calendar_time;

    /* Get the current time. */
    if (clock_gettime(CLOCK_REALTIME, &secs))
      goto error;

    /* Convert it to calendar time. */
    calendar_time = localtime(&(secs.tv_sec));
    if (!calendar_time)
      goto error;

    /* And convert that to a string. */
    if (!strftime(date_str, sizeof(date_str),
		  "%Y-%m-%d", calendar_time)) {
      /* This should not be possible because `date_str' should big
	 enough to hold all possible dates. */
      mu_die(-1, "date overflowed (this should not be happening)");
    }

    date = date_str;
  }

  /*
   * Check whether there are long options with requried arguments that
   * have equivalent short options, and also whether there are
   * environment variables.
   */
  env_vars = print_mandatory_args_msg = 0;
  for (opt = context->options; !OPT_END(opt); opt++) {
    if (opt->short_opt && opt->long_opt &&
	opt->has_arg == MU_OPT_REQUIRED)
      print_mandatory_args_msg = 1;

    if (opt->env_var)
      env_vars = 1;
    if (!env_vars && HAS_SUBOPTS(opt)) {
      for (subopt = opt->subopts; !SUBOPT_END(subopt); subopt++) {
	if (subopt->env_var) {
	  env_vars = 1;
	  break;
	}
      }
    }

    if (env_vars && print_mandatory_args_msg)
      /* We have all the info we need. */
      break;
  }

  if (format(data, &cur_col, 0, 0, 0, 0, 0,
	     ".\\\" DO NOT MODIFY THIS FILE! Generated by %s.\n",
	     PACKAGE_STRING))
    goto error;

  /* Get the basename of 'context->argv[0]'. Make a copy because
     basename(3) is allowed to modify its argument. */
  tmp = strdup(context->argv[0]);
  if (!tmp)
    goto error;
  name = strdup(basename(tmp));
  errno_save = errno;
  free(tmp);
  errno = errno_save;
  if (!name)
    goto error;

  /* Get the option and usage strings. Note that `name' will now be
     part of `opt_strings' and will be freed when we free
     `opt_strings', so we don't need to explicitly free `name'. */
  opt_strings = get_opt_strings(name, context);
  if (!opt_strings)
    goto error;
  usage_strings = get_multiple_usage_strings(usage);
  if (!usage_strings)
    goto error;

  /* Get the name to use in the title (same as `name' but ALL
     CAPS). */
  title_name = strdup(name);
  if (!title_name)
    goto error;
  for (char *p = title_name; *p; p++)
    *p = toupper(*p);

  if (!pkg_str)
    pkg_str = name;

  /* Print the title. */
  ret = format(data, &cur_col, 0, 0, 0, 0, 0,
	       ".TH %s \"%s\" \"%s\" \"%s\" \"%s\"\n",
	       title_name, context->section,
	       date, pkg_str, sect_name);
  errno_save = errno;
  free(title_name);
  if (ret) {
    errno = errno_save;
    goto error;
  }

  if (context->short_desc)
    tmp = strdup(context->short_desc);
  else if (asprintf(&tmp, "manual page for %s", pkg_str) < 0)
    tmp = NULL;
  if (!tmp)
    goto error;

  /* Print the name and short description. */
  ret = format(data, &cur_col, 0, 0, 0, 0, 0,
	       ".SH NAME\n%s \\- %s\n", name, tmp);
  errno_save = errno;
  free(tmp);
  if (ret) {
    errno = errno_save;
    goto error;
  }

  /* Print the synopsis section header. */
  if (format(data, &cur_col, 0, 0, 0, 0, 0, ".SH SYNOPSIS\n"))
    goto error;

  /* Format the possible usages. */
  assert(*usage);
  for (; *usage; usage++) {
    tmp = format_man_str(*usage, NULL, NULL, NULL, USAGE_STRING);
    ret = !tmp ||
      format(data, &cur_col, 0, 0, 0, 0, 0, ".B %s%s%s\n%s",
	     name, *tmp ? "\n" : "", tmp, usage[1] ? ".br\n" : "");
    errno_save = errno;
    free(tmp);
    if (ret) {
      errno = errno_save;
      goto error;
    }
  }

  /* Print the description section header. */
  if (format(data, &cur_col, 0, 0, 0, 0, 0, ".SH DESCRIPTION\n"))
    goto error;

  if (context->desc) {
    tmp = format_man_str(context->desc, opt_strings,
			 usage_strings, NULL, 0);
    if (!tmp)
      goto error;

    /* Print the description. */
    ret = format(data, &cur_col, 0, 0, 0, 0, 0, "%s\n", tmp);
    errno_save = errno;
    free(tmp);
    if (ret) {
      errno = errno_save;
      goto error;
    }
  }

  /* Print the mandatory arguments message if we need to.  */
  if (print_mandatory_args_msg &&
      format(data, &cur_col, 0, 0, 0, 0, 0,
	     ".PP\n" MANDATORY_ARGS_MSG "\n")) {
    goto error;
  }

  /* Print the help for each option. */
  for (opt = context->options; !OPT_END(opt); opt++) {
    if (format_opt_help_man(opt, OPT_TYPE_LONG, context->no_prefixes,
			    opt_strings, usage_strings,
			    format, data, &cur_col)) {
      goto error;
    }
  }

  /* Print suboption help after regular options. */
  for (opt = context->options; !OPT_END(opt); opt++) {
    if (HAS_SUBOPTS(opt)) {
      if (print_subopts_header(opt, ".SS \"", "\"\n", "\\-",
			       format, data, &cur_col, 0, 0))
	goto error;

      for (subopt = opt->subopts; !SUBOPT_END(subopt); subopt++) {
	if (format_opt_help_man(subopt, OPT_TYPE_SUBOPT,
				context->no_prefixes,
				opt_strings, usage_strings,
				format, data, &cur_col))
	  goto error;
      }
    }
  }

  /* Finally, print environment variable help. */
  if (env_vars) {
    if (format(data, &cur_col, 0, 0, 0, 0, 0, ".SH ENVIRONMENT\n"))
      goto error;

    for (opt = context->options; !OPT_END(opt); opt++) {
      if (format_opt_help_man(opt, OPT_TYPE_ENV, context->no_prefixes,
			      opt_strings, usage_strings,
			      format, data, &cur_col))
	goto error;
    }

    /* Print environment variables for suboptions as well (after
       environment variables for regular options). */
    for (opt = context->options; !OPT_END(opt); opt++) {
      if (HAS_SUBOPTS(opt)) {
	for (subopt = opt->subopts; !SUBOPT_END(subopt); subopt++) {
	  if (format_opt_help_man(subopt, OPT_TYPE_ENV,
				  context->no_prefixes,
				  opt_strings, usage_strings,
				  format, data, &cur_col))
	    goto error;
	}
      }
    }
  }

  if (context->notes) {
    tmp = format_man_str(context->notes, opt_strings,
			 usage_strings, NULL, 0);
    if (!tmp)
      goto error;

    /* Print the notes. */
    ret = format(data, &cur_col, 0, 0, 0, 0, 0,
		 ".SH NOTES\n%s\n", context->notes);
    errno_save = errno;
    free(tmp);
    if (ret) {
      errno = errno_save;
      goto error;
    }
  }

  return_value = 0;

 error:
  free_string_array(opt_strings);
  free_string_array(usage_strings);
  return return_value;
}

/*
 * Return a NULL-terminated list of strings that specify options in
 * 'context->options'. In addition, `prog_name' will be included in
 * the list. Note that `prog_name' itself will be included, not a
 * copy, and will be freed when the returned list is freed. If an
 * error occurs, NULL will be returned, `errno' will be set to
 * indicate the error, and `prog_name' will be freed.
 */
static char **get_opt_strings(char *prog_name,
			      const MU_OPT_CONTEXT *context) {
  char **ret;
  size_t len, allocated;
  int err;

  /* Get the length and allocate `ret'. +1 for `prog_name'. */
  len = write_opt_strings(NULL, 0, &err, 0, context->options,
			  context->no_prefixes, 0) + 1;
  assert(!err);
  /* +1 for terminating NULL. */
  ret = malloc(sizeof(*ret) * (len + 1));

  /* Write the strings. */
  ret[0] = prog_name;
  allocated = write_opt_strings(ret, len, &err, 1, context->options,
				context->no_prefixes, 0);
  if (err) {
    while (allocated)
      free(ret[--allocated]);
    free(ret);
    errno = err;
    return NULL;
  }

  /* NULL-terminate the list and return it. */
  assert(allocated == len);
  ret[len] = NULL;
  return ret;
}

/*
 * Write strings that specify options in `options' to `strs', which
 * must be at least of size `len' or NULL. If `strs' is NULL, then
 * don't write anything. Returns the new index after writing the
 * strings. If an error occurs, `*err' will be set to an errno(3)
 * code, though the return value will still be the new index.
 */
static size_t write_opt_strings(char **strs, size_t len, int *err,
				size_t index, const MU_OPT *options,
				char **no_prefixes, int subopts) {
  char *prefix;
  size_t prefix_size;

  *err = 0;

  /* Get the maximum possible prefix size. */
  prefix_size = 0;
  for (char **p = no_prefixes; *p; p++) {
    const size_t len = strlen(*p);
    if (len > prefix_size)
      prefix_size = len;
  }
  prefix_size += 3; /* For "--\0" */

  prefix = malloc(prefix_size);
  if (!prefix) {
    *err = errno;
    assert(*err);
    return index;
  }

  for (const MU_OPT *opt = options; !OPT_END(opt) && !*err; opt++) {
    if (HAS_SUBOPTS(opt)) {
      assert(!subopts);
      index = write_opt_strings(strs, len, err, index,
				opt->subopts, no_prefixes, 1);
      continue;
    }

    /* Check all types except suboptions, or only suboptions if
       `subopts'. */
    for (enum opt_type type = subopts ? OPT_TYPE_SUBOPT : 0;
	 type < OPT_TYPE_SUBOPT + !!subopts && !*err; type++) {
      const char *const name = opt_name(opt, type);
      int negate = 0;
      size_t prefix_len = 0;

      if (!name)
	continue;

      while (!*err) {
	switch (type) {
	case OPT_TYPE_SHORT:
	  prefix[prefix_len++] = negate ? '+' : '-';
	  break;
	case OPT_TYPE_LONG:
	  prefix[0] = prefix[1] = '-';
	  prefix_len = 2;
	  break;
	case OPT_TYPE_ENV:
	case OPT_TYPE_SUBOPT:
	  /* These don't have prefixes. */
	  break;
	}

	if (type != OPT_TYPE_SHORT && *no_prefixes) {
	  const size_t no_prefix_len = strlen(*no_prefixes);
	  assert(no_prefix_len < prefix_size - prefix_len);
	  memcpy(prefix + prefix_len, *no_prefixes, no_prefix_len);
	  prefix_len += no_prefix_len;
	}

	prefix[prefix_len] = '\0';

	index = write_alias_strings(strs, len, err, index,
				    type, prefix, name);
	/* Add "-foo" as well as "--foo". TODO: It might be better to
	   make this optional... */
	if (!*err && type == OPT_TYPE_LONG)
	  index = write_alias_strings(strs, len, err, index,
				      type, prefix + 1, name);

	if (type == OPT_TYPE_SHORT) {
	  if (negate)
	    break;
	  negate = 1;
	}
	else {
	  if (!*no_prefixes)
	    break;
	  no_prefixes++;
	}
      }
    }
  }

  free(prefix);
  return index;
}

/*
 * Write all aliases in `aliases' to `strs', each prefixed with
 * `prefix'. `strs' must be at least of size `len' or NULL. If `strs'
 * is NULL, then don't write anything. `index' is the number of
 * strings written so far. Returns the new index in `strs', or what
 * the new index would be. If an error occurs, '*err' will be set to
 * an errno(3) code, though the return value will still be the same.
 */
static size_t write_alias_strings(char **strs, size_t len, int *err,
				  size_t index, enum opt_type type,
				  const char *prefix,
				  const char *aliases) {
  const size_t prefix_len = strlen(prefix);
  const int is_short = type == OPT_TYPE_SHORT;

  *err = 0;

  for (; is_short ? !!*aliases : !!aliases;
       is_short ? aliases++ : NEXT_ALIAS(aliases)) {
    if (strs) {
      size_t alias_len;

      assert(len > index);
      alias_len = is_short ? 1 : get_alias_len(aliases, '\0');
      strs[index] = malloc(prefix_len + alias_len + 1);
      if (!strs[index]) {
	*err = errno;
	assert(*err);
	return index;
      }

      memcpy(strs[index], prefix, prefix_len);
      memcpy(strs[index] + prefix_len, aliases, alias_len);
      strs[index][prefix_len + alias_len] = '\0';
    }

    index++;
  }

  return index;
}

/*
 * Return a NULL-terminated list of what appear to be metasyntactic
 * variables in all strings in `usages'. On error, NULL will be
 * returned and `errno' will be set to indicate the error.
 */
static char **get_multiple_usage_strings(char **usages) {
  size_t strings_len, ret_len, index;
  char **ret, ***strings;
  int errno_save;

  /* Get the length of `usages'. */
  for (strings_len = 0; usages[strings_len]; strings_len++);

  strings = malloc(sizeof(*strings) * strings_len);
  if (!strings)
    return NULL;

  /* Call get_usage_strings() for each string in `usages'. */
  ret_len = 0;
  for (size_t i = 0; i < strings_len; i++) {
    size_t len;

    strings[i] = get_usage_strings(usages[i]);
    if (!strings[i]) {
      strings_len = i;
      goto error;
    }

    for (len = 0; strings[i][len]; len++);
    ret_len += len;
  }

  /* +1 for terminating NULL. */
  ret = malloc(sizeof(*ret) * (ret_len + 1));
  if (!ret)
    goto error;

  /* Copy the strings to `ret' and return it. */
  index = 0;
  for (size_t i = 0; i < strings_len; i++) {
    for (size_t j = 0; strings[i][j]; j++) {
      assert(index < ret_len);
      ret[index++] = strings[i][j];
    }
    free(strings[i]);
  }
  free(strings);
  assert(index == ret_len);
  ret[index] = NULL;
  return ret;

 error:
  errno_save = errno;
  for (size_t i = 0; i < strings_len; i++)
    free_string_array(strings[i]);
  free(strings);
  errno = errno_save;
  return NULL;
}

/*
 * Return a NULL-terminated list of what appear to be metasyntactic
 * variables in `usage'. On error, NULL will be returned and `errno'
 * will be set to indicate the error.
 */
static char **get_usage_strings(const char *usage) {
  char **ret;
  size_t len, allocated;
  int err;

  /* Get the length and allocate `ret'. */
  len = write_usage_strings(NULL, 0, &err, usage);
  assert(!err);
  /* +1 for terminating NULL. */
  ret = malloc(sizeof(*ret) * (len + 1));

  /* Write the strings. */
  allocated = write_usage_strings(ret, len, &err, usage);
  if (err) {
    while (allocated)
      free(ret[--allocated]);
    free(ret);
    errno = err;
    return NULL;
  }

  /* NULL-terminate the list and return it. */
  assert(allocated == len);
  ret[len] = NULL;
  return ret;
}

/*
 * Write what appear to be matasyntactic variables in `usage' to
 * `strs', which must be at least of size `len' or NULL. If `strs' is
 * NULL, then don't write anything. Returns the number of strings that
 * are written or would be written. If an error occurs, '*err' will be
 * set to an errno(3) code, though the return value will still be the
 * number of strings written.
 */
static size_t write_usage_strings(char **strs, size_t len, int *err,
				  const char *usage) {
  size_t ret = *err = 0;
  static size_t call_number = 0;

  call_number++;

  for (size_t i = 0, var_len = 0; usage[i];
       i += var_len, var_len = 0) {
    /* Search for the first metasyntactic variable character. */
    for (; usage[i] && !is_var_char((unsigned char)usage[i]); i++);
    /* If the last character was a word character, it's (probably) not
       a metasyntactic variable. */
    if (i && usage[i] && is_word_char((unsigned char)usage[i - 1])) {
      while (is_var_char((unsigned char)usage[++i]));
      continue;
    }

    /* It might be a metasyntactic variable; get it's length. */
    for (var_len = 0;
	 is_var_char((unsigned char)usage[i + var_len]); var_len++);
    /* If the next character is a word character, it's (probably) not
       a metasyntactic variable. */
    if (is_word_char((unsigned char)usage[i + var_len]))
      continue;

    /* It's probably a metasyntactic variable; add it to the list. */
    if (strs) {
      assert(len > ret);
      strs[ret] = malloc(var_len + 1);
      if (!strs[ret]) {
	*err = errno;
	assert(*err);
	return ret;
      }

      memcpy(strs[ret], usage + i, var_len);
      strs[ret][var_len] = '\0';
    }

    ret++;
  }

  return ret;
}

/*
 * Convert a newline separated list of strings to an allocated
 * NULL-terminated list of strings. Returns the allocated list of
 * strings on success or NULL on error, in which case `errno' will be
 * set to indicate the error. The strings in the returned list are
 * dynamically allocated, as well as the list itself.
 */
static char **create_usage_list(const char *str) {
  struct string_list *first, *last;
  size_t size;
  char **out, **pos;
  int errno_save;

  if (!str) {
    /* Default to "[OPTION]...". */
    out = malloc(sizeof(*out) * 2);
    if (!out)
      return NULL;
    out[0] = strdup("[OPTION]...");
    if (!out[0]) {
      errno_save = errno;
      free(out);
      errno = errno_save;
      return NULL;
    }
    out[1] = NULL;

    return out;
  }

  first = last = malloc(sizeof(*first));
  if (!first)
    return NULL;

  /* Create a list of strings. */
  size = 0;
  while (1) {
    const char *newline;
    size_t str_size;

    newline = strchrnul(str, '\n');
    str_size = newline - str;

    /* +1 for the terminating null byte. */
    last->str = malloc(str_size + 1);
    if (!last->str) {
      free_string_list(first, last);
      return NULL;
    }
    memcpy(last->str, str, str_size);
    last->str[str_size] = '\0';

    last->next = malloc(sizeof(*(last->next)));
    if (!last->next) {
      errno_save = errno;
      free(last->str);
      free_string_list(first, last);
      errno = errno_save;
      return NULL;
    }
    last = last->next;
    size++;
    str = newline;
    if (*str)
      str++;
    else
      break;
  }

  /* Now copy the list to `out' (+1 for the terminating NULL). */
  out = malloc(sizeof(*out) * (size + 1));
  if (!out) {
    free_string_list(first, last);
    return NULL;
  }
  for (pos = out; size; size--, pos++) {
    struct string_list *old = first;
    *pos = first->str;
    first = first->next;
    assert(first != last || size == 1);
    free(old);
  }
  assert(first == last);
  free(last);
  *pos = NULL;

  return out;
}

/*
 * Free a NULL-terminated list of strings. Leaves `errno' unchanged.
 */
static void free_string_array(char **strings) {
  int errno_save;
  if (!strings)
    return;
  errno_save = errno;
  for (char **str = strings; *str; str++)
    free(*str);
  free(strings);
  errno = errno_save;
}

/*
 * Print the "Suboptions for [...]:" line for `opt'. Prints `prefix'
 * first, then the suboptions header, then `suffix'. `dash' is used as
 * the dash character. Returns zero on success or nonzero on error, in
 * which case `errno' will be set to indicate the error.
 */
static int print_subopts_header(const MU_OPT *opt, const char *prefix,
				const char *suffix, const char *dash,
				FORMAT_HELP_CALLBACK format,
				void *data, unsigned short *col,
				unsigned short goal,
				unsigned short width) {
  if (format(data, col, goal, width, 0, 0, 1,
	     "%sSuboptions for ", prefix))
    return 1;

  if (opt->short_opt) {
    for (const char *s = opt->short_opt; *s; s++) {
      if (format(data, col, goal, width, 0, 0, 1, "%s%s%c",
		 (s != opt->short_opt) ? ", " : "", dash, *s))
	return 1;
    }
  }

  if ((opt->long_opt &&
       print_aliases(opt->long_opt, format, data, col,
		     goal, width, 0, 0, opt->short_opt ?
		     ", " : "", ", ", "%s%s", dash, dash)) ||
      (opt->env_var &&
       print_aliases(opt->env_var, format, data, col, goal, width,
		     0, 0, (opt->short_opt || opt->long_opt) ?
		     ", " : "", ", ", "%s", ""))) {
    return 1;
  }

  return format(data, col, goal, width, 0, 0, 1, "%s", suffix);
}

/*
 * Print all aliases in `aliases' separated by `sep', each prefixed
 * with `fmt' (after being expanded printf()-style). `prefix' will be
 * printed before anything else. `format' is used to print (and
 * format) the text, and `data', `col', `goal', and `width' will be
 * passed to `format'. Returns zero on success or nonzero on error, in
 * which case `errno' will be set to indicate the error.
 */
static int print_aliases(const char *aliases,
			 FORMAT_HELP_CALLBACK format, void *data,
			 unsigned short *col, unsigned short goal,
			 unsigned short width, unsigned short indent,
			 unsigned short subindent, const char *prefix,
			 const char *sep, const char *fmt, ...) {
  char *opt_prefix;
  va_list ap;
  int ret, errno_save;

  va_start(ap, fmt);
  ret = vasprintf(&opt_prefix, fmt, ap);
  errno_save = errno; /* Just in case... */
  va_end(ap);
  if (ret < 0) {
    errno = errno_save;
    return ret;
  }

  for (const char *s = aliases; s; NEXT_ALIAS(s)) {
    char *name = get_alias_str(s, '\0');
    ret = format(data, col, goal, width,
		 indent, subindent, 1, "%s%s%s",
		 (s == aliases) ? prefix : sep, opt_prefix, name);
    errno_save = errno;
    free(name);
    if (ret) {
      free(opt_prefix);
      errno = errno_save;
      return ret;
    }
  }

  free(opt_prefix);
  return 0;
}

/*
 * Format help for a single option. Returns zero on success or nonzero
 * on error, in which case `errno' will be set to indicate the error.
 */
static int format_opt_help(const MU_OPT *opt, enum opt_type type,
			   char **no_prefixes, int has_short_opts,
			   int *category_newline,
			   FORMAT_HELP_CALLBACK format, void *data,
			   unsigned short *col,
			   unsigned short goal, unsigned short width,
			   unsigned short indent,
			   unsigned short offset) {
  return
    format_opt_help_maybe_negate(opt, type, NULL, has_short_opts,
				 category_newline, format, data, col,
				 goal, width, indent, offset) ||
    (opt_has_no_prefix(opt, type) &&
     format_opt_help_maybe_negate(opt, type, no_prefixes,
				  has_short_opts, category_newline,
				  format, data, col, goal, width,
				  indent, offset));
}

/*
 * A helper function for format_opt_help(). `no_prefixes' should be a
 * list of negation prefixes if the option should be negated, zero if
 * it shouldn't be.
 */
static int format_opt_help_maybe_negate(const MU_OPT *opt,
					enum opt_type type,
					char **no_prefixes,
					int has_short_opts,
					int *category_newline,
					FORMAT_HELP_CALLBACK format,
					void *data,
					unsigned short *col,
					unsigned short goal,
					unsigned short width,
					unsigned short indent,
					unsigned short offset) {
  int ret;

  if (opt->category) {
    const char *newline;

    if (!has_options_for_category(opt, type))
      return 0;

    /* Print the category or just print a newline if the category is
       empty. */
    newline = *category_newline ? "\n" : "";
    *category_newline = 1;
    return *(opt->category) ?
      format(data, col, goal, width, 0, 0, 1,
	     "%s%s:\n", newline, opt->category) :
      format(data, col, 0, 0, 0, 0, 1, "\n");
  }

  /* Make sure we're actually going to print something. */
  if (!opt_is_documented(opt, type, !!no_prefixes))
    return 0;

  *category_newline = 1;

  /* Use 0 for the goal and width when printing the names, because we
     don't want to wrap them. */
  ret = print_aliases_generic(opt, type, no_prefixes, 0,
			      has_short_opts, "  ", format, data,
			      col, 0, 0, 0, 0);
  if (ret)
    return ret > 0;

  if (opt->has_arg) {
    int should_free;
    char *str;

    /* Print the argument/value. We don't want to wrap this either. */
    ret =
      format(data, col, 0, 0, 0, 0, 1,
	     (opt->has_arg == MU_OPT_REQUIRED) ? "%s%s" : "[%s%s]",
	     get_arg_separator(opt, type),
	     get_arg_help(&should_free, &str, opt));
    if (should_free) {
      const int errno_save = errno;
      free(str);
      errno = errno_save;
    }
    if (ret)
      return 1;
  }

  /* Put the help on a new line if there's no room on the current
     line. */
  return
    (get_min_indent(opt, type, no_prefixes, has_short_opts) > indent &&
     format(data, col, 0, 0, 0, 0, 1, "\n")) ||
    print_help_message(opt, type, !!no_prefixes, 0, has_short_opts,
		       NULL, NULL, format, data, col,
		       goal, width, indent, indent + offset);
}

/*
 * Format help for a single option, in man(7) format. Returns zero on
 * success or nonzero on error, in which case `errno' will be set to
 * indicate the error.
 */
static int format_opt_help_man(const MU_OPT *opt, enum opt_type type,
			       char **no_prefixes, char **opt_strings,
			       char **usage_strings,
			       FORMAT_HELP_CALLBACK format,
			       void *data, unsigned short *col) {
  return
    format_opt_help_man_maybe_negate(opt, type, NULL,
				     opt_strings, usage_strings,
				     format, data, col) ||
    (opt_has_no_prefix(opt, type) &&
     format_opt_help_man_maybe_negate(opt, type, no_prefixes,
				      opt_strings, usage_strings,
				      format, data, col));
}

/*
 * A helper function for format_opt_help_man(). `no_prefixes' should
 * be nonzero if the option should be negated, zero if it shouldn't
 * be.
 */
static int format_opt_help_man_maybe_negate(const MU_OPT *opt,
					    enum opt_type type,
					    char **no_prefixes,
					    char **opt_strings,
					    char **usage_strings,
					    FORMAT_HELP_CALLBACK format,
					    void *data,
					    unsigned short *col) {
  int ret, errno_save;

  if (opt->category) {
    char *category;

    /* Ignore the category if it's empty or if there are no options
       for it. */
    if (!*(opt->category) || !has_options_for_category(opt, type))
      return 0;

    /* Print the category. */
    category =
      format_man_str(opt->category, NULL, NULL, NULL, ESCAPE_ONLY);
    if (!opt->category)
      return 1;
    ret = format(data, col, 0, 0, 0, 0, 0, ".SS \"%s\"\n", category);
    errno_save = errno;
    free(category);
    errno = errno_save;
    return ret;
  }

  /* Make sure we're actually going to print something. */
  if (!opt_is_documented(opt, type, !!no_prefixes))
    return 0;

  if (format(data, col, 0, 0, 0, 0, 0, ".TP\n"))
    return 1;

  /* Print the option itself. */
  ret = print_aliases_generic(opt, type, no_prefixes, 1, 0, "",
			      format, data, col, 0, 0, 0, 0);
  if (ret)
    return ret > 0;

  if (opt->has_arg) {
    int should_free;
    char *help, *str;

    help = format_man_str(get_arg_help(&should_free, &str, opt),
			  NULL, NULL, NULL, ARG_HELP);
    if (should_free) {
      errno_save = errno;
      free(str);
      errno = errno_save;
    }
    if (!help)
      return 1;
    /* Print the argument/value. */
    ret =
      format(data, col, 0, 0, 0, 0, 0,
	     (opt->has_arg == MU_OPT_REQUIRED) ? "%s%s" : "[%s%s]",
	     get_arg_separator(opt, type), help);
    errno_save = errno;
    free(help);
    if (ret) {
      errno = errno_save;
      return 1;
    }
  }

  return format(data, col, 0, 0, 0, 0, 0, "\n") ||
    print_help_message(opt, type, !!no_prefixes, 1, 0, opt_strings,
		       usage_strings, format, data, col, 0, 0, 0, 0);
}

/*
 * Check whether options of the type `type' exist under the category
 * specified by 'options->category'.
 */
static int has_options_for_category(const MU_OPT *options,
				    enum opt_type type) {
  assert(options->category);
  for (const MU_OPT *opt = options + 1;
       !ANYOPT_END(opt, type == OPT_TYPE_SUBOPT); opt++) {
    /* If we've reached the next category, there are no options under
       this category. */
    if (opt->category)
      return 0;
    if (opt_is_documented(opt, type, 0) ||
	(NEGATABLE(opt) && opt_is_documented(opt, type, 1)))
      return 1;
  }
  return 0;
}

/*
 * Return nonzero if `opt' should be documented, zero if it shouldn't
 * be. `type' should be the type of the option and `negate' should be
 * nonzero if `opt' should be negated, zero if it shouldn't be.
 */
static int opt_is_documented(const MU_OPT *opt,
			     enum opt_type type, int negate) {
  if (opt->category)
    return 0;

  switch (type) {
  case OPT_TYPE_SHORT:
  case OPT_TYPE_LONG:
    if (!opt->short_opt && !opt->long_opt)
      return 0;
    break;
  case OPT_TYPE_ENV:
    if (!opt->env_var)
      return 0;
    break;
  case OPT_TYPE_SUBOPT:
    assert(opt->subopt_name);
    break;
  }

  return opt->help || (negate && opt->negated_help);
}

/*
 * Print the help message for `opt'. If `negate' print the help for
 * negated options. If `man', format for man(1). Returns zero on
 * success or nonzero on error, in which case `errno' will be set to
 * indicate the error.
 */
static int print_help_message(const MU_OPT *opt, enum opt_type type,
			      int negate, int man, int has_short_opts,
			      char **opt_strings,
			      char **usage_strings,
			      FORMAT_HELP_CALLBACK format, void *data,
			      unsigned short *col,
			      unsigned short goal,
			      unsigned short width,
			      unsigned short indent,
			      unsigned short subindent) {
  union {
    const char *const_help;
    char *mutable_help;
  } help;
  int free_help = 0;
  int ret;

  if (negate)
    help.const_help = opt->negated_help;
  else {
    assert(opt->help);
    help.const_help = opt->help;
    if (man) {
      char **arg_help_strings;

      /* Format for a man page. */
      if (opt->has_arg &&
	  /* If it's an enumerated argument without argument help, it
	     will not contain metasyntactic variables. So don't bother
	     checking in that case. */
	  (opt->arg_help || opt->arg_type != MU_OPT_ENUM)) {
	int should_free;
	char *arg_help;

	arg_help_strings =
	  get_usage_strings(get_arg_help(&should_free,
					 &arg_help, opt));
	assert(arg_help_strings);
	if (should_free)
	  free(arg_help);
      }
      else
	arg_help_strings = NULL;

      help.mutable_help =
	format_man_str(help.const_help, opt_strings, usage_strings,
		       arg_help_strings, INDENT_PARAGRAPH);
      free_string_array(arg_help_strings);
      if (!help.mutable_help)
	return 1;

      free_help = 1;
    }
  }

  if (help.const_help)
    ret = format(data, col, goal, width, indent, subindent,
		 !man, "%s\n", help.const_help);
  else {
    ret = print_aliases_generic(opt, type, NULL, man, 0, "negate ",
				format, data, col,
				goal, width, indent, subindent);
    assert(ret >= 0);
    if (!ret)
      ret = format(data, col, 0, 0, 0, 0, !man, "\n");
  }
  if (free_help) {
    const int errno_save = errno;
    free(help.mutable_help);
    errno = errno_save;
  }
  return ret;
}

/*
 * Print all aliases for `opt' of type `type'. `prefix' is printed
 * before any aliases. If `man', format for man(1). Returns zero on
 * success or a positive number on error, in which case `errno' will
 * be set to indicate the error. If the option should not be printed
 * at all, a negative number is returned.
 */
static int print_aliases_generic(const MU_OPT *opt,
				 enum opt_type type,
				 char **no_prefixes, int man,
				 int has_short_opts,
				 const char *prefix,
				 FORMAT_HELP_CALLBACK format,
				 void *data, unsigned short *col,
				 unsigned short goal,
				 unsigned short width,
				 unsigned short indent,
				 unsigned short subindent) {
  const char *dash, *bold, *end, *comma;
  const char *name = NULL;
  char *start;
  int ret = 0, errno_save;

  if (man) {
    assert(!has_short_opts);
    dash = "\\-";
    bold = "\\fB";
    end = "\\fR";
    comma = "\\fR, \\fB";
  }
  else {
    dash = "-";
    bold = end = "";
    comma = ", ";
  }

  switch (type) {
  case OPT_TYPE_SHORT:
  case OPT_TYPE_LONG:
    assert(opt->short_opt || opt->long_opt);

    if (opt->short_opt) {
      const char *s = opt->short_opt;
      const char *const no_prefix = no_prefixes ? "+" : dash;
      if (format(data, col, goal, width, indent, subindent, 1,
		 "%s%s%s%c%s", prefix, bold, no_prefix, *s, end))
	return 1;
      while (*++s) {
	if (format(data, col, goal, width, indent, subindent, 1,
		   ", %s%s%c%s", bold, no_prefix, *s, end))
	  return 1;
      }
    }
    else if (has_short_opts) {
      if (format(data, col, goal, width,
		 indent, subindent, 1, "    "))
	return 1;
    }

    if (opt->long_opt) {
      if (asprintf(&start, "%s%s",
		   opt->short_opt ? ", " : prefix, bold) < 0)
	return 1;
      if (no_prefixes) {
	for (char **p = no_prefixes; *p && !ret; p++) {
	  ret = print_aliases(opt->long_opt, format, data, col,
			      goal, width, indent, subindent,
			      start, comma, "%s%s%s", dash, dash, *p);
	  if (p == no_prefixes && p[1] && !opt->short_opt) {
	    /* Change `start' to a comma for the next time around. */
	    free(start);
	    if (asprintf(&start, ", %s", bold) < 0)
	      return 1;
	  }
	}
      }
      else
	ret = print_aliases(opt->long_opt, format, data, col,
			    goal, width, indent, subindent,
			    start, comma, "%s%s", dash, dash);
      errno_save = errno;
      free(start);
      errno = errno_save;
      if (ret || format(data, col, goal, width,
			indent, subindent, 1, "%s", end))
	return 1;
    }

    break;

  case OPT_TYPE_ENV:
    assert(opt->env_var);
    name = opt->env_var;
    /* fall-through */
  case OPT_TYPE_SUBOPT:
    if (!name) {
      assert(opt->subopt_name);
      name = opt->subopt_name;
    }

    if (asprintf(&start, "%s%s", prefix, bold) < 0)
      return 1;
    if (no_prefixes) {
      for (char **p = no_prefixes; *p && !ret; p++) {
	ret = print_aliases(name, format, data, col, goal,
			    width, indent, subindent,
			    start, comma, "%s", *p);
	if (p == no_prefixes && p[1]) {
	  /* Change `start' to a comma for the next time around. */
	  free(start);
	  if (asprintf(&start, ", %s", bold) < 0)
	    return 1;
	}
      }
    }
    else
      ret = print_aliases(name, format, data, col, goal, width,
			  indent, subindent, start, comma, "%s", "");
    errno_save = errno;
    free(start);
    errno = errno_save;
    if (ret ||
	format(data, col, goal, width, indent,
	       subindent, 1, "%s", end))
      return 1;

    break;
  }

  return 0;
}

/*
 * Format a string for man(7) output using
 * format_man_str_no_alloc(). Returns a dynamically allocated,
 * formatted string on success or NULL on error, in which case `errno'
 * will be set to indicate the error.
 */
static char *format_man_str(const char *str, char **opts,
			    char **vars1, char **vars2, int flags) {
  const size_t len = strlen(str);
  size_t size;
  char *out;

  size = format_man_str_no_alloc(NULL, 0, opts, vars1, vars2,
				 str, len, flags);
  if (!size)
    return NULL;
  out = malloc(size);
  if (!out)
    return NULL;
  if (!format_man_str_no_alloc(out, size, opts, vars1, vars2,
			       str, len, flags)) {
    const int errno_save = errno;
    free(out);
    errno = errno_save;
    return NULL;
  }
  return out;
}

/*
 * Format a string for man(7) output. If the string is the `arg_help'
 * field, then you should pass ARG_HELP in `flags'. If you only want
 * to escape characters, not format options, metasyntactic variables,
 * etc., then you should pass ESCAPE_ONLY in `flags'.
 *
 * Returns the result in `out', not writing more that `out_max',
 * including the terminating null byte (unless `out' is NULL, in which
 * case no limit will be enforced). If `out' is NULL, don't write
 * anything. Returns the size of `out' (or what the size would be),
 * including the terminating null byte on success or zero on error, in
 * which case `errno' will be set to indicate the error.
 *
 * Note: Currently, this is pretty hacky, and there's no real way to
 * fix that. Actually, maybe we should implement a very simple markup
 * language (such as "@var{foo}" or something like Texinfo).
 */
static size_t format_man_str_no_alloc(char *out, size_t out_max,
				      char **opts, char **vars1,
				      char **vars2, const char *str,
				      size_t len, int flags) {
  size_t index, end;
  int first;

  index = end = 0;

  /*
   * Before we do anything, we need to make sure that `str' doesn't
   * start with ''' or '.', because those characters start macros and
   * requests. If it does, escape it with '\:'. '\:' tells troff that
   * it can break the line there, but it shouldn't use a hyphen. Since
   * it is at the beginning of a line, it shouldn't have any effect
   * other than to escape the ''' or '.'.
   */
  if (*str == '\'' || *str == '.')
    write_out(out, &index, out_max, "\\:");

  for (first = 1; *str && (!out || index < out_max); first = 0) {
    if (*str == '\n') {
      int is_paragraph = 0;
      const char *p;

      assert(!(flags & ARG_HELP));
      /* For now, ESCAPE_ONLY is used only for strings which have no
	 newlines. */
      assert(!(flags & ESCAPE_ONLY));

      /* Check if it's a new paragraph. */
      for (p = str + 1; *p && isspace(*p); p++) {
	/* Don't break when we see a newline because we want to skip
	   all the other newlines as well. */
	if (*p == '\n')
	  is_paragraph = 1;
      }

      if (is_paragraph) {
	/* Go back to the last newline. */
	do {
	  p--;
	  assert(p > str);
	} while (*p != '\n');
	p++;

	/* Start a new paragraph. */
	write_out(out, &index, out_max, "\n.%cP\n",
		  flags & INDENT_PARAGRAPH ? 'I' : 'P');
	len -= p - str;
	str = p;
      }
      else {
	/* It's not a paragraph, but put an explicit line break
	   here. */
	write_out(out, &index, out_max, "\n.br\n");
	str++;
	len--;
      }
    }

    if (!(flags & ESCAPE_ONLY) && !end &&
	(first || flags & ARG_HELP || !is_word_char(str[-1]))) {
      int is_var;

      /* Check for options we should put in bold. */
      end = find_opt_or_var(str, len, opts, first, flags);
      if (!(flags & ARG_HELP) && end) {
	/* It might actually be an option; put it in bold. (Note:
	   this is still just a guess.) */
	if (write_out(out, &index, out_max, "\\fB"))
	  return 0;
	continue;
      }

      /* Check for metasyntactic variables we should put in italics. */
      if (flags & (ARG_HELP | USAGE_STRING)) {
	for (end = 0; end < len && is_var_char(str[end]); end++);
	/* Don't treat "A" and "I" as metasyntactic variables unless
	   ARG_HELP is in `flags', because they are English words. This
	   is pretty hacky, and they might actually be metasyntactic
	   variables after all... */
	is_var = end && !(end == 1 && strchr("AI", *str));
      }
      else {
	end = find_opt_or_var(str, len, vars1, first, flags);
	if (!end)
	  end = find_opt_or_var(str, len, vars2, first, flags);
	is_var = !!end;
      }

      if (is_var) {
	/* It might actually be a metasyntactic variable; italicize
	   it. (Note: this is still just a guess.) */
	if (write_out(out, &index, out_max, "\\fI"))
	  return 0;
	continue;
      }

      end = 0;
    }

    /* Print the character verbatim or escape it. */
    if (write_out(out, &index, out_max, "%s%c",
		  strchr("\\-", *str) ? "\\" : "", *str))
      return 0;
    str++;
    len--;
    if (end && !--end && write_out(out, &index, out_max, "\\fR"))
      return 0;
  }

  if (out)
    assert(index <= out_max);

  /* Terminate the output string. */
  if (index && index == out_max)
    index--;
  if (out && out_max)
    out[index] = '\0';
  index++;

  /* Return the size (or would be size) of the output string. */
  return index;
}

/*
 * Write to `out', at index '*index', not exceeding `out_max'. Updates
 * '*index' to the new index. Returns 0 on success or nonzero on
 * error, in which case `errno' will be set to indicate the error.
 */
static int write_out(char *out, size_t *index, size_t out_max,
		     const char *fmt, ...) {
  int len;
  char *str;
  va_list ap;

  va_start(ap, fmt);
  len = vasprintf(&str, fmt, ap);
  if (len < 0)
    return 1;
  va_end(ap);

  if (out) {
    size_t left = out_max - *index;
    if (len > left)
      len = left - 1;
    memcpy(out + *index, str, len);
  }
  *index += len;

  free(str);

  return 0;
}

/*
 * Find an instance of any of `substrs' in `str'. `len' is the length
 * of `str', for efficiency. If no substrings are found, return zero;
 * otherwise return the length of the substring. If any of `substrs'
 * are substrings of other strings in `substrs', match the longest
 * one.
 */
static size_t find_opt_or_var(const char *str, size_t len,
			      char **substrs, int first, int flags) {
  size_t max_len = 0;

  if (!substrs)
    return 0;
  for (char **substr = substrs; *substr; substr++) {
    const size_t sub_len = strlen(*substr);
    if (sub_len > max_len &&
	(len == sub_len ||
	 (len > sub_len &&
	  (flags & ARG_HELP || !is_word_char(str[sub_len])))) &&
	!memcmp(str, *substr, sub_len))
      max_len = sub_len;
  }
  return max_len;
}

/*
 * Check if `c' is a valid charcter for a metasyntactic variable.
 */
static int is_var_char(int c) {
  return c && (isupper(c) || isdigit(c) || strchr("-_", c));
}

/*
 * Check if `c' is a character that could be part of a word.
 */
static int is_word_char(int c) {
  return isalnum(c);
}

/*
 * A wrapper around vformat() to be passed as a callback to
 * format_help_type().
 */
static int format_help_file_callback(void *data, unsigned short *col,
				     unsigned short goal,
				     unsigned short width,
				     unsigned short indent,
				     unsigned short subindent,
				     int format_tabs,
				     const char *fmt, ...) {
  FILE *file = data;
  int ret;
  va_list ap;

  va_start(ap, fmt);
  ret = vformat(file, col, goal, width, indent,
		subindent, format_tabs, fmt, ap);
  va_end(ap);

  return ret;
}

/*
 * Format a string and add the result to the string list pointed to by
 * `data'. Returns zero on success or nonzero on error, in which case
 * `errno' will be set to indicate the error.
 */
static int format_help_str_callback(void *data, unsigned short *col,
				    unsigned short goal,
				    unsigned short width,
				    unsigned short indent,
				    unsigned short subindent,
				    int format_tabs,
				    const char *fmt, ...) {
  struct string_list **list_ptr = data, *list = *list_ptr;
  va_list ap;
  int errno_save;

  va_start(ap, fmt);
  list->str = vformat_string(col, goal, width, indent,
			     subindent, format_tabs, fmt, ap);
  errno_save = errno; /* Just in case... */
  va_end(ap);

  if (!list->str) {
    errno = errno_save;
    return 1;
  }

  list->next = malloc(sizeof(*(list->next)));
  list = list->next;
  if (!list) {
    errno_save = errno;
    free(list->str);
    errno = errno_save;
    return 1;
  }

  *list_ptr = list;
  return 0;
}

/*
 * Concatenate all strings in the string list starting at first and
 * ending at last, while also freeing the entire list, and return the
 * result. Returns NULL on error, in which case `errno' will be set to
 * indicate the error. Even on error, the entire list will be freed.
 */
static char *concat_string_list(struct string_list *first,
				struct string_list *last) {
  size_t size;
  char *str;

  /* Get the size of the strings when concatenated. */
  size = 0;
  for (struct string_list *s = first; s != last; s = s->next)
    size += strlen(s->str);
  size++;			/* For '\0'. */

  str = malloc(sizeof(*str) * size);
  if (!str) {
    free_string_list(first, last);
    return NULL;
  }

  /* Concatenate all of the strings. */
  str[0] = '\0';
  while (first != last) {
    struct string_list *old = first;

    /* This assertion is fairly expensive, but assertions can be
       turned off by defining NDEBUG, so I think we should keep it. */
    assert(strlen(str) + strlen(first->str) < size);
    strcat(str, first->str);

    first = first->next;
    free(old->str);
    free(old);
  }
  free(last);

  /* Return the result. */
  return str;
}

/*
 * Free a list of strings. Leaves `errno' unchanged.
 */
static void free_string_list(struct string_list *first,
			     struct string_list *last) {
  const int errno_save = errno;
  while (first != last) {
    struct string_list *old = first;
    first = first->next;
    free(old->str);
    free(old);
  }
  free(last);
  errno = errno_save;
}

/*
 * Format and print a help message to `stdout' (or piped to man). Does
 * not return on success.
 */
static int help_option_callback(int has_arg, int arg,
				void *data, char *err) {
  MU_OPT_CONTEXT *context = data;

  if (!has_arg)
    arg = context->default_format;

  if (arg == HELP_TYPE_MAN)
    return man_option_callback(data, err);

  assert(arg == HELP_TYPE_PLAIN);
  if (mu_format_help(stdout, context) || fflush(stdout) == EOF) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot print help message: %s", strerror(errno));
    return 1;
  }

  /* We don't really need to do this since we're about to exit(), but
     it makes valgrind happy. */
  mu_opt_context_free(context);

  exit(0);
}

/*
 * Format and print a help message in man(7) format. Does not return
 * on success.
 */
static int man_option_callback(void *data, char *err) {
  MU_OPT_CONTEXT *context = data;
  FILE *output = stdout;
#ifdef USE_MAN_PIPE
  int do_man_pipe = 0;
  pid_t man_pid;
  int wstatus;
  int man_pipe[2], err_pipe[2];
#endif

#ifdef USE_MAN_PIPE
  /* Check whether we should pipe the help output to `man'. */
  do_man_pipe = isatty(STDOUT_FILENO);
  if (do_man_pipe && (pipe(man_pipe) || pipe(err_pipe))) {
    /* An error occured while setting up the pipe. Print a warning and
       don't pipe to man after all. */
    fprintf(stderr, "%s: warning: cannot set up pipe: %s\n",
	    context->argv[0], strerror(errno));
    do_man_pipe = 0;
  }

  if (do_man_pipe) {
    man_pid = fork();
    if (man_pid < 0) {
      /* An error occured. Print a warning and don't pipe to man after
	 all. */
      fprintf(stderr, "%s: warning: cannot fork: %s\n",
	      context->argv[0], strerror(errno));
      do_man_pipe = 0;
    }
  }

  if (do_man_pipe) {
    char *chld_err;

    if (!man_pid) {
      /* We are the child. We don't need the write end of `man_pipe'
	 or the read end of `err_pipe'. */
      if (close(man_pipe[1]) || close(err_pipe[0]))
	report_err(err_pipe[1], "cannot close pipes: %s",
		   strerror(errno));

      /* Redirect standard input from `man_pipe'. */
      if (dup2(man_pipe[0], STDIN_FILENO) < 0)
	report_err(err_pipe[1],
		   "cannot redirect standard input: %s",
		   strerror(errno));

      /* Make sure to close the output `err_pipe' after a successful
	 execve() so that get_err() doesn't hang in the parent. */
      if (fcntl(err_pipe[1], F_SETFD, FD_CLOEXEC) < 0)
	report_err(err_pipe[1],
		   "cannot set FD_CLOEXEC flag on error pipe (%d): %s",
		   err_pipe[1], strerror(errno));

      /* Execute `man' in such a way that it reads from standard
	 input. */
      execlp("man", "man", "-l", "-", NULL);
      report_err(err_pipe[1], "cannot execute 'man': %s",
		 strerror(errno));
    }

    /* We are the parent. We don't need the read end of `man_pipe'
       or the write end of `err_pipe'. */
    if (close(man_pipe[0]) || close(err_pipe[1])) {
      snprintf(err, MU_OPT_ERR_MAX, "cannot close pipes: %s",
	       strerror(errno));
      goto kill;
    }

    /* Check if an error occured. */
    chld_err = get_err(err_pipe[0]);

    /* We can close the error pipe now. */
    if (close(err_pipe[0])) {
      snprintf(err, MU_OPT_ERR_MAX,
	       "cannot close error pipe: %s", strerror(errno));
      return 1;
    }

    if (chld_err) {
      /* An error occured. Print a warning message. */
      fprintf(stderr, "%s: warning: %s\n",
	      context->argv[0], chld_err);
      free(chld_err);

      /* Wait for the child... */
      do {
	pid_t pid = waitpid(man_pid, &wstatus, 0);
	if (pid < 0) {
	  snprintf(err, MU_OPT_ERR_MAX,
		   "cannot wait for child: %s", strerror(errno));
	  return 1;
	}
	assert(pid == man_pid);
      } while (!(WIFEXITED(wstatus) || WIFSIGNALED(wstatus)));

      /* And don't pipe through man after all. */
      do_man_pipe = 0;
    }
  }

  if (do_man_pipe) {
    /* Associate 'man_pipe[1]' with a stream to pass to
       mu_format_help(). */
    output = fdopen(man_pipe[1], "w");
    if (!output) {
      snprintf(err, MU_OPT_ERR_MAX, "cannot associate "
	       "file descriptor %d with a stream: %s",
	       man_pipe[1], strerror(errno));
      goto kill;
    }
  }
#endif /* USE_MAN_PIPE */

  /* Format the help text. */
  if (mu_format_help_man(output, context) || fflush(output) == EOF) {
    snprintf(err, MU_OPT_ERR_MAX,
	     "cannot print man page: %s", strerror(errno));
#ifdef USE_MAN_PIPE
    if (do_man_pipe) {
      fclose(output);
      close(err_pipe[0]);
      goto kill;
    }
#endif
    return 1;
  }

#ifdef USE_MAN_PIPE
  if (do_man_pipe) {
    /* Close the man pipe (closing `output' automatically closes
       'man_pipe[1]') */
    if (fclose(output)) {
      snprintf(err, MU_OPT_ERR_MAX,
	       "cannot close man pipe: %s", strerror(errno));
      close(err_pipe[0]);
      goto kill;
    }

    /* Wait for the child process. */
    do {
      pid_t pid = waitpid(man_pid, &wstatus, 0);
      if (pid < 0) {
	snprintf(err, MU_OPT_ERR_MAX,
		 "cannot wait for man: %s", strerror(errno));
	return 1;
      }
      assert(pid == man_pid);
    } while (!(WIFEXITED(wstatus) || WIFSIGNALED(wstatus)));

    if (WIFEXITED(wstatus)) {
      int status = WEXITSTATUS(wstatus);
      if (status) {
	snprintf(err, MU_OPT_ERR_MAX,
		 "man exited with status %d", status);
	return 1;
      }
    }
    else if (WIFSIGNALED(wstatus)) {
      int signal, coredump;
      char signame[SIG2STR_MAX];

      signal = WTERMSIG(wstatus);
      /* WCOREDUMP() is not specified in POSIX.1-2001 (see
	 wait(2)). */
# ifdef WCOREDUMP
      coredump = WCOREDUMP(wstatus);
# else
      coredump = 0;
# endif

      /* Get the signal name. */
      if (sig2str(signal, signame))
	mu_die(-1, "invalid signal: %d", signal);

      snprintf(err, MU_OPT_ERR_MAX,
	       /* This looks like a headline (heh heh). */
	       "man killed by SIG%s (%d)%s",
	       signame, signal, coredump ? " (core dumped)" : "");

      return 1;
    }
    else
      mu_die(-1, "child wasn't signaled or exited");
  }
#endif /* USE_MAN_PIPE */

  /* We don't really need to do this since we're about to exit(), but
     it makes valgrind happy. */
  mu_opt_context_free(context);

  exit(0);

#ifdef USE_MAN_PIPE
 kill: {
    /* A single millisecond. */
    const struct timespec ms = {0, 1000000};
    int dead = 0;
    pid_t pid;

    /* First try to nicely ask the child to terminate. If the kill()
       fails, then it's because the child is already dead (or rather,
       undead), so we should still be able to wait for it. */
    kill(man_pid, SIGTERM);

    /* Now try and wait for the child. Give it 10 ms to die, checking
       every millisecond. */
    for (int i = 0; i < 10; i++) {
      pid = waitpid(man_pid, NULL, WNOHANG);
      if (pid) {
	/* No errors should occur. */
	assert(pid == man_pid);
	dead = 1;
	break;
      }

      /* No need for error checking; it's not worth it. */
      nanosleep(&ms, NULL);
    }

    if (!dead) {
      /* The child is still alive. Remedy that. */
      kill(man_pid, SIGKILL);
      pid = waitpid(man_pid, NULL, WNOHANG);
      /* We should definitely be able to wait for the child now. */
      assert(pid == man_pid);
    }

    return 1;
  }
#endif /* USE_MAN_PIPE */
}

#ifdef USE_MAN_PIPE
/*
 * Report an error to the parent process. Does not return.
 */
static void report_err(int fd, const char *fmt, ...) {
  char *err;
  unsigned err_len;
  va_list ap;
  int success;

  va_start(ap, fmt);
  err_len = mu_xvasprintf(&err, fmt, ap);
  va_end(ap);

  /* Send the length of the error message first, and the message
     itself second. */
  success =
    write(fd, &err_len,	sizeof(err_len)) == sizeof(err_len) &&
    write(fd, err, sizeof(*err) * err_len) == sizeof(*err) * err_len;
  if (!success) {
    /* We can't write the error message, so just print it instead. */
    fputs(err, stderr);
    putc('\n', stderr);
  }
  free(err);

  exit(-1);
}

/*
 * Get an error from the child process. Returns a dynamically
 * allocated error string if there was en error, or NULL if there
 * wasn't.
 */
static char *get_err(int fd) {
  ssize_t bytes_read;
  char *err;
  unsigned err_len;

  /* First get the length of the error string. */
  bytes_read = read(fd, &err_len, sizeof(err_len));
  if (!bytes_read)
    return NULL;
  if (bytes_read < 0) {
    mu_xasprintf(&err, "cannot get error string: %s",
		 strerror(errno));
    return err;
  }
  if (bytes_read != sizeof(err_len))
    return mu_xstrdup("cannot get error string");

  /* Now get the actual error string (+1 for '\0'). */
  err = mu_xmalloc(sizeof(*err) * (err_len + 1));
  bytes_read = read(fd, err, sizeof(*err) * err_len);
  if (bytes_read < 0) {
    const int errno_save = errno;
    free(err);
    mu_xasprintf(&err, "cannot get error string: %s",
		 strerror(errno_save));
    return err;
  }
  /* If we couldn't read the whole string, I guess just return what we
     got. */
  assert(bytes_read <= err_len);
  err_len = bytes_read;

  /* Terminate the error string. */
  err[err_len] = '\0';

  return err;
}
#endif /* USE_MAN_PIPE */

/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * safe.c -- versions of standard functions that do not return on
 * error
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>

#include "compat.h"
#include "format.h"
#include "options.h"
#include "options-internal.h"
#include "safe.h"

#define SAFE_DIE(fmt, ...)					     \
  SAFE_CALL(mu_die, -1, fmt ": %s", __VA_ARGS__, strerror(errno));

/* Like SAFE_DIE, but only takes one argument. This is required
   because it is not really possible to use __VA_ARGS__ portably when
   there are no arguments. */
#define SAFE_DIE_NOARGS(msg)					\
  SAFE_CALL(mu_die, -1, msg ": %s" , strerror(errno));

/* Call another safe function from within one. */
#define SAFE_CALL(name, ...)				\
  _MU_SAFE_TR_NAME(name)(file, line, func, __VA_ARGS__)

/*
 * Static helper functions
 */
static int _MU_SAFE_DECL(print_err, const char *, va_list)
  __attribute__((_mu_safe_format_attr(printf, 1, 0)));
static int needs_context(const char *);

/*
 * Call mu_vdie(); see below.
 */
void _MU_SAFE_DECL(mu_die, int status, const char *fmt, ...) {
  va_list ap;

  /* Check `status' here so that we can report that it was mu_die()
     that was called erroneously, not mu_vdie(). */
  if (!status)
    SAFE_CALL(mu_die, -1, "mu_die() called with 0 status");

  va_start(ap, fmt);
  SAFE_CALL(mu_vdie, status, fmt, ap);
  va_end(ap);
}

/*
 * Call mu_vwarn(); see below.
 */
int _MU_SAFE_DECL(mu_warn, const char *fmt, ...) {
  va_list ap;
  int ret;

  va_start(ap, fmt);
  ret = SAFE_CALL(mu_vwarn, fmt, ap);
  va_end(ap);

  return ret;
}

/*
 * Print the printf-style format string `fmt' to standard error and
 * die with status `status'. If `status' is negative, abort()
 * instead. `status' must not be 0. If you want to print a warning
 * without exiting, see mu_warn() and mu_vwarn().
 */
void _MU_SAFE_DECL(mu_vdie, int status, const char *fmt, va_list ap) {
  /* If we failed to print to standard error, there's no point in
     trying again, so simply abort(). We should also abort if `status'
     is negative. */
  if (SAFE_CALL(print_err, fmt, ap) || status < 0)
    abort();
  exit(status);
}

/*
 * Like mu_vdie() (see above) except that it returns instead of
 * exiting/aborting. Returns zero if we could successfully print the
 * message, or nonzero if we couldn't.
 */
int _MU_SAFE_DECL(mu_vwarn, const char *fmt, va_list ap) {
  return SAFE_CALL(print_err, fmt, ap);
}

/*
 * The functions that follow are safe versions of the non-mu_x
 * functions.
 */

void *_MU_SAFE_DECL(mu_xmalloc, size_t size) {
  void *const ptr = malloc(size);

  if (ptr)
    return ptr;

  /* If we're here, we failed to allocate memory. */
  SAFE_DIE("failed to allocate %zu bytes of memory", size);
}

void *_MU_SAFE_DECL(mu_xcalloc, size_t nmemb, size_t size) {
  void *const ptr = calloc(nmemb, size);

  if (ptr)
    return ptr;

  /* If we're here, we failed to allocate memory. */
  SAFE_DIE("failed to allocate %zu elements of size %zu",
	   nmemb, size);
}

void *_MU_SAFE_DECL(mu_xrealloc, void *ptr, size_t size) {
  void *const res = realloc(ptr, size);

  if (res)
    return res;

  /* If we're here, we failed to allocate memory. */
  SAFE_DIE("failed to re-allocate %zu bytes of memory", size);
}

void *_MU_SAFE_DECL(mu_xreallocarray, void *ptr, size_t nmemb, size_t size) {
  void *const res = reallocarray(ptr, nmemb, size);

  if (res)
    return res;

  /* If we're here, we failed to allocate memory. */
  SAFE_DIE("failed to re-allocate %zu elements, each of size %zu",
	   nmemb, size);
}

char *_MU_SAFE_DECL(mu_xstrdup, const char *str) {
  char *const newstr = strdup(str);

  if (newstr)
    return newstr;

  /* If we're here, we failed to allocate memory. */
  SAFE_DIE("failed to allocate memory for a string "
	   "of length %zu, \"%s\"", strlen(str), str);
}

char *_MU_SAFE_DECL(mu_xstrndup, const char *str, size_t max) {
  char *const newstr = strndup(str, max);
  size_t len;

  if (newstr)
    return newstr;

  /* If we're here, we failed to allocate memory. */
  len = strlen(str);
  SAFE_DIE("failed to allocate %zu bytes of memory for a string of "
	   "length %zu, \"%s\"", (len > max) ? max : len, len, str);
}

/*
 * These printf()-style functions return 'unsigned' simply to indicate
 * that the return value will never be negative. They will not,
 * however, return a value greater than INT_MAX.
 */

unsigned _MU_SAFE_DECL(mu_xasprintf, char **strp, const char *fmt, ...) {
  va_list ap;
  int ret;

  va_start(ap, fmt);
  ret = SAFE_CALL(mu_xvasprintf, strp, fmt, ap);
  va_end(ap);

  return ret;
}

unsigned _MU_SAFE_DECL(mu_xvasprintf,
		       char **strp, const char *fmt, va_list ap) {
  const int ret = vasprintf(strp, fmt, ap);
  if (ret < 0)
    SAFE_DIE("failed to allocate for a string with format \"%s\"", fmt);
  return ret;
}

/*
 * These are safe versions of functions provided by Mu itself.
 */

void _MU_SAFE_DECL(mu_xformat,
		   FILE *stream, unsigned short *col,
		   unsigned short goal, unsigned short width,
		   unsigned short indent, unsigned short subindent,
		   const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  SAFE_CALL(mu_xvformat,
	    stream, col, goal, width, indent, subindent, fmt, ap);
  va_end(ap);
}

char *_MU_SAFE_DECL(mu_xformat_string,
		    unsigned short *col, unsigned short goal,
		    unsigned short width, unsigned short indent,
		    unsigned short subindent, const char *fmt, ...) {
  va_list ap;
  char *ret;

  va_start(ap, fmt);
  ret = SAFE_CALL(mu_xvformat_string, col, goal, width,
		  indent, subindent, fmt, ap);
  va_end(ap);

  return ret;
}

void _MU_SAFE_DECL(mu_xvformat,
		   FILE *stream, unsigned short *col,
		   unsigned short goal, unsigned short width,
		   unsigned short indent, unsigned short subindent,
		   const char *fmt, va_list ap) {
  if (mu_vformat(stream, col, goal, width,
		 indent, subindent, fmt, ap))
    SAFE_DIE("failed to format a string with "
	     "format \"%s\" to a file", fmt);
}

char *_MU_SAFE_DECL(mu_xvformat_string,
		    unsigned short *col,
		    unsigned short goal, unsigned short width,
		    unsigned short indent, unsigned short subindent,
		    const char *fmt, va_list ap) {
  char *const ret =
    mu_vformat_string(col, goal, width, indent, subindent, fmt, ap);
  if (!ret)
    SAFE_DIE("failed to format a string with "
	     "format \"%s\" to a string", fmt);
  return ret;
}

MU_OPT_CONTEXT *_MU_SAFE_DECL(mu_opt_context_xnew,
			      int argc, char **argv,
			      const MU_OPT *options, int flags) {
  MU_OPT_CONTEXT *const context =
    mu_opt_context_new(argc, argv, options, flags);
  if (!context)
    SAFE_DIE_NOARGS("cannot create a new option context");
  return context;
}

MU_OPT_CONTEXT *_MU_SAFE_DECL(mu_opt_context_xnew_with_env,
			      int argc, char **argv, char **env,
			      const MU_OPT *options, int flags) {
  MU_OPT_CONTEXT *const context =
    mu_opt_context_new_with_env(argc, argv, env, options, flags);
  if (!context)
    SAFE_DIE_NOARGS("cannot create a new option context");
  return context;
}

void _MU_SAFE_DECL(mu_opt_context_xfree, MU_OPT_CONTEXT *context) {
  if (mu_opt_context_free(context))
    SAFE_DIE_NOARGS("cannot free an option context");
}

void _MU_SAFE_DECL(mu_opt_context_xset_no_prefixes,
		   MU_OPT_CONTEXT *context, ...) {
  va_list ap;
  int ret;
  va_start(ap, context);
  ret = opt_context_vset_no_prefixes(context, ap);
  va_end(ap);
  if (ret)
    SAFE_DIE_NOARGS("cannot add negation prefixes "
		    "to an option context");
}

void _MU_SAFE_DECL(mu_opt_context_xset_no_prefix_array,
		   MU_OPT_CONTEXT *context, char **strings) {
  if (mu_opt_context_set_no_prefix_array(context, strings))
    SAFE_DIE("cannot add an array of negation prefixes "
	     "(%p) to an option context", strings);
}

void _MU_SAFE_DECL(mu_opt_context_xadd_options,
		   MU_OPT_CONTEXT *context, const MU_OPT *options,
		   enum MU_OPT_WHERE where) {
  if (mu_opt_context_add_options(context, options, where))
    SAFE_DIE_NOARGS("cannot add new options to an option context");
}

void _MU_SAFE_DECL(mu_opt_context_xadd_help_options,
		   MU_OPT_CONTEXT *context, int flags) {
  if (mu_opt_context_add_help_options(context, flags))
    SAFE_DIE("cannot add help options (%d) to an option context",
	     flags);
}

MU_SUBOPT_CONTEXT *_MU_SAFE_DECL(mu_subopt_context_xnew,
				 const char *prog_name,
				 const char *suboptstr,
				 const MU_OPT *subopts) {
  MU_SUBOPT_CONTEXT *const context =
    mu_subopt_context_new(prog_name, suboptstr, subopts);
  if (!context)
    SAFE_DIE_NOARGS("cannot create a new suboption context");
  return context;
}

void _MU_SAFE_DECL(mu_subopt_context_xfree,
		   MU_SUBOPT_CONTEXT *context) {
  if (mu_subopt_context_free(context))
    SAFE_DIE_NOARGS("cannot free a suboption context");
}

void _MU_SAFE_DECL(mu_subopt_context_xset_no_prefixes,
		   MU_SUBOPT_CONTEXT *context, ...) {
  va_list ap;
  int ret;
  va_start(ap, context);
  ret = subopt_context_vset_no_prefixes(context, ap);
  va_end(ap);
  if (ret)
    SAFE_DIE_NOARGS("cannot add negation prefixes "
		    "to a suboption context");
}

void _MU_SAFE_DECL(mu_subopt_context_xset_no_prefix_array,
		   MU_SUBOPT_CONTEXT *context, char **strings) {
  if (mu_subopt_context_set_no_prefix_array(context, strings))
    SAFE_DIE("cannot add an array of negation prefixes "
	     "(%p) to a suboption context", strings);
}

void _MU_SAFE_DECL(mu_xformat_help,
		   FILE *stream, const MU_OPT_CONTEXT *context) {
  if (mu_format_help(stream, context))
    SAFE_DIE_NOARGS("cannot print help message");
}

char *_MU_SAFE_DECL(mu_xformat_help_string,
		    const MU_OPT_CONTEXT *context,
		    unsigned short goal, unsigned short width) {
  char *const str = mu_format_help_string(context, goal, width);
  if (!str)
    SAFE_DIE_NOARGS("cannot format help message");
  return str;
}

void _MU_SAFE_DECL(mu_xformat_help_man,
		   FILE *stream, const MU_OPT_CONTEXT *context) {
  if (mu_format_help_man(stream, context))
    SAFE_DIE_NOARGS("cannot print roff-formatted help message");
}

char *_MU_SAFE_DECL(mu_xformat_help_man_string,
		    const MU_OPT_CONTEXT *context) {
  char *const str = mu_format_help_man_string(context);
  if (!str)
    SAFE_DIE_NOARGS("cannot format roff-formatted help message");
  return str;
}

/***************************\
|* Static helper functions *|
\***************************/

/*
 * Print a message for mu_{v,}{die,warn}(). Like perl(1), if `fmt'
 * (after expansion) does not end in a newline, print information
 * about where mu_vdie() was called, and add a newline. Otherwise, the
 * message is printed verbatim.
 *
 * Returns zero if we could successfully print the message, or nonzero
 * if we couldn't.
 */
static int _MU_SAFE_DECL(print_err, const char *fmt, va_list ap) {
  char *msg = NULL;
  int len;
  int ret = 0, errno_save, context;
  /* The message printed if the provided one was empty. */
  char default_msg[] = "an error occured";

  /* Check whether we should print context. */
  context = needs_context(fmt);
  if (context < 0) {
    /* We need to expand `fmt' into a temporary string so we can check
       if it ends in a newline. */
    len = SAFE_CALL(mu_xvasprintf, &msg, fmt, ap);
    if (len)
      context = msg[len - 1] != '\n';
    else {
      free(msg);
      /* Generic error message in case none was supplied. */
      msg = default_msg;
      context = 1;
    }
  }

  /* Print the message. */
  if (context)
    ret = fprintf(stderr, "%s:%d in function %s: ",
		  file, line, func) < 0;
  if (!ret) {
    if (msg)
      ret = fputs(msg, stderr) == EOF;
    else {
      len = vfprintf(stderr, fmt, ap);
      if (len < 0)
	ret = 1;
      else if (!len)
	ret = fputs(default_msg, stderr) == EOF;
    }
  }
  if (!ret && context)
    ret = putc('\n', stderr) == EOF;

  if (msg && msg != default_msg) {
    errno_save = errno;
    free(msg);
    errno = errno_save;
  }

  return ret;
}

/*
 * Check if the format string, `fmt', would end in a newline if
 * expanded. If it would, return 0. If it wouldn't, return a positive
 * number. If it cannot be determined without actually expanding the
 * string, return a negative number.
 */
static int needs_context(const char *fmt) {
  const size_t len = strlen(fmt);
  char last;

  /* If the string is empty, then it cannot end in a newline. */
  if (!len)
    return 1;

  /* If the format string ends in a newline, then the resulting string
     will as well. */
  last = fmt[len - 1];
  if (last == '\n')
    return 0;

  /*
   * Format specifiers that could cause the string to end in a newline
   * are 'c', 's', 'C', and 'S' ('C' and 'S' are obsolete synonyms for
   * 'lc' and 'ls'). Although 'm' (a GNU extension) expands to a
   * string ('strerror(errno)'), it will never end in a newline, so we
   * can ignore it.
   *
   * If the string does not end in one of these format specifiers, and
   * it doesn't end in a newline (which we checked earlier), then the
   * expanded string cannot end in a newline.
   */
  if (!strchr("csCS", (unsigned int)last))
    return 1;

  /*
   * OK, so it does end in 'c', 's', 'C', or 'S'. Now, we need to make
   * sure that's actually a format specifier. The format is fairly
   * complicated, so let's just check if a '%' appears before the last
   * character. This will, admittedly, have some false positives,
   * though it shouldn't have any false negatives.
   *
   * Note that we don't have to use strrchr(), because we only care if
   * a '%' appears before the last character.
   */
  if (strchr(fmt, '%'))
    return -1;

  /* There are no '%' directives at all; we're safe. */
  return 0;
}

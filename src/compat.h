/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * compat.h -- provide some extensions if they aren't available
 */

#ifndef _MU_COMPAT_H
#define _MU_COMPAT_H

#include <stdlib.h>		/* For size_t */

/* Don't define these if we're being included from compat.c. */
#ifndef _MU_COMPAT_C_INTERNAL
/* Just in case these were defined. */
# undef asprintf
# undef vasprintf
# undef strchrnul
# undef reallocarray

# define asprintf(strp, ...)		_mu_asprintf(strp, __VA_ARGS__)
# define vasprintf(strp, fmt, ap)	_mu_vasprintf(strp, fmt, ap)
# define strchrnul(s, c)		_mu_strchrnul(s, c)
# define reallocarray(ptr, nmemb, size)	_mu_reallocarray(ptr, nmemb, size)
#endif /* !_MU_COMPAT_C_INTERNAL */

/* If __attribute__ is not available, define a dummy macro. */
#if !defined(__GNUC__) && !defined(__attribute__)
# define __attribute__(attr)
#endif

int _mu_asprintf(char **, const char *, ...)
  __attribute__((format(printf, 2, 3)));
int _mu_vasprintf(char **, const char *, va_list)
  __attribute__((format(printf, 2, 0)));
char *_mu_strchrnul(const char *, int) __attribute__((pure));
void *_mu_reallocarray(void *, size_t, size_t);

#endif /* !_MU_COMPAT_H */

/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/

/*
 * format.h -- print nicely formatted text
 */

#ifndef _MU_FORMAT_H
#define _MU_FORMAT_H

#include <stdio.h>
#include <stdarg.h>

#include "compat.h"		/* For __attribute__() */

/* The default tab stop. */
#define MU_FORMAT_TAB_STOP 8

/* Default: MU_FORMAT_TAB_STOP. Set to something else if you
   prefer. */
extern unsigned short mu_format_tab_stop;

int mu_format(FILE *, unsigned short *, unsigned short, unsigned short,
	      unsigned short, unsigned short, const char *, ...)
  __attribute__((format(printf, 7, 8)));
char *mu_format_string(unsigned short *, unsigned short, unsigned short,
		       unsigned short, unsigned short, const char *, ...)
  __attribute__((format(printf, 6, 7), malloc));
int mu_vformat(FILE *, unsigned short *, unsigned short, unsigned short,
	       unsigned short, unsigned short, const char *, va_list)
  __attribute__((format(printf, 7, 0)));
char *mu_vformat_string(unsigned short *, unsigned short, unsigned short,
			unsigned short, unsigned short, const char *, va_list)
  __attribute__((format(printf, 6, 0), malloc));

#endif /* !_MU_FORMAT_H */

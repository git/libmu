#!/bin/sh

# bootstrap.sh -- bootstrap the source directory checked out from git

######################################################################
# Mu (Miscellaneous Utilities) is a general convenience library.     #
# Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>               #
#                                                                    #
# This file is part of Mu.                                           #
#                                                                    #
# Mu is free software: you can redistribute it and/or modify it      #
# under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or  #
# (at your option) any later version.                                #
#                                                                    #
# Mu is distributed in the hope that it will be useful, but WITHOUT  #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY #
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public   #
# License for more details.                                          #
#                                                                    #
# You should have received a copy of the GNU General Public License  #
# along with Mu.  If not, see <https://www.gnu.org/licenses/>.       #
######################################################################

package=Mu

examplesdir=doc/examples

available_targets='gnulib autoreconf run-example'

# Print a help message to standard output and exit.
usage() {
    cat <<EOF; exit $1
Usage: $0 [OPTION]... [TARGET]...
Bootstrap $package from checked-out sources. If TARGETs are specified,
only build the specified TARGETs, otherwise build all targets.

  -c, --configure[=ARGS]  run 'configure' with ARGS (or none if not specified)
                            after bootstrapping
  -m, --make[=ARGS]       run 'make' with ARGS (or none if not specified) after
                            bootstrapping; implies --configure
  -h, --help              print this help and exit

Targets:
  $available_targets
EOF
}

# Check if the first argument is repeated in the rest of the
# arguments.
in_list() {
    check=$1
    shift
    for item; do
	if [ "x$check" = "x$item" ]; then
	    return 0	       # 0 is true in shell scripts. Remember?
	fi
    done
    return 1		       # And 1 is false...
}

# Print a message in a banner.
banner() {
    if [ $# -eq 0 ]; then
	message="$(cat)"
    else
	message="$@"
    fi

    # This is so that empty lines will be printed.
    message="$(echo "$message" | sed 's/^$/ /g')"

    # +4 for leading '# ' and trailing ' #'.
    maxlen=$(($(echo "$message" | wc -L) + 4))

    echo
    for i in $(seq $maxlen); do
	echo -n '#'
    done
    echo
    save_IFS=$IFS
    IFS='
'
    for line in $message; do
	echo -n "# $line"
	for i in $(seq $(($(echo -n "$line" | wc -c) + 4)) $maxlen); do
	    echo -n ' '
	done
	echo '#'
    done
    IFS=$save_IFS
    for i in $(seq $maxlen); do
	echo -n '#'
    done
    echo
    echo
}

# Run a command and tell the user that we're running it.
run_cmd() {
    banner "Running $@"
    $@
}

# Print an error message and exit.
error() {
    banner <<EOF 1>&2
An error occurred while bootstrapping. Please make
sure you have the necessary tools installed:

    Gnulib:    https://www.gnu.org/software/gnulib/
    Autoconf:  https://www.gnu.org/software/autoconf/
    Automake:  https://www.gnu.org/software/automake/
EOF
    exit 1
}

do_configure=no
do_make=no

# Parse options
for arg; do
    case $arg in
	-c*)
	    do_configure=yes
	    configure_args=${arg#-c} ;;
	--c|--co|--con|---conf|--confi|--config|--configu| --configur|--configure)
	    do_configure=yes
	    configure_args= ;;
	--c=*|--co=*|--con=*|--conf=*|--confi=*|--config=*| --configu=*|--configur=*|--configure=*)
	    do_configure=yes
	    configure_args=${arg#--c*=} ;;
	-m*)
	    do_make=yes
	    make_args=${arg#-m} ;;
	--m|--ma|--mak|--make)
	    do_make=yes
	    make_args= ;;
	--m=*|--ma=*|--mak=*|--make=*)
	    do_make=yes
	    make_args=${arg#--m*=} ;;
	-h|--h|--he|--hel|--help)
	    usage 0 ;;
	-*)
	    echo "Unknown option: $arg" 1>&2
	    exit 1 ;;
	*)
	    if in_list $arg $available_targets; then
		targets="$targets $arg"
	    else
		echo "Invalid target: $arg" 1>&2
		echo "Available targets: $available_targets" 1>&2
		exit 1
	    fi ;;
    esac
done

if [ "x${targets+set}" != xset ]; then
    targets=$available_targets
fi

if [ "x$do_make" = xyes ]; then
    do_configure=yes
fi

# Cd to the directory this script is in.
cd "$(dirname "$0")"

if in_list gnulib $targets; then
    run_cmd gnulib-tool --update || error
    # We don't use the 'gendocs' module, because we include our own
    # version of 'gendocs_template'. We only want the gendocs.sh
    # script.
    run_cmd gnulib-tool --copy-file build-aux/gendocs.sh
fi
if in_list run-example $targets; then
    # Generate doc/examples/run-example.sh.in from
    # doc/examples/run-example.as. We have to do this before
    # autoreconf, because Autoconf expects to have
    # doc/examples/run-example.sh.in available.
    run_cmd autom4te --language m4sh --cache=autom4te.cache	\
	    -o $examplesdir/run-example.sh.in			\
	    $examplesdir/run-example.as || error

    # Make run-example.sh.in non-executable because we shouldn't execute
    # this directly, but rather run-example.sh created by 'configure'.
    chmod -x $examplesdir/run-example.sh.in ||			\
	echo "$0: warning: cannot make"				\
	     "$examplesdir/run-example.sh.in non-executable" 1>&2
fi
if in_list autoreconf $targets; then
    run_cmd autoreconf -vi || error
fi

if [ "x$do_configure" = xyes ]; then
    run_cmd ./configure $configure_args || error
fi

if [ "x$do_make" = xyes ]; then
    run_cmd make $make_args || error
fi

# Only print the "$package is now..." message if we're building all
# targets. Otherwise, it might not be fully bootstrapped. Rather than
# just comparing $targets and $available_targets as strings, do it
# this way so we can ignore whitespace differences.
all_targets=yes
for target in $available_targets; do
    if ! in_list $target $targets; then
	all_targets=no
	break
    fi
done
if [ "x$all_targets" = xyes ]; then
    if [ "x$do_configure" = xyes ]; then
	if [ "x$do_make" = xyes ]; then
	    banner "$package is now built and ready to install"
	else
	    banner "$package is now configured and ready to build"
	fi
    else
	banner "$package is now bootstrapped and ready to configure"
    fi
fi

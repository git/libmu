#!/usr/bin/env perl

# gen_safe_defines.pl -- generate the macros for safe.h functions

my $license_notice = <<'EOF';
/********************************************************************\
 * Mu (Miscellaneous Utilities) is a general convenience library.   *
 * Copyright (C) 2020  Asher Gordon <AsDaGo@posteo.net>             *
 *                                                                  *
 * This file is part of Mu.                                         *
 *                                                                  *
 * Mu is free software: you can redistribute it and/or modify it    *
 * under the terms of the GNU General Public License as published   *
 * by the Free Software Foundation, either version 3 of the         *
 * License, or (at your option) any later version.                  *
 *                                                                  *
 * Mu is distributed in the hope that it will be useful, but        *
 * WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with Mu.  If not, see                              *
 * <https://www.gnu.org/licenses/>.                                 *
\********************************************************************/
EOF

use strict;
use warnings;

chomp $license_notice;

my $usage = <<EOF;
Usage: $0 [INFILE] [OUTFILE]
Read INFILE, or standard input if not given, and parse it to generate
the macros for safe.h functions. Write the macro definitions to
OUTFILE or standard output if not given.

  -q, --quiet  don't print statistics when finished
  -h, --help   print this help and exit
EOF

# Check if a string is an option specified by $short and $long.
sub is_opt {
    my ($short, $long, $str) = @_;
    $str = $_ unless defined $str;
    $str eq "-$short" || "--$long" =~ /^\Q$str\E/;
}

# Parse options.
my $quiet = 0;
my @positional_args;
my $seen_terminator = 0;
foreach (@ARGV) {
    if ($_ eq '--') {
	$seen_terminator = 1;
	next;
    }
    if ($seen_terminator || !/^-/) {
	push @positional_args, $_;
	next;
    }

    if (is_opt 'q', 'quiet') {
	$quiet = 1;
    }
    elsif (is_opt 'h', 'help') {
	print $usage;
	exit;
    }
    else {
	die "$0: invalid option '$_'\n";
    }
}

die $usage unless @positional_args <= 2;

sub basename {
    my $name = @_ ? $_[0] : $_;
    $name =~ m|([^/]+)$| or die "Invalid filename: $name";
    return $1;
}

my ($in_name, $out_name) = @positional_args;
my ($close_in, $close_out);
my ($in, $out);
if (defined $in_name) {
    open $in, '<', $in_name or die "Cannot open $in_name: $!";
    $close_in = 1;
}
else {
    $in = *STDIN;
    $in_name = '<stdin>';
    $close_in = 0;
}
if (defined $out_name) {
    open $out, '>', $out_name or die "Cannot open $out_name: $!";
    $close_out = 1;
}
else {
    $out = *STDOUT;
    $out_name = '<stdout>';
    $close_out = 0;
}

# Get the basename of the input and output files.
my $in_base = $close_in ? basename $in_name : 'safe.h';
my $out_base = $close_out ? basename $out_name : 'safe_defs.h';

# Print the header.
my @cmd = ($0, @ARGV);
# EOF on separate line to please emacs.
print $out
    <<EOF;
/*
 * DO NOT MODIFY THIS FILE! Generated from $in_base by
 * '@cmd'.
 * Modify $in_base or $0 instead.
 */

$license_notice

/*
 * $out_base -- preprocessor macros for functions declared in $in_base
 */

EOF

# Search for the macro that safe.h defines to let safe_defs.h know
# it's not being included by a dumb user.
my $test_macro;
my $last_macro;
while (<$in>) {
    if (/#\s*define\s+([[:alnum:]_]+)/) {
	$last_macro = $1;
	next;
    }
    if (/#\s*include\s*([<"]\Q$out_base\E[>"])/) {
	die "$1 was included in $in_base without " .
	    "defining an anti-dumb-user macro"
	    unless defined $last_macro;
	$test_macro = $last_macro;
	last;
    }
}
die "\"$out_base\" is never included from $in_base"
    unless defined $test_macro;

# Make sure it's undefined after safe_defs.h is included.
my $is_undefined = 0;
while (<$in>) {
    if (/#\s*undef\s+([[:alnum:]_]+)/ && $1 eq $test_macro) {
	$is_undefined = 1;
	last;
    }
}
die "$test_macro is never undefined" unless $is_undefined;

# Make sure the output file is included from the input file.
print $out
    <<EOF;
#ifndef $test_macro
# error "Never include <mu/$out_base> directly; use <mu/$in_base> instead"
#endif

EOF

# Parse the function definitions and write the macro definitions.
$/ = ')';
my $funcs_parsed = 0;
while (<$in>) {
    next unless /_MU_SAFE_DECL\s*\(([[:alnum:]_]+)\s*,?\s*(.*?)\)/s;

    # The function name and arguments.
    my $func = $1;
    my @args = split /\s*,\s*/, $2;
    my $n_args = @args;

    # Handle variable arguments.
    my $va_args = @args && $args[$#args] eq '...';
    if ($va_args) {
	# Functions with variable arguments must have at least one
	# fixed argument.
	die "$func(" . (join ', ', @args) . ") has no fixed arguments"
	    if $n_args < 2;

	# Subtract 1 from $n_args for the '...' argument, and another
	# because variable arguments in macros must consist of at
	# least one argument.
	$n_args -= 2;
    }

    # Get the list of argument names. Use 'a' for the first argument,
    # 'b' for the second, 'c' for the third, etc. If we reach the 27th
    # argument, use 'aa', then use 'ab', and so on.
    my $argstr = '';
    for (my $i = 0; $i < $n_args; $i++) {
	my $arg = '';
	my $quotient = $i + 1;
	while ($quotient) {
	    use integer;
	    my $remainder = $quotient % 26;
	    $remainder = 26 unless $remainder;
	    $quotient = ($quotient - $remainder) / 26;
	    $arg = (chr ($remainder - 1 + ord 'a')) . $arg;
	}

	$argstr .= ', ' if $argstr;
	$argstr .= $arg;
    }

    my $argdef = $argstr;
    if ($va_args) {
	$argdef .= ', ' if $argdef;
	$argdef .= '...';
	$argstr .= ', ' if $argstr;
	$argstr .= '__VA_ARGS__';
    }
    $argstr = ", $argstr" if $argstr;

    # Now write the macro definition.
    print $out "#define $func($argdef) _MU_SAFE_TR_NAME($func)" .
	"(__FILE__, __LINE__, __func__$argstr)\n";
    $funcs_parsed++;
}

# Close the files if we need to.
if ($close_in) {
    close $in or die "Cannot close $in_name: $!";
}
if ($close_out) {
    close $out or die "Cannot close $out_name: $!";
}

print "$in_name: $funcs_parsed functions parsed\n"
    unless $quiet || $out eq *STDOUT;

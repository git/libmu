/********************************************************************\
 * This is an example file for Mu (Miscellaneous Utilities).        *
 *                                                                  *
 * You may copy, modify, and redistribute this example under the    *
 * terms of the GNU General Public License version 3 (or, at your   *
 * option, any later version), and/or the GNU Free Documentation    *
 * License, Version 1.3 (or, at your option, any later version).    *
 *                                                                  *
 * This example is free software: you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This example is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with this example.  If not, see                    *
 * <https://www.gnu.org/licenses/>.                                 *
 *                                                                  *
 * Permission is granted to copy, distribute and/or modify these    *
 * examples under the terms of the GNU Free Documentation License,  *
 * Version 1.3 or any later version published by the Free Software  *
 * Foundation; with no Invariant Sections, no Front-Cover Texts,    *
 * and no Back-Cover Texts.  A copy of the license is included in   *
 * the file ../fdl-1.3.texi.                                        *
\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <mu/format.h>

int main(void) {
  char *str;
  unsigned short cursor = 0;

  /* Format a message to standard output. */
  puts("===== mu_format =====");
  mu_format(stdout, &cursor, 40, 50, 4, 2, "\
This is some text. The first line will be indented 4 \
characters, while following lines will be indented 2. Lines \
will be wrapped at 40 characters, except \
reaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaally \
long words, which will be wrapped at 50 characters. A line \
break will appear here:\nno matter what.\n");

  /* Write a similarly formatted message to a string. */
  str = mu_format_string(&cursor, 40, 50, 4, 2,
                         "This text is similarly formatted "
                         "to the text above.\n");

  /* Print the string to standard output. */
  puts("===== mu_format_string =====");
  fputs(str, stdout);

  /* We must free the string since `mu_format_string' dynamically
     allocates it. */
  free(str);

  return 0;
}

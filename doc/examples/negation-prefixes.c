/********************************************************************\
 * This is an example file for Mu (Miscellaneous Utilities).        *
 *                                                                  *
 * You may copy, modify, and redistribute this example under the    *
 * terms of the GNU General Public License version 3 (or, at your   *
 * option, any later version), and/or the GNU Free Documentation    *
 * License, Version 1.3 (or, at your option, any later version).    *
 *                                                                  *
 * This example is free software: you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This example is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with this example.  If not, see                    *
 * <https://www.gnu.org/licenses/>.                                 *
 *                                                                  *
 * Permission is granted to copy, distribute and/or modify these    *
 * examples under the terms of the GNU Free Documentation License,  *
 * Version 1.3 or any later version published by the Free Software  *
 * Foundation; with no Invariant Sections, no Front-Cover Texts,    *
 * and no Back-Cover Texts.  A copy of the license is included in   *
 * the file ../fdl-1.3.texi.                                        *
\********************************************************************/

#include <stdio.h>
#include <mu/options.h>
#include <mu/safe.h>            /* For mu_opt_context_x* */

/* Print a message when we find the negatable option. */
static int print_negatable(int value, void *data, char *err) {
  printf("    Found the negatable option, and it was%s negated.\n",
         value ? " not" : "");
  return 0;
}

int main(int argc, char **argv) {
  int ret;
  const MU_OPT options[] = {
    {
     .short_opt          = "n",
     .long_opt           = "negatable",
     .has_arg            = MU_OPT_NONE,
     .negatable          = 1,
     .callback_negatable = print_negatable
    },
    { 0 }
  };
  MU_OPT_CONTEXT *context;

  context = mu_opt_context_xnew(argc, argv, options, MU_OPT_PERMUTE);

  /* Set the negation prefixes. This must be done *before*
     mu_parse_opts() is called. */
  mu_opt_context_xset_no_prefixes(context, "negate-", "no-", "x", NULL);

  /* Now parse the options. */
  ret = mu_parse_opts(context);
  mu_opt_context_xfree(context);
  if (MU_OPT_ERR(ret))
    return 1;

  return 0;
}

/********************************************************************\
 * This is an example file for Mu (Miscellaneous Utilities).        *
 *                                                                  *
 * You may copy, modify, and redistribute this example under the    *
 * terms of the GNU General Public License version 3 (or, at your   *
 * option, any later version), and/or the GNU Free Documentation    *
 * License, Version 1.3 (or, at your option, any later version).    *
 *                                                                  *
 * This example is free software: you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This example is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with this example.  If not, see                    *
 * <https://www.gnu.org/licenses/>.                                 *
 *                                                                  *
 * Permission is granted to copy, distribute and/or modify these    *
 * examples under the terms of the GNU Free Documentation License,  *
 * Version 1.3 or any later version published by the Free Software  *
 * Foundation; with no Invariant Sections, no Front-Cover Texts,    *
 * and no Back-Cover Texts.  A copy of the license is included in   *
 * the file ../fdl-1.3.texi.                                        *
\********************************************************************/

#include <stdio.h>
#include <mu/options.h>
#include <mu/safe.h>            /* For mu_opt_context_x{new,free} */

enum selection { FOO, BAR, BAZ };

int main(int argc, char **argv) {
  enum selection sel;
  int found_sel;
  int ret;
  const MU_ENUM_VALUE enum_table[] = {
    /* Aliases can be specified for enumerated arguments. */
    { "foo|alias-foo", FOO },
    { "bar", BAR },
    /* Aliases can alternatively be specified like this. */
    { "alias-bar", BAR },
    { "baz", BAZ },
    /* This terminates the enumeration specification. */
    { 0 }
  };
  const MU_OPT options[] = {
    {
     .short_opt       = "s",
     .long_opt        = "selection",
     .has_arg         = MU_OPT_REQUIRED,
     .arg_type        = MU_OPT_ENUM,
     /* This indicates that matching should be case insensitive. */
     .enum_case_match = 0,
     .enum_values     = enum_table,
     .found_arg       = &found_sel,
     .arg             = &sel
    },
    {
     .short_opt       = "c",
     .long_opt        = "case-selection",
     .has_arg         = MU_OPT_REQUIRED,
     .arg_type        = MU_OPT_ENUM,
     /* This indicates that matching should be case sensitive. */
     .enum_case_match = 1,
     .enum_values     = enum_table,
     .found_arg       = &found_sel,
     .arg             = &sel
    },
    { 0 }
  };
  MU_OPT_CONTEXT *context;

  /* Parse the options. */
  context = mu_opt_context_xnew(argc, argv, options, MU_OPT_PERMUTE);
  ret = mu_parse_opts(context);
  mu_opt_context_xfree(context);
  if (MU_OPT_ERR(ret))
    return 1;

  if (found_sel) {
    /* Print the selection. */
    fputs("You selected: ", stdout);
    switch (sel) {
    case FOO:
      puts("FOO");
      break;
    case BAR:
      puts("BAR");
      break;
    case BAZ:
      puts("BAZ");
      break;
    default:
      /* This is guaranteed not to happen. */
      puts("an unknown value!");
    }
  }
  else
    puts("You didn't select anything.");

  return 0;
}

/********************************************************************\
 * This is an example file for Mu (Miscellaneous Utilities).        *
 *                                                                  *
 * You may copy, modify, and redistribute this example under the    *
 * terms of the GNU General Public License version 3 (or, at your   *
 * option, any later version), and/or the GNU Free Documentation    *
 * License, Version 1.3 (or, at your option, any later version).    *
 *                                                                  *
 * This example is free software: you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This example is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with this example.  If not, see                    *
 * <https://www.gnu.org/licenses/>.                                 *
 *                                                                  *
 * Permission is granted to copy, distribute and/or modify these    *
 * examples under the terms of the GNU Free Documentation License,  *
 * Version 1.3 or any later version published by the Free Software  *
 * Foundation; with no Invariant Sections, no Front-Cover Texts,    *
 * and no Back-Cover Texts.  A copy of the license is included in   *
 * the file ../fdl-1.3.texi.                                        *
\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <mu/options.h>
#include <mu/safe.h>            /* For mu_opt_context_x{new,free} */

int file_callback(int has_arg, const char *filename,
                  FILE *file, void *data, char *err) {
  /* Make sure the file is named "foo". */
  if (strcmp(filename, "foo")) {
    snprintf(err, MU_OPT_ERR_MAX, "file is not named \"foo\"");
    return 1;
  }

  /* It is named "foo"; return `file' in `*data'.
     This is WRONG! Do not do this! */
  *(FILE **)data = file;

  return 0;
}

int main(int argc, char **argv) {
  FILE *file = NULL;
  char buf[256];
  size_t size;
  int ret;
  const MU_OPT options[] = {
    {
     .short_opt     = "f",
     .long_opt      = "file",
     .has_arg       = MU_OPT_REQUIRED,
     .arg_type      = MU_OPT_FILE,
     .file_mode     = "r",
     .callback_file = file_callback,
     .cb_data       = &file
    },
    { 0 }
  };
  MU_OPT_CONTEXT *context;

  /* Parse the options. */
  context = mu_opt_context_xnew(argc, argv, options, MU_OPT_PERMUTE);
  ret = mu_parse_opts(context);
  mu_opt_context_xfree(context);
  if (MU_OPT_ERR(ret))
    return 1;

  if (!file) {
    /* We weren't passed the `-f' option. */
    return 0;
  }

  /* Read the file. This invokes UNDEFINED BEHAVIOR because
     `file' was already closed by `mu_parse_opts'! */
  size = fread(buf, sizeof(*buf), sizeof(buf), file);
  if (ferror(file)) {
    fprintf(stderr, "%s: cannot read foo: %s\n",
            argv[0], strerror(errno));
    return 1;
  }
  fclose(file);

  /* Print the contents of the file to standard output. */
  fwrite(buf, sizeof(*buf), size, stdout);

  return 0;
}

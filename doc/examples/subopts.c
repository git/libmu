/********************************************************************\
 * This is an example file for Mu (Miscellaneous Utilities).        *
 *                                                                  *
 * You may copy, modify, and redistribute this example under the    *
 * terms of the GNU General Public License version 3 (or, at your   *
 * option, any later version), and/or the GNU Free Documentation    *
 * License, Version 1.3 (or, at your option, any later version).    *
 *                                                                  *
 * This example is free software: you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This example is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with this example.  If not, see                    *
 * <https://www.gnu.org/licenses/>.                                 *
 *                                                                  *
 * Permission is granted to copy, distribute and/or modify these    *
 * examples under the terms of the GNU Free Documentation License,  *
 * Version 1.3 or any later version published by the Free Software  *
 * Foundation; with no Invariant Sections, no Front-Cover Texts,    *
 * and no Back-Cover Texts.  A copy of the license is included in   *
 * the file ../fdl-1.3.texi.                                        *
\********************************************************************/

#include <stdio.h>
#include <mu/options.h>
#include <mu/safe.h>            /* For mu_opt_context_x* */

int subopt_none(void *data, char *err) {
  puts("suboption found: none");
  return 0;
}

int subopt_opt(int has_arg, const char *arg,
               void *data, char *err) {
  puts("suboption found: opt");
  if (has_arg)
    printf("argument: %s\n", arg);
  return 0;
}

int subopt_req(int has_arg, const char *arg,
               void *data, char *err) {
  printf("suboption found: req\nargument: %s\n", arg);
  return 0;
}

int main(int argc, char **argv) {
  int ret;
  /* These are the suboptions that can be passed to the `-o'
     option. They are specified just like regular options, except
     that `subopt_name' is used instead of `long_opt' or
     `short_opt', and they may not have suboptions of their
     own. */
  const MU_OPT suboptions[] = {
    {
     .subopt_name     = "none",
     .has_arg         = MU_OPT_NONE,
     .callback_none   = subopt_none,
     .help            = "a suboption taking no arguments"
    },
    {
     .subopt_name     = "opt",
     .has_arg         = MU_OPT_OPTIONAL,
     .arg_type        = MU_OPT_STRING,
     .callback_string = subopt_opt,
     .help            = "a suboption taking an optional argument"
    },
    {
     .subopt_name     = "req",
     .has_arg         = MU_OPT_REQUIRED,
     .arg_type        = MU_OPT_STRING,
     .callback_string = subopt_req,
     .help            = "a suboption taking a required argument"
    },
    { 0 }
  };
  const MU_OPT options[] = {
    {
     .short_opt = "o",
     .long_opt  = "options",
     .has_arg   = MU_OPT_REQUIRED,
     .arg_type  = MU_OPT_SUBOPT,
     .subopts   = suboptions,
     .help      = "a regular option which takes suboptions"
    },
    { 0 }
  };
  MU_OPT_CONTEXT *context;

  context = mu_opt_context_xnew(argc, argv, options, MU_OPT_PERMUTE);

  /* Add the help option. */
  mu_opt_context_add_help(context, NULL, NULL, "Parse suboptions.",
                          NULL, "1", NULL, NULL, NULL);
  mu_opt_context_xadd_help_options(context, MU_HELP_BOTH);

  /* Parse the options. */
  ret = mu_parse_opts(context);
  mu_opt_context_xfree(context);
  if (MU_OPT_ERR(ret))
    return 1;

  return 0;
}

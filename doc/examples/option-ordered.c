/********************************************************************\
 * This is an example file for Mu (Miscellaneous Utilities).        *
 *                                                                  *
 * You may copy, modify, and redistribute this example under the    *
 * terms of the GNU General Public License version 3 (or, at your   *
 * option, any later version), and/or the GNU Free Documentation    *
 * License, Version 1.3 (or, at your option, any later version).    *
 *                                                                  *
 * This example is free software: you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This example is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with this example.  If not, see                    *
 * <https://www.gnu.org/licenses/>.                                 *
 *                                                                  *
 * Permission is granted to copy, distribute and/or modify these    *
 * examples under the terms of the GNU Free Documentation License,  *
 * Version 1.3 or any later version published by the Free Software  *
 * Foundation; with no Invariant Sections, no Front-Cover Texts,    *
 * and no Back-Cover Texts.  A copy of the license is included in   *
 * the file ../fdl-1.3.texi.                                        *
\********************************************************************/

#include <stdio.h>
#include <mu/options.h>
#include <mu/safe.h>            /* For mu_opt_context_x{new,free} */

/* Callbacks to print a message when we find an option. */

static int print_example(void *data, char *err) {
  puts("Option found: example");
  return 0;
}

static int print_another(void *data, char *err) {
  puts("Option found: another");
  return 0;
}

int main(int argc, char **argv) {
  const MU_OPT options[] = {
    {
     .short_opt     = "e",
     .long_opt      = "example",
     .has_arg       = MU_OPT_NONE,
     .callback_none = print_example
    },
    {
     .short_opt     = "a",
     .long_opt      = "another",
     .has_arg       = MU_OPT_NONE,
     .callback_none = print_another
    },
    { 0 }
  };
  MU_OPT_CONTEXT *context;

  context = mu_opt_context_xnew(argc, argv, options, MU_OPT_CONTINUE);
  while (argc > 1) {
    int ret;

    /* Parse options. */
    ret = mu_parse_opts(context);
    if (MU_OPT_ERR(ret))
      return 1;

    /* Shift the arguments (to get rid of the options we just
       parsed). */
    mu_shift_args(&argc, &argv, ret);

    if (argc > 1) {
      /* Print an argument (we don't have to print them all at once
         because if `mu_parse_opts' doesn't find any options, it will
         just return 0). */
      printf("Argument found: %s\n", argv[1]);
      /* Shift away this argument. */
      mu_shift_args(&argc, &argv, 1);
      mu_opt_context_shift(context, 1);
    }
  }
  mu_opt_context_xfree(context);

  return 0;
}

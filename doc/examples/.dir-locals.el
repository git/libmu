;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; Don't use tabs in these example files since they will be included
;; in the Texinfo manual.
((c-mode
  (indent-tabs-mode . nil)))

/********************************************************************\
 * This is an example file for Mu (Miscellaneous Utilities).        *
 *                                                                  *
 * You may copy, modify, and redistribute this example under the    *
 * terms of the GNU General Public License version 3 (or, at your   *
 * option, any later version), and/or the GNU Free Documentation    *
 * License, Version 1.3 (or, at your option, any later version).    *
 *                                                                  *
 * This example is free software: you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This example is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with this example.  If not, see                    *
 * <https://www.gnu.org/licenses/>.                                 *
 *                                                                  *
 * Permission is granted to copy, distribute and/or modify these    *
 * examples under the terms of the GNU Free Documentation License,  *
 * Version 1.3 or any later version published by the Free Software  *
 * Foundation; with no Invariant Sections, no Front-Cover Texts,    *
 * and no Back-Cover Texts.  A copy of the license is included in   *
 * the file ../fdl-1.3.texi.                                        *
\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <mu/options.h>
#include <mu/compat.h>          /* For __attribute__() */
#include <mu/safe.h>            /* For mu_opt_context_x* */

__attribute__((noreturn))
int print_version(void *data, char *err) {
  puts("Version 1.0");
  exit(0);
}

int main(int argc, char **argv) {
  int ret;
  const MU_OPT opts_start[] = {
    {
     .short_opt = "n",
     .long_opt  = "none",
     .has_arg   = MU_OPT_NONE,
     .help      = "an option which takes no argument"
    },
    { .category = "Options taking arguments" },
    {
     .short_opt = "o",
     .long_opt  = "optional",
     .has_arg   = MU_OPT_OPTIONAL,
     .arg_type  = MU_OPT_STRING,
     .arg_help  = "OPTARG",
     .help      = "an option which optionally takes an argument"
    },
    {
     .short_opt = "r",
     .long_opt  = "required",
     .has_arg   = MU_OPT_REQUIRED,
     .arg_type  = MU_OPT_STRING,
     .arg_help  = "REQARG",
     .help      = "an option which requires an argument"
    },
    { .category = "Help options and environment variables" },
    { 0 }
  };
  /* Options to add after the help options. */
  const MU_OPT opts_end[] = {
    { .category = "Version information" },
    {
     .short_opt = "v",
     .long_opt  = "version",
     .has_arg   = MU_OPT_NONE,
     .callback_none = print_version,
     .help      = "print version information and exit"
    },
    { 0 }
  };
  MU_OPT_CONTEXT *context;

  context = mu_opt_context_xnew(argc, argv, opts_start,
                                MU_OPT_BUNDLE | MU_OPT_PERMUTE);

  /* Add the help data. */
  mu_opt_context_add_help(context, "[OPTION]...", "do stuff",
                          "Do stuff. If this text is really long, it "
                          "will be wrapped. Some more text to make "
                          "this text long enough to be wrapped.",
                          "Report bugs to <libmu-bug@nongnu.org>.",
                          "1", NULL, "Mu Examples", NULL);
  /* Create the help option. MU_HELP_ALL is equivalent to
     MU_HELP_SHORT | MU_HELP_LONG | MU_HELP_MAN_SHORT |
     MU_HELP_MAN_LONG | MU_HELP_ENV, so it will create the options
     '-h', '--help', '-m', and '--man', and it will create the
     environment variable 'MU_HELP_FORMAT'. */
  mu_opt_context_xadd_help_options(context, MU_HELP_ALL);
  /* Add the other options. */
  mu_opt_context_xadd_options(context, opts_end, MU_OPT_APPEND);

  /* Parse the options. */
  ret = mu_parse_opts(context);

  /* If there was an option parsing error, print a usage message so
     the user knows how to use us properly. */
  if (ret == MU_OPT_ERR_PARSE)
    mu_format_help(stderr, context);

  mu_opt_context_xfree(context);

  return !!MU_OPT_ERR(ret);
}

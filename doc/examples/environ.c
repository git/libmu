/********************************************************************\
 * This is an example file for Mu (Miscellaneous Utilities).        *
 *                                                                  *
 * You may copy, modify, and redistribute this example under the    *
 * terms of the GNU General Public License version 3 (or, at your   *
 * option, any later version), and/or the GNU Free Documentation    *
 * License, Version 1.3 (or, at your option, any later version).    *
 *                                                                  *
 * This example is free software: you can redistribute it and/or    *
 * modify it under the terms of the GNU General Public License as   *
 * published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.              *
 *                                                                  *
 * This example is distributed in the hope that it will be useful,  *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    *
 * GNU General Public License for more details.                     *
 *                                                                  *
 * You should have received a copy of the GNU General Public        *
 * License along with this example.  If not, see                    *
 * <https://www.gnu.org/licenses/>.                                 *
 *                                                                  *
 * Permission is granted to copy, distribute and/or modify these    *
 * examples under the terms of the GNU Free Documentation License,  *
 * Version 1.3 or any later version published by the Free Software  *
 * Foundation; with no Invariant Sections, no Front-Cover Texts,    *
 * and no Back-Cover Texts.  A copy of the license is included in   *
 * the file ../fdl-1.3.texi.                                        *
\********************************************************************/

#include <stdio.h>
#include <mu/options.h>
#include <mu/safe.h>            /* For mu_opt_context_x* */

/* Print a message when an option is found. */
int print_opt(int has_arg, const char *arg,
              void *data, char *err) {
  const char *name = data;
  printf("Found an option/environment variable '%s'", name);
  if (has_arg)
    printf(" with an argument '%s'", arg);
  putchar('\n');
  return 0;
}

int main(int argc, char **argv) {
  int ret;
  const MU_OPT suboptions[] = {
    {
     .subopt_name     = "subopt",
     /* Suboptions can have environment variables as well. */
     .env_var         = "ENV_SUBOPT",
     .has_arg         = MU_OPT_OPTIONAL,
     .arg_type        = MU_OPT_STRING,
     .callback_string = print_opt,
     .cb_data         = "a suboption",
     .help            =
     "a suboption with an equivalent environment variable"
    },
    { 0 }
  };
  const MU_OPT options[] = {
    {
     .short_opt       = "a",
     .long_opt        = "an-option",
     /* AN_ENV_VAR will always take precedence over ALIAS since it is
        specified first below. */
     .env_var         = "AN_ENV_VAR|ALIAS",
     .has_arg         = MU_OPT_OPTIONAL,
     .arg_type        = MU_OPT_STRING,
     .callback_string = print_opt,
     .cb_data         = "an option",
     .help            =
     "an option with an equivalent environment variable"
    },
    {
     .short_opt       = "b",
     .long_opt        = "another-option",
     .has_arg         = MU_OPT_OPTIONAL,
     .arg_type        = MU_OPT_STRING,
     .callback_string = print_opt,
     .cb_data         = "another option",
     .help            =
     "an option without an equivalent environment variable"
    },
    {
     .env_var         = "ANOTHER_ENV_VAR",
     .has_arg         = MU_OPT_REQUIRED,
     /* Environment variables can have suboptions as well. */
     .arg_type        = MU_OPT_SUBOPT,
     .subopts         = suboptions,
     .help            =
     "an environment variable (which takes "
     "suboptions) without an equivalent option"
    },
    { 0 }
  };
  MU_OPT_CONTEXT *context;

  context = mu_opt_context_xnew(argc, argv, options, MU_OPT_PERMUTE);

  /* Add the help option. */
  mu_opt_context_add_help(context, NULL, NULL,
                          "Parse options and environment variables.",
                          NULL, "1", NULL, NULL, NULL);
  mu_opt_context_xadd_help_options(context, MU_HELP_BOTH);

  /* Parse the options. */
  ret = mu_parse_opts(context);
  mu_opt_context_xfree(context);
  if (MU_OPT_ERR(ret))
    return 1;

  return 0;
}
